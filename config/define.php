<?php
	
	define('PASSWORD_ENCRYPTION', "bcrypt"); //available values: "sha512", "bcrypt"
	define('PASSWORD_BCRYPT_COST', "13");
	define('PASSWORD_SHA512_ITERATIONS', 25000);
	define('PASSWORD_SALT', "UPj8EVJWvP73FZ9Ih0xzUf");
	define('PASSWORD_RESET_KEY_LIFE', 60);
