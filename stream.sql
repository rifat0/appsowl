-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 15, 2019 at 11:22 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stream`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent` mediumtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `note` mediumtext COLLATE utf8mb4_unicode_ci,
  `document` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`log_id`, `subject`, `url`, `method`, `ip`, `agent`, `user_id`, `note`, `document`, `created_at`, `updated_at`) VALUES
(1, 'My Testing Add To Log.', 'http://localhost/apz/add-to-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 09:17:27', '2019-05-04 09:17:27'),
(2, 'My Testing Add To Log.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:19:49', '2019-05-04 10:19:49'),
(3, 'My Testing Add To Log.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:22:28', '2019-05-04 10:22:28'),
(4, 'My Testing Add To Log.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:23:44', '2019-05-04 10:23:44'),
(5, 'log from dashboard', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:24:16', '2019-05-04 10:24:16'),
(6, 'log from dashboard', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:27:13', '2019-05-04 10:27:13'),
(7, 'log from dashboard', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:29:24', '2019-05-04 10:29:24'),
(8, 'log from dashboard', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:29:33', '2019-05-04 10:29:33'),
(9, 'log from blade', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:29:52', '2019-05-04 10:29:52'),
(10, 'log from dashboard', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:31:03', '2019-05-04 10:31:03'),
(11, 'Using SuperAdminController.', 'http://localhost/apz/agent-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:33:01', '2019-05-04 10:33:01'),
(12, 'Using HomeController.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:33:29', '2019-05-04 10:33:29'),
(13, 'Using SuperAdminController.', 'http://localhost/apz/subdomain-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:33:31', '2019-05-04 10:33:31'),
(14, 'Using HomeController.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:34:24', '2019-05-04 10:34:24'),
(15, 'Using dashboard.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 10:34:24', '2019-05-04 10:34:24'),
(16, 'Using HomeController.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:42:06', '2019-05-04 10:42:06'),
(17, 'Using HomeController.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:42:10', '2019-05-04 10:42:10'),
(18, 'Using HomeController.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:42:51', '2019-05-04 10:42:51'),
(19, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:46:13', '2019-05-04 10:46:13'),
(20, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:51:28', '2019-05-04 10:51:28'),
(21, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:56:18', '2019-05-04 10:56:18'),
(22, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:56:23', '2019-05-04 10:56:23'),
(23, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:57:30', '2019-05-04 10:57:30'),
(24, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:57:48', '2019-05-04 10:57:48'),
(25, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 10:58:18', '2019-05-04 10:58:18'),
(26, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:04:40', '2019-05-04 11:04:40'),
(27, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:04:54', '2019-05-04 11:04:54'),
(28, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:05:14', '2019-05-04 11:05:14'),
(29, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:06:26', '2019-05-04 11:06:26'),
(30, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:07:29', '2019-05-04 11:07:29'),
(31, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:08:35', '2019-05-04 11:08:35'),
(32, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:09:26', '2019-05-04 11:09:26'),
(33, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:10:06', '2019-05-04 11:10:06'),
(34, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:10:58', '2019-05-04 11:10:58'),
(35, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:11:21', '2019-05-04 11:11:21'),
(36, 'Using SuperAdminController.', 'http://localhost/apz/user-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:34:06', '2019-05-04 11:34:06'),
(37, 'Using SuperAdminController.', 'http://localhost/apz/agent-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:34:17', '2019-05-04 11:34:17'),
(38, 'Using SuperAdminController.', 'http://localhost/apz/user-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:34:29', '2019-05-04 11:34:29'),
(39, 'Using SuperAdminController.', 'http://localhost/apz/user-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:35:55', '2019-05-04 11:35:55'),
(40, 'Using SuperAdminController.', 'http://localhost/apz/user-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:36:18', '2019-05-04 11:36:18'),
(41, 'Using SuperAdminController.', 'http://localhost/apz/user-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:36:37', '2019-05-04 11:36:37'),
(42, 'Using SuperAdminController.', 'http://localhost/apz/user-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:37:16', '2019-05-04 11:37:16'),
(43, 'Using SuperAdminController.', 'http://localhost/apz/user-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:37:39', '2019-05-04 11:37:39'),
(44, 'Using SuperAdminController.', 'http://localhost/apz/user-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:38:02', '2019-05-04 11:38:02'),
(45, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:38:58', '2019-05-04 11:38:58'),
(46, 'Using HomeController.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:39:44', '2019-05-04 11:39:44'),
(47, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:39:48', '2019-05-04 11:39:48'),
(48, 'Using HomeController.', 'http://localhost/apz/dashboard', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:39:52', '2019-05-04 11:39:52'),
(49, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:39:53', '2019-05-04 11:39:53'),
(50, 'Using SuperAdminController.', 'http://localhost/apz/agent-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:46:33', '2019-05-04 11:46:33'),
(51, 'Using SuperAdminController.', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:46:43', '2019-05-04 11:46:43'),
(52, 'Commission changed from to ', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 11:46:43', '2019-05-04 11:46:43'),
(53, 'Using SuperAdminController.', 'http://localhost/apz/agent-manage', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:46:43', '2019-05-04 11:46:43'),
(54, 'Using HomeController.', 'http://localhost/apz/activity-log', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, NULL, NULL, '2019-05-04 11:46:49', '2019-05-04 11:46:49'),
(55, 'Commission changed from to ', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 11:48:18', '2019-05-04 11:48:18'),
(56, 'Commission changed from to 100', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 11:49:02', '2019-05-04 11:49:02'),
(57, 'Commission changed from 100 to 15', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 11:50:59', '2019-05-04 11:50:59'),
(58, '4 commission changed from 15 to 20', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 11:53:19', '2019-05-04 11:53:19'),
(59, 'dsfds sf commission changed from 20 to 100', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 12:01:07', '2019-05-04 12:01:07'),
(60, 'dsfds sf commission changed from 100 to 25', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-04 12:02:10', '2019-05-04 12:02:10'),
(61, 'Test main Test main status changed', 'http://localhost/apz/user_status_change/24', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 06:13:55', '2019-05-05 06:13:55'),
(62, 'Test main Test main status changed from Active to Deactive', 'http://localhost/apz/user_status_change/24', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 06:18:16', '2019-05-05 06:18:16'),
(63, 'Test main Test main status changed from Deactive to Active', 'http://localhost/apz/user_status_change/24', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 06:19:28', '2019-05-05 06:19:28'),
(64, 'Test main Test main status changed from Active to Deactive', 'http://localhost/apz/user_status_change/24', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 07:10:33', '2019-05-05 07:10:33'),
(65, 'dsfds sf commission changed from 25 to 15', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 08:39:50', '2019-05-05 08:39:50'),
(66, 'dsfds sf status changed from Active to Deactive', 'http://localhost/apz/user_status_change/4', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 08:39:55', '2019-05-05 08:39:55'),
(67, 'dsfds sf status changed from Deactive to Active', 'http://localhost/apz/user_status_change/4', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 08:39:57', '2019-05-05 08:39:57'),
(68, 'Test main Test main status changed from Deactive to Active', 'http://localhost/apz/user_status_change/24', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 08:48:26', '2019-05-05 08:48:26'),
(69, 'Test main Test main status changed from Active to Deactive', 'http://localhost/apz/user_status_change/24', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 08:48:30', '2019-05-05 08:48:30'),
(70, 'Test main Test main Test plugin status changed to inactive', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 09:08:04', '2019-05-05 09:08:04'),
(71, 'Test main Test main Test plugin status changed to active', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 09:09:16', '2019-05-05 09:09:16'),
(72, 'Test main Test main Test plugin status changed to inactive', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 09:10:42', '2019-05-05 09:10:42'),
(73, 'Test main Test main Test plugin status changed to active', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 09:12:18', '2019-05-05 09:12:18'),
(74, 'Test main Test main Test plugin status changed to inactive', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 09:12:31', '2019-05-05 09:12:31'),
(75, 'Test main Test main Test plugin status changed to active', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 09:12:46', '2019-05-05 09:12:46'),
(76, 'Test main Test main Test plugin status changed to inactive', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 09:13:18', '2019-05-05 09:13:18'),
(77, 'Test main Test main Test plugin status changed to Test note', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, '1557051413.png', '2019-05-05 10:16:53', '2019-05-05 10:16:53'),
(78, 'Test main Test main Test plugin status changed to inactive', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, '1557051605.jpg', '2019-05-05 10:20:05', '2019-05-05 10:20:05'),
(79, 'Test main Test main Test plugin status changed to active', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, '1557051699.jpg', '2019-05-05 10:21:40', '2019-05-05 10:21:40'),
(80, 'Test main Test main Test plugin status changed to inactive', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'Test note', '1557051823.jpg', '2019-05-05 10:23:43', '2019-05-05 10:23:43'),
(81, 'Test main Test main Test plugin status changed to active', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'd', '1557051843.jpg', '2019-05-05 10:24:04', '2019-05-05 10:24:04'),
(82, 'Test main Test main Test plugin status changed to inactive', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'Taka dey nai', '1557053623.jpg', '2019-05-05 10:53:43', '2019-05-05 10:53:43'),
(83, 'Test main Test main status changed from Deactive to Active', 'http://localhost/apz/user_status_change/24', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 11:30:22', '2019-05-05 11:30:22'),
(84, 'Test main Test main Test software status changed to inactive', 'http://localhost/apz/subscribe-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'Test', NULL, '2019-05-05 11:31:52', '2019-05-05 11:31:52'),
(85, 'Test main Test main status changed from Active to Deactive', 'http://localhost/apz/user_status_change/24', 'GET', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-05 11:34:42', '2019-05-05 11:34:42'),
(86, 'Test main Test main status changed from Deactive to Active', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'Demo', '1557057426.png', '2019-05-05 11:57:06', '2019-05-05 11:57:06'),
(87, 'Test main Test main status changed from Active to Deactive', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'Demo', '1557057494.png', '2019-05-05 11:58:14', '2019-05-05 11:58:14'),
(88, 'Test main Test main status changed from Deactive to Active', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'RRRRRRRRRR', NULL, '2019-05-05 11:58:51', '2019-05-05 11:58:51'),
(89, 'dsfds sf status changed from Active to Deactive', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'demo', '1557058764.png', '2019-05-05 12:19:24', '2019-05-05 12:19:24'),
(90, 'dsfds sf status changed from Deactive to Active', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'asd', NULL, '2019-05-05 12:19:35', '2019-05-05 12:19:35'),
(91, 'dsfds sf status changed from Active to Deactive', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'asd', NULL, '2019-05-05 12:19:46', '2019-05-05 12:19:46'),
(92, 'dsfds sf status changed from Deactive to Active', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'j', NULL, '2019-05-05 12:26:19', '2019-05-05 12:26:19'),
(93, 'dsfds sf status changed from Active to Deactive', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'Agent check', '1557126509.jpg', '2019-05-06 07:08:29', '2019-05-06 07:08:29'),
(94, 'dsfds sf commission changed from 15 to 10', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-06 07:41:23', '2019-05-06 07:41:23'),
(95, 'dsfds sf paid by 500.00', 'http://localhost/apz/agent-pay', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-06 09:03:30', '2019-05-06 09:03:30'),
(96, 'dsfds sf agent bill paid by 500.00', 'http://localhost/apz/agent-pay', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, '1557133680.png', '2019-05-06 09:08:00', '2019-05-06 09:08:00'),
(97, 'dsfds sf status changed from Deactive to Active', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'asdas', NULL, '2019-05-06 09:09:03', '2019-05-06 09:09:03'),
(98, 'dsfds sf commission changed from 10 to 15', 'http://localhost/apz/commission-submit', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-06 10:13:58', '2019-05-06 10:13:58'),
(99, 'Test Test status changed from Active to Deactive', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'fghfsgdfgdfghfghf', NULL, '2019-05-06 11:41:15', '2019-05-06 11:41:15'),
(100, 'Test Test status changed from Deactive to Active', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'ddddddddddddddd', NULL, '2019-05-06 11:41:41', '2019-05-06 11:41:41'),
(101, '1 2 status changed from Active to Deactive', 'http://localhost/apz/user-status-change', 'POST', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'asd', NULL, '2019-05-06 11:53:40', '2019-05-06 11:53:40'),
(102, 'Test main Test main status changed from Active to Deactive', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'test', '1557386171.txt', '2019-05-09 07:16:11', '2019-05-09 07:16:11'),
(103, 'Test main Test main status changed from Deactive to Active', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 's', '1557386418.txt', '2019-05-09 07:20:18', '2019-05-09 07:20:18'),
(104, 'Test main Test main status changed from Active to Deactive', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 's', '1557386452.txt', '2019-05-09 07:20:52', '2019-05-09 07:20:52'),
(105, 'Test main Test main status changed from Deactive to Active', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'deeeee', '1557388070.png', '2019-05-09 07:47:50', '2019-05-09 07:47:50'),
(106, 'Test main Test main status changed from Active to Deactive', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'a', '1557388125.jpg', '2019-05-09 07:48:45', '2019-05-09 07:48:45'),
(107, 'Test main Test main status changed from Deactive to Active', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'sssss', '1557388154.txt', '2019-05-09 07:49:14', '2019-05-09 07:49:14'),
(108, 'Test main Test main Test software status changed to active', 'http://apz.local/subscribe-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'ddddd', '1557389005.jpg', '2019-05-09 08:03:26', '2019-05-09 08:03:26'),
(109, '1 2 status changed from Active to Deactive', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'fggfgfd', '1557392630.jpg', '2019-05-09 09:03:51', '2019-05-09 09:03:51'),
(110, '1 2 status changed from Deactive to Active', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 's', '1557392699.jpg', '2019-05-09 09:04:59', '2019-05-09 09:04:59'),
(111, '123456 status changed from active to deactive.', 'http://apz.local/promocode-status', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'TEst', '1557396776.txt', '2019-05-09 10:12:56', '2019-05-09 10:12:56'),
(112, '123456 status changed from active to deactive.', 'http://apz.local/promocode-status', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'd11111111111111111', '1557396859.editorconfig', '2019-05-09 10:14:19', '2019-05-09 10:14:19'),
(113, '123456 status changed from deactive to active.', 'http://apz.local/promocode-status', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'ju', NULL, '2019-05-09 10:14:37', '2019-05-09 10:14:37'),
(114, '1 voucher added by Rifat Hasan', 'http://apz.local/promocode-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-12 04:38:17', '2019-05-12 04:38:17'),
(115, ' voucher status changed from active to inactive.', 'http://apz.local/voucher-status', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'testtttt', '1557637100.png', '2019-05-12 04:58:20', '2019-05-12 04:58:20'),
(116, ' voucher status changed from inactive to active.', 'http://apz.local/voucher-status', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'd', NULL, '2019-05-12 04:58:32', '2019-05-12 04:58:32'),
(117, ' voucher status changed from active to inactive.', 'http://apz.local/voucher-status', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'd', NULL, '2019-05-12 04:58:58', '2019-05-12 04:58:58'),
(118, '1 voucher status changed from inactive to active.', 'http://apz.local/voucher-status', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 's', NULL, '2019-05-12 05:01:12', '2019-05-12 05:01:12'),
(119, 'Test main Test main Test software status changed to inactive', 'http://apz.local/subscribe-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'g', NULL, '2019-05-12 07:18:51', '2019-05-12 07:18:51'),
(120, 'Test main Test main Test software status changed to active', 'http://apz.local/subscribe-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'k', NULL, '2019-05-12 08:23:25', '2019-05-12 08:23:25'),
(121, 'Test main Test main Test software status changed to inactive', 'http://apz.local/subscribe-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'h', NULL, '2019-05-12 08:23:36', '2019-05-12 08:23:36'),
(122, '200 fund added to 24', 'http://apz.local/add-fund-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'dfighdfkjgdfkjn', '1557655719.txt', '2019-05-12 10:08:39', '2019-05-12 10:08:39'),
(123, '5 fund added to 24', 'http://apz.local/add-fund-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'dfgdf', NULL, '2019-05-12 11:11:34', '2019-05-12 11:11:34'),
(124, '20 fund added to 24', 'http://apz.local/add-fund-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-13 05:50:30', '2019-05-13 05:50:30'),
(125, '100 fund added to 24', 'http://apz.local/add-fund-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, NULL, '2019-05-13 05:55:11', '2019-05-13 05:55:11'),
(126, 'Test main Test main status changed from Active to Deactive', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, 'fdf', '1557731930.txt', '2019-05-13 07:18:50', '2019-05-13 07:18:50'),
(127, 'dsfds sf agent bill paid by 500.00', 'http://apz.local/agent-pay', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 1, NULL, '1557732925.txt', '2019-05-13 07:35:25', '2019-05-13 07:35:25'),
(128, 'Test main Test main status changed from Deactive to Active', 'http://apz.local/user-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 's', NULL, '2019-05-14 05:26:28', '2019-05-14 05:26:28'),
(129, '4 status changed to hold', 'http://apz.local/withdraw-status-change/4/hold', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-14 08:18:30', '2019-05-14 08:18:30'),
(130, '4 status changed to due', 'http://apz.local/withdraw-status-change/4/due', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-14 08:18:41', '2019-05-14 08:18:41'),
(131, '4 status changed to cancel', 'http://apz.local/withdraw-status-change/4/cancel', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-14 08:19:14', '2019-05-14 08:19:14'),
(132, '4 status changed to due', 'http://apz.local/withdraw-status-change/4/due', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-14 08:19:19', '2019-05-14 08:19:19'),
(133, '4 status changed to hold', 'http://apz.local/withdraw-status-change/4/hold', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-14 08:34:31', '2019-05-14 08:34:31'),
(134, '4 status changed to due', 'http://apz.local/withdraw-status-change/4/due', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-14 08:34:35', '2019-05-14 08:34:35'),
(135, '4 withdrawal paid by 500.00', 'http://apz.local/withdrawal-pay', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'withdraw pay first', '1557823859.doc', '2019-05-14 08:50:59', '2019-05-14 08:50:59'),
(136, '1 withdrawal paid by 500.00', 'http://apz.local/withdrawal-pay', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, '9999999999999999999999999999999999', '1557824316.txt', '2019-05-14 08:58:37', '2019-05-14 08:58:37'),
(137, 'Test main Test main Test software status changed to ', 'http://apz.local/subscribe-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'sub change', '1557830670.png', '2019-05-14 10:44:30', '2019-05-14 10:44:30'),
(138, 'Test main Test main Test software status changed to cancel', 'http://apz.local/subscribe-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, '0000000000', NULL, '2019-05-14 10:46:24', '2019-05-14 10:46:24'),
(139, '1 Edited.', 'http://apz.local/subscribe-edit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, '8888888888888', '1557832174.PNG', '2019-05-14 11:09:34', '2019-05-14 11:09:34'),
(140, 'dsfds sf commission changed from 15 to 50', 'http://apz.local/commission-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-14 11:14:58', '2019-05-14 11:14:58'),
(141, 'withdrawal id 5 withdrawal paid by 500.00', 'http://apz.local/withdrawal-pay', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'ygfhgfhg', '1557832616.PNG', '2019-05-14 11:16:56', '2019-05-14 11:16:56'),
(142, 'Test main Test main Test software status changed to active', 'http://apz.local/subscribe-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'test', NULL, '2019-05-14 11:19:45', '2019-05-14 11:19:45'),
(143, 'Test main Test main Test software status changed to return', 'http://127.0.0.1:8000/subscribe-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:66.0) Gecko/20100101 Firefox/66.0', 31, 'dsf', NULL, '2019-05-15 07:53:01', '2019-05-15 07:53:01'),
(144, '100 fund added to 24', 'http://apz.local/add-fund-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-15 08:43:21', '2019-05-15 08:43:21'),
(145, 'Send Message To Test main Test main', 'http://apz.local/send_user_message', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'gh', NULL, '2019-05-15 08:45:58', '2019-05-15 08:45:58'),
(146, 'Add Privacy & Policy By Super Admin', 'http://apz.local/privacy/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-15 08:50:32', '2019-05-15 08:50:32'),
(147, 'Add Privacy & Policy By Super Admin', 'http://apz.local/privacy/add', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, '1557910418.PNG', '2019-05-15 08:53:38', '2019-05-15 08:53:38'),
(148, 'Notification Add By Super Admin', 'http://apz.local/notification-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'sdf', NULL, '2019-05-15 09:17:31', '2019-05-15 09:17:31'),
(149, '1 No. Notification Status Change By Super Admin', 'http://apz.local/notification-status/1', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'sdf', NULL, '2019-05-15 09:18:35', '2019-05-15 09:18:35'),
(150, '1 No. Notification Delete By Super Admin', 'http://apz.local/notification-delete/1', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, NULL, NULL, '2019-05-15 09:18:43', '2019-05-15 09:18:43'),
(151, 'Send Message To Test main Test main', 'http://apz.local/send_user_message', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'vvvvv', NULL, '2019-05-15 09:19:47', '2019-05-15 09:19:47'),
(152, 'Notification Add By Super Admin', 'http://apz.local/notification-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification', NULL, '2019-05-15 09:30:04', '2019-05-15 09:30:04'),
(153, '2 No. Notification Update By Super Admin', 'http://apz.local/notification-update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification', NULL, '2019-05-15 09:30:22', '2019-05-15 09:30:22'),
(154, '2 No. Notification Update By Super Admin', 'http://apz.local/notification-update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification', NULL, '2019-05-15 09:30:33', '2019-05-15 09:30:33'),
(155, '2 No. Notification Status Change By Super Admin', 'http://apz.local/notification-status/2', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification', NULL, '2019-05-15 09:30:41', '2019-05-15 09:30:41'),
(156, '2 No. Notification Status Change By Super Admin', 'http://apz.local/notification-status/2', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification', NULL, '2019-05-15 09:30:45', '2019-05-15 09:30:45'),
(157, 'Notification Add By Super Admin', 'http://apz.local/notification-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification', NULL, '2019-05-15 09:51:42', '2019-05-15 09:51:42'),
(158, 'Notification Add By Super Admin', 'http://apz.local/notification-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification 5555', NULL, '2019-05-15 09:56:58', '2019-05-15 09:56:58'),
(159, 'Notification Add By Super Admin', 'http://apz.local/notification-submit', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, '789', NULL, '2019-05-15 10:28:34', '2019-05-15 10:28:34'),
(160, '3 No. Notification Status Change By Super Admin', 'http://apz.local/notification-status/3', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, '789', NULL, '2019-05-15 10:29:59', '2019-05-15 10:29:59'),
(161, '4 No. Notification Status Change By Super Admin', 'http://apz.local/notification-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification 5555', NULL, '2019-05-15 10:43:59', '2019-05-15 10:43:59'),
(162, '4 No. Notification Status Change By Super Admin', 'http://apz.local/notification-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification 5555', NULL, '2019-05-15 10:44:08', '2019-05-15 10:44:08'),
(163, '4 No. Notification Status Change By Super Admin', 'http://apz.local/notification-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification 5555', NULL, '2019-05-15 10:47:48', '2019-05-15 10:47:48'),
(164, '4 No. Notification Status Change By Super Admin', 'http://apz.local/notification-status-change', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'Test Noltification 5555', NULL, '2019-05-15 10:48:28', '2019-05-15 10:48:28'),
(165, 'Send Message To Test main Test main', 'http://apz.local/send_user_message', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'dfgdfgdfgdfg', NULL, '2019-05-15 11:10:43', '2019-05-15 11:10:43'),
(166, 'Message Send Successfull', 'http://apz.local/send_user_message', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'd', NULL, '2019-05-15 11:13:07', '2019-05-15 11:13:07'),
(167, 'Message Send Successfull', 'http://apz.local/send_user_message', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', 31, 'dsfdsfdsf', NULL, '2019-05-15 11:16:27', '2019-05-15 11:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `as_agent_commission`
--

CREATE TABLE `as_agent_commission` (
  `commission_id` int(10) UNSIGNED NOT NULL,
  `agent_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `previous_rate` double(10,2) DEFAULT NULL,
  `new_rate` double(10,2) DEFAULT NULL,
  `commission_note` mediumtext COLLATE utf8mb4_unicode_ci,
  `document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_agent_commission`
--

INSERT INTO `as_agent_commission` (`commission_id`, `agent_id`, `previous_rate`, `new_rate`, `commission_note`, `document`, `created_at`, `updated_at`) VALUES
(1, '4', 20.50, 30.50, 'dfgjdnfgkdnfgnkdnfgkgfnbnknbgfn\r\ndfmg,dfm,', NULL, '2019-04-10 18:00:00', NULL),
(2, '4', 20.50, 35.50, 'dfgjdnfgkdnfgnkdnfgkgfnbnknbgfn\r\ndfmg,dfm,', NULL, '2019-04-10 18:00:00', NULL),
(3, '999', 20.50, 35.50, 'dfgjdnfgkdnfgnkdnfgkgfnbnknbgfn\r\ndfmg,dfm,', NULL, '2019-04-10 18:00:00', NULL),
(4, '4', 35.50, 100.00, '100', NULL, '2019-04-24 12:20:45', '2019-04-24 12:20:45'),
(5, '4', 100.00, 25.00, '554131313', '516165', '2019-04-25 10:35:14', '2019-04-25 10:35:14'),
(6, '4', 25.00, 10.00, 'dsfdsf', '1556341791.png', '2019-04-27 05:09:51', '2019-04-27 05:09:51'),
(7, '4', 10.00, 50.00, 'asd', NULL, '2019-05-02 11:41:17', '2019-05-02 11:41:17'),
(8, '4', 50.00, 15.00, '123', NULL, '2019-05-02 11:49:47', '2019-05-02 11:49:47'),
(9, '4', 50.00, 15.00, '123', NULL, '2019-05-02 11:50:27', '2019-05-02 11:50:27'),
(10, '4', 50.00, 15.00, '123', NULL, '2019-05-02 11:50:56', '2019-05-02 11:50:56'),
(11, '4', 50.00, 15.00, '123', NULL, '2019-05-02 11:51:03', '2019-05-02 11:51:03'),
(12, '4', 15.00, 56.00, '56', NULL, '2019-05-02 11:51:43', '2019-05-02 11:51:43'),
(13, '4', 15.00, 56.00, '56', NULL, '2019-05-02 11:51:53', '2019-05-02 11:51:53'),
(14, '4', 15.00, 56.00, '56', NULL, '2019-05-02 11:52:24', '2019-05-02 11:52:24'),
(15, '4', 56.00, 44.00, 'f', NULL, '2019-05-02 11:52:33', '2019-05-02 11:52:33'),
(16, '4', 44.00, 50.00, '1425', NULL, '2019-05-04 11:46:43', '2019-05-04 11:46:43'),
(17, '4', 50.00, 10.00, '5', NULL, '2019-05-04 11:48:18', '2019-05-04 11:48:18'),
(18, '4', 10.00, 100.00, 'h', NULL, '2019-05-04 11:49:02', '2019-05-04 11:49:02'),
(19, '4', 100.00, 15.00, 'asdas', NULL, '2019-05-04 11:50:59', '2019-05-04 11:50:59'),
(20, '4', 15.00, 20.00, 'sdfds', NULL, '2019-05-04 11:53:19', '2019-05-04 11:53:19'),
(21, '4', 20.00, 100.00, '23', NULL, '2019-05-04 12:01:07', '2019-05-04 12:01:07'),
(22, '4', 100.00, 25.00, 'asd', NULL, '2019-05-04 12:02:10', '2019-05-04 12:02:10'),
(23, '4', 25.00, 15.00, 'asca', NULL, '2019-05-05 08:39:49', '2019-05-05 08:39:49'),
(24, '4', 15.00, 10.00, 'TEST', '1557128483.png', '2019-05-06 07:41:23', '2019-05-06 07:41:23'),
(25, '4', 10.00, 15.00, '123', '0', '2019-05-06 10:13:58', '2019-05-06 10:13:58'),
(26, '4', 15.00, 50.00, 'hjhk', '1557832498.PNG', '2019-05-14 11:14:58', '2019-05-14 11:14:58');

-- --------------------------------------------------------

--
-- Table structure for table `as_agent_payment`
--

CREATE TABLE `as_agent_payment` (
  `agent_payment_id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` enum('receive','withdraw') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribe_id` int(11) DEFAULT NULL,
  `subscribe_payment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_amount` decimal(20,2) DEFAULT NULL,
  `payment_charge` decimal(20,2) DEFAULT NULL,
  `payment_details` mediumtext COLLATE utf8mb4_unicode_ci,
  `pay_date` datetime DEFAULT NULL,
  `pay_document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` enum('paid','due','cancel') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_agent_payment`
--

INSERT INTO `as_agent_payment` (`agent_payment_id`, `user_id`, `agent_id`, `payment_type`, `subscribe_id`, `subscribe_payment_id`, `payment_date`, `payment_method`, `payment_amount`, `payment_charge`, `payment_details`, `pay_date`, `pay_document`, `pay_note`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, '24', '4', 'receive', 1, '1', '2019-05-23 00:00:00', 'Gateway', '500.00', '10.00', '5665565656', '2019-05-13 13:35:25', '1557732925.txt', '3333333333333333', 'paid', '2019-05-07 18:00:00', '2019-05-13 07:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `as_country`
--

CREATE TABLE `as_country` (
  `id` int(11) NOT NULL,
  `code` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL,
  `currency_name` varchar(20) NOT NULL,
  `currency_symbol` varchar(20) NOT NULL,
  `currency_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `as_country`
--

INSERT INTO `as_country` (`id`, `code`, `name`, `phonecode`, `currency_name`, `currency_symbol`, `currency_code`) VALUES
(1, 'AF', 'Afghanistan', 93, 'Afghan afghani', '؋', 'AFN'),
(2, 'AL', 'Albania', 355, 'Albanian lek', 'L', 'ALL'),
(3, 'DZ', 'Algeria', 213, 'Algerian dinar', 'د.ج', 'DZD'),
(4, 'AS', 'American Samoa', 1684, '', '', ''),
(5, 'AD', 'Andorra', 376, 'Euro', '€', 'EUR'),
(6, 'AO', 'Angola', 244, 'Angolan kwanza', 'Kz', 'AOA'),
(7, 'AI', 'Anguilla', 1264, 'East Caribbean dolla', '$', 'XCD'),
(8, 'AQ', 'Antarctica', 0, '', '', ''),
(9, 'AG', 'Antigua And Barbuda', 1268, 'East Caribbean dolla', '$', 'XCD'),
(10, 'AR', 'Argentina', 54, 'Argentine peso', '$', 'ARS'),
(11, 'AM', 'Armenia', 374, 'Armenian dram', '', 'AMD'),
(12, 'AW', 'Aruba', 297, 'Aruban florin', 'ƒ', 'AWG'),
(13, 'AU', 'Australia', 61, 'Australian dollar', '$', 'AUD'),
(14, 'AT', 'Austria', 43, 'Euro', '€', 'EUR'),
(15, 'AZ', 'Azerbaijan', 994, 'Azerbaijani manat', '', 'AZN'),
(16, 'BS', 'Bahamas The', 1242, '', '', ''),
(17, 'BH', 'Bahrain', 973, 'Bahraini dinar', '.د.ب', 'BHD'),
(18, 'BD', 'Bangladesh', 88, 'Bangladeshi taka', '৳', 'BDT'),
(19, 'BB', 'Barbados', 1246, 'Barbadian dollar', '$', 'BBD'),
(20, 'BY', 'Belarus', 375, 'Belarusian ruble', 'Br', 'BYR'),
(21, 'BE', 'Belgium', 32, 'Euro', '€', 'EUR'),
(22, 'BZ', 'Belize', 501, 'Belize dollar', '$', 'BZD'),
(23, 'BJ', 'Benin', 229, 'West African CFA fra', 'Fr', 'XOF'),
(24, 'BM', 'Bermuda', 1441, 'Bermudian dollar', '$', 'BMD'),
(25, 'BT', 'Bhutan', 975, 'Bhutanese ngultrum', 'Nu.', 'BTN'),
(26, 'BO', 'Bolivia', 591, 'Bolivian boliviano', 'Bs.', 'BOB'),
(27, 'BA', 'Bosnia and Herzegovina', 387, 'Bosnia and Herzegovi', 'KM or КМ', 'BAM'),
(28, 'BW', 'Botswana', 267, 'Botswana pula', 'P', 'BWP'),
(29, 'BV', 'Bouvet Island', 0, '', '', ''),
(30, 'BR', 'Brazil', 55, 'Brazilian real', 'R$', 'BRL'),
(31, 'IO', 'British Indian Ocean Territory', 246, 'United States dollar', '$', 'USD'),
(32, 'BN', 'Brunei', 673, 'Brunei dollar', '$', 'BND'),
(33, 'BG', 'Bulgaria', 359, 'Bulgarian lev', 'лв', 'BGN'),
(34, 'BF', 'Burkina Faso', 226, 'West African CFA fra', 'Fr', 'XOF'),
(35, 'BI', 'Burundi', 257, 'Burundian franc', 'Fr', 'BIF'),
(36, 'KH', 'Cambodia', 855, 'Cambodian riel', '៛', 'KHR'),
(37, 'CM', 'Cameroon', 237, 'Central African CFA ', 'Fr', 'XAF'),
(38, 'CA', 'Canada', 1, 'Canadian dollar', '$', 'CAD'),
(39, 'CV', 'Cape Verde', 238, 'Cape Verdean escudo', 'Esc or $', 'CVE'),
(40, 'KY', 'Cayman Islands', 1345, 'Cayman Islands dolla', '$', 'KYD'),
(41, 'CF', 'Central African Republic', 236, 'Central African CFA ', 'Fr', 'XAF'),
(42, 'TD', 'Chad', 235, 'Central African CFA ', 'Fr', 'XAF'),
(43, 'CL', 'Chile', 56, 'Chilean peso', '$', 'CLP'),
(44, 'CN', 'China', 86, 'Chinese yuan', '¥ or 元', 'CNY'),
(45, 'CX', 'Christmas Island', 61, '', '', ''),
(46, 'CC', 'Cocos (Keeling) Islands', 672, 'Australian dollar', '$', 'AUD'),
(47, 'CO', 'Colombia', 57, 'Colombian peso', '$', 'COP'),
(48, 'KM', 'Comoros', 269, 'Comorian franc', 'Fr', 'KMF'),
(49, 'CG', 'Congo', 242, '', '', ''),
(50, 'CD', 'Congo The Democratic Republic Of The', 242, '', '', ''),
(51, 'CK', 'Cook Islands', 682, 'New Zealand dollar', '$', 'NZD'),
(52, 'CR', 'Costa Rica', 506, 'Costa Rican colón', '₡', 'CRC'),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225, '', '', ''),
(54, 'HR', 'Croatia (Hrvatska)', 385, '', '', ''),
(55, 'CU', 'Cuba', 53, 'Cuban convertible pe', '$', 'CUC'),
(56, 'CY', 'Cyprus', 357, 'Euro', '€', 'EUR'),
(57, 'CZ', 'Czech Republic', 420, 'Czech koruna', 'Kč', 'CZK'),
(58, 'DK', 'Denmark', 45, 'Danish krone', 'kr', 'DKK'),
(59, 'DJ', 'Djibouti', 253, 'Djiboutian franc', 'Fr', 'DJF'),
(60, 'DM', 'Dominica', 1767, 'East Caribbean dolla', '$', 'XCD'),
(61, 'DO', 'Dominican Republic', 1809, 'Dominican peso', '$', 'DOP'),
(62, 'TP', 'East Timor', 670, 'United States dollar', '$', 'USD'),
(63, 'EC', 'Ecuador', 593, 'United States dollar', '$', 'USD'),
(64, 'EG', 'Egypt', 20, 'Egyptian pound', '£ or ج.م', 'EGP'),
(65, 'SV', 'El Salvador', 503, 'United States dollar', '$', 'USD'),
(66, 'GQ', 'Equatorial Guinea', 240, 'Central African CFA ', 'Fr', 'XAF'),
(67, 'ER', 'Eritrea', 291, 'Eritrean nakfa', 'Nfk', 'ERN'),
(68, 'EE', 'Estonia', 372, 'Euro', '€', 'EUR'),
(69, 'ET', 'Ethiopia', 251, 'Ethiopian birr', 'Br', 'ETB'),
(70, 'XA', 'External Territories of Australia', 61, '', '', ''),
(71, 'FK', 'Falkland Islands', 500, 'Falkland Islands pou', '£', 'FKP'),
(72, 'FO', 'Faroe Islands', 298, 'Danish krone', 'kr', 'DKK'),
(73, 'FJ', 'Fiji Islands', 679, '', '', ''),
(74, 'FI', 'Finland', 358, 'Euro', '€', 'EUR'),
(75, 'FR', 'France', 33, 'Euro', '€', 'EUR'),
(76, 'GF', 'French Guiana', 594, '', '', ''),
(77, 'PF', 'French Polynesia', 689, 'CFP franc', 'Fr', 'XPF'),
(78, 'TF', 'French Southern Territories', 0, '', '', ''),
(79, 'GA', 'Gabon', 241, 'Central African CFA ', 'Fr', 'XAF'),
(80, 'GM', 'Gambia The', 220, '', '', ''),
(81, 'GE', 'Georgia', 995, 'Georgian lari', 'ლ', 'GEL'),
(82, 'DE', 'Germany', 49, 'Euro', '€', 'EUR'),
(83, 'GH', 'Ghana', 233, 'Ghana cedi', '₵', 'GHS'),
(84, 'GI', 'Gibraltar', 350, 'Gibraltar pound', '£', 'GIP'),
(85, 'GR', 'Greece', 30, 'Euro', '€', 'EUR'),
(86, 'GL', 'Greenland', 299, '', '', ''),
(87, 'GD', 'Grenada', 1473, 'East Caribbean dolla', '$', 'XCD'),
(88, 'GP', 'Guadeloupe', 590, '', '', ''),
(89, 'GU', 'Guam', 1671, '', '', ''),
(90, 'GT', 'Guatemala', 502, 'Guatemalan quetzal', 'Q', 'GTQ'),
(91, 'XU', 'Guernsey and Alderney', 44, '', '', ''),
(92, 'GN', 'Guinea', 224, 'Guinean franc', 'Fr', 'GNF'),
(93, 'GW', 'Guinea-Bissau', 245, 'West African CFA fra', 'Fr', 'XOF'),
(94, 'GY', 'Guyana', 592, 'Guyanese dollar', '$', 'GYD'),
(95, 'HT', 'Haiti', 509, 'Haitian gourde', 'G', 'HTG'),
(96, 'HM', 'Heard and McDonald Islands', 0, '', '', ''),
(97, 'HN', 'Honduras', 504, 'Honduran lempira', 'L', 'HNL'),
(98, 'HK', 'Hong Kong S.A.R.', 852, '', '', ''),
(99, 'HU', 'Hungary', 36, 'Hungarian forint', 'Ft', 'HUF'),
(100, 'IS', 'Iceland', 354, 'Icelandic króna', 'kr', 'ISK'),
(101, 'IN', 'India', 91, 'Indian rupee', '₹', 'INR'),
(102, 'ID', 'Indonesia', 62, 'Indonesian rupiah', 'Rp', 'IDR'),
(103, 'IR', 'Iran', 98, 'Iranian rial', '﷼', 'IRR'),
(104, 'IQ', 'Iraq', 964, 'Iraqi dinar', 'ع.د', 'IQD'),
(105, 'IE', 'Ireland', 353, 'Euro', '€', 'EUR'),
(106, 'IL', 'Israel', 972, 'Israeli new shekel', '₪', 'ILS'),
(107, 'IT', 'Italy', 39, 'Euro', '€', 'EUR'),
(108, 'JM', 'Jamaica', 1876, 'Jamaican dollar', '$', 'JMD'),
(109, 'JP', 'Japan', 81, 'Japanese yen', '¥', 'JPY'),
(110, 'XJ', 'Jersey', 44, 'British pound', '£', 'GBP'),
(111, 'JO', 'Jordan', 962, 'Jordanian dinar', 'د.ا', 'JOD'),
(112, 'KZ', 'Kazakhstan', 7, 'Kazakhstani tenge', '', 'KZT'),
(113, 'KE', 'Kenya', 254, 'Kenyan shilling', 'Sh', 'KES'),
(114, 'KI', 'Kiribati', 686, 'Australian dollar', '$', 'AUD'),
(115, 'KP', 'Korea North', 850, '', '', ''),
(116, 'KR', 'Korea South', 82, '', '', ''),
(117, 'KW', 'Kuwait', 965, 'Kuwaiti dinar', 'د.ك', 'KWD'),
(118, 'KG', 'Kyrgyzstan', 996, 'Kyrgyzstani som', 'лв', 'KGS'),
(119, 'LA', 'Laos', 856, 'Lao kip', '₭', 'LAK'),
(120, 'LV', 'Latvia', 371, 'Euro', '€', 'EUR'),
(121, 'LB', 'Lebanon', 961, 'Lebanese pound', 'ل.ل', 'LBP'),
(122, 'LS', 'Lesotho', 266, 'Lesotho loti', 'L', 'LSL'),
(123, 'LR', 'Liberia', 231, 'Liberian dollar', '$', 'LRD'),
(124, 'LY', 'Libya', 218, 'Libyan dinar', 'ل.د', 'LYD'),
(125, 'LI', 'Liechtenstein', 423, 'Swiss franc', 'Fr', 'CHF'),
(126, 'LT', 'Lithuania', 370, 'Euro', '€', 'EUR'),
(127, 'LU', 'Luxembourg', 352, 'Euro', '€', 'EUR'),
(128, 'MO', 'Macau S.A.R.', 853, '', '', ''),
(129, 'MK', 'Macedonia', 389, '', '', ''),
(130, 'MG', 'Madagascar', 261, 'Malagasy ariary', 'Ar', 'MGA'),
(131, 'MW', 'Malawi', 265, 'Malawian kwacha', 'MK', 'MWK'),
(132, 'MY', 'Malaysia', 60, 'Malaysian ringgit', 'RM', 'MYR'),
(133, 'MV', 'Maldives', 960, 'Maldivian rufiyaa', '.ރ', 'MVR'),
(134, 'ML', 'Mali', 223, 'West African CFA fra', 'Fr', 'XOF'),
(135, 'MT', 'Malta', 356, 'Euro', '€', 'EUR'),
(136, 'XM', 'Man (Isle of)', 44, '', '', ''),
(137, 'MH', 'Marshall Islands', 692, 'United States dollar', '$', 'USD'),
(138, 'MQ', 'Martinique', 596, '', '', ''),
(139, 'MR', 'Mauritania', 222, 'Mauritanian ouguiya', 'UM', 'MRO'),
(140, 'MU', 'Mauritius', 230, 'Mauritian rupee', '₨', 'MUR'),
(141, 'YT', 'Mayotte', 269, '', '', ''),
(142, 'MX', 'Mexico', 52, 'Mexican peso', '$', 'MXN'),
(143, 'FM', 'Micronesia', 691, 'Micronesian dollar', '$', ''),
(144, 'MD', 'Moldova', 373, 'Moldovan leu', 'L', 'MDL'),
(145, 'MC', 'Monaco', 377, 'Euro', '€', 'EUR'),
(146, 'MN', 'Mongolia', 976, 'Mongolian tögrög', '₮', 'MNT'),
(147, 'MS', 'Montserrat', 1664, 'East Caribbean dolla', '$', 'XCD'),
(148, 'MA', 'Morocco', 212, 'Moroccan dirham', 'د.م.', 'MAD'),
(149, 'MZ', 'Mozambique', 258, 'Mozambican metical', 'MT', 'MZN'),
(150, 'MM', 'Myanmar', 95, 'Burmese kyat', 'Ks', 'MMK'),
(151, 'NA', 'Namibia', 264, 'Namibian dollar', '$', 'NAD'),
(152, 'NR', 'Nauru', 674, 'Australian dollar', '$', 'AUD'),
(153, 'NP', 'Nepal', 977, 'Nepalese rupee', '₨', 'NPR'),
(154, 'AN', 'Netherlands Antilles', 599, '', '', ''),
(155, 'NL', 'Netherlands The', 31, '', '', ''),
(156, 'NC', 'New Caledonia', 687, 'CFP franc', 'Fr', 'XPF'),
(157, 'NZ', 'New Zealand', 64, 'New Zealand dollar', '$', 'NZD'),
(158, 'NI', 'Nicaragua', 505, 'Nicaraguan córdoba', 'C$', 'NIO'),
(159, 'NE', 'Niger', 227, 'West African CFA fra', 'Fr', 'XOF'),
(160, 'NG', 'Nigeria', 234, 'Nigerian naira', '₦', 'NGN'),
(161, 'NU', 'Niue', 683, 'New Zealand dollar', '$', 'NZD'),
(162, 'NF', 'Norfolk Island', 672, '', '', ''),
(163, 'MP', 'Northern Mariana Islands', 1670, '', '', ''),
(164, 'NO', 'Norway', 47, 'Norwegian krone', 'kr', 'NOK'),
(165, 'OM', 'Oman', 968, 'Omani rial', 'ر.ع.', 'OMR'),
(166, 'PK', 'Pakistan', 92, 'Pakistani rupee', '₨', 'PKR'),
(167, 'PW', 'Palau', 680, 'Palauan dollar', '$', ''),
(168, 'PS', 'Palestinian Territory Occupied', 970, '', '', ''),
(169, 'PA', 'Panama', 507, 'Panamanian balboa', 'B/.', 'PAB'),
(170, 'PG', 'Papua new Guinea', 675, 'Papua New Guinean ki', 'K', 'PGK'),
(171, 'PY', 'Paraguay', 595, 'Paraguayan guaraní', '₲', 'PYG'),
(172, 'PE', 'Peru', 51, 'Peruvian nuevo sol', 'S/.', 'PEN'),
(173, 'PH', 'Philippines', 63, 'Philippine peso', '₱', 'PHP'),
(174, 'PN', 'Pitcairn Island', 0, '', '', ''),
(175, 'PL', 'Poland', 48, 'Polish złoty', 'zł', 'PLN'),
(176, 'PT', 'Portugal', 351, 'Euro', '€', 'EUR'),
(177, 'PR', 'Puerto Rico', 1787, '', '', ''),
(178, 'QA', 'Qatar', 974, 'Qatari riyal', 'ر.ق', 'QAR'),
(179, 'RE', 'Reunion', 262, '', '', ''),
(180, 'RO', 'Romania', 40, 'Romanian leu', 'lei', 'RON'),
(181, 'RU', 'Russia', 70, 'Russian ruble', '', 'RUB'),
(182, 'RW', 'Rwanda', 250, 'Rwandan franc', 'Fr', 'RWF'),
(183, 'SH', 'Saint Helena', 290, 'Saint Helena pound', '£', 'SHP'),
(184, 'KN', 'Saint Kitts And Nevis', 1869, 'East Caribbean dolla', '$', 'XCD'),
(185, 'LC', 'Saint Lucia', 1758, 'East Caribbean dolla', '$', 'XCD'),
(186, 'PM', 'Saint Pierre and Miquelon', 508, '', '', ''),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784, 'East Caribbean dolla', '$', 'XCD'),
(188, 'WS', 'Samoa', 684, 'Samoan tālā', 'T', 'WST'),
(189, 'SM', 'San Marino', 378, 'Euro', '€', 'EUR'),
(190, 'ST', 'Sao Tome and Principe', 239, 'São Tomé and Príncip', 'Db', 'STD'),
(191, 'SA', 'Saudi Arabia', 966, 'Saudi riyal', 'ر.س', 'SAR'),
(192, 'SN', 'Senegal', 221, 'West African CFA fra', 'Fr', 'XOF'),
(193, 'RS', 'Serbia', 381, 'Serbian dinar', 'дин. or din.', 'RSD'),
(194, 'SC', 'Seychelles', 248, 'Seychellois rupee', '₨', 'SCR'),
(195, 'SL', 'Sierra Leone', 232, 'Sierra Leonean leone', 'Le', 'SLL'),
(196, 'SG', 'Singapore', 65, 'Brunei dollar', '$', 'BND'),
(197, 'SK', 'Slovakia', 421, 'Euro', '€', 'EUR'),
(198, 'SI', 'Slovenia', 386, 'Euro', '€', 'EUR'),
(199, 'XG', 'Smaller Territories of the UK', 44, '', '', ''),
(200, 'SB', 'Solomon Islands', 677, 'Solomon Islands doll', '$', 'SBD'),
(201, 'SO', 'Somalia', 252, 'Somali shilling', 'Sh', 'SOS'),
(202, 'ZA', 'South Africa', 27, 'South African rand', 'R', 'ZAR'),
(203, 'GS', 'South Georgia', 0, '', '', ''),
(204, 'SS', 'South Sudan', 211, 'South Sudanese pound', '£', 'SSP'),
(205, 'ES', 'Spain', 34, 'Euro', '€', 'EUR'),
(206, 'LK', 'Sri Lanka', 94, 'Sri Lankan rupee', 'Rs or රු', 'LKR'),
(207, 'SD', 'Sudan', 249, 'Sudanese pound', 'ج.س.', 'SDG'),
(208, 'SR', 'Suriname', 597, 'Surinamese dollar', '$', 'SRD'),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47, '', '', ''),
(210, 'SZ', 'Swaziland', 268, 'Swazi lilangeni', 'L', 'SZL'),
(211, 'SE', 'Sweden', 46, 'Swedish krona', 'kr', 'SEK'),
(212, 'CH', 'Switzerland', 41, 'Swiss franc', 'Fr', 'CHF'),
(213, 'SY', 'Syria', 963, 'Syrian pound', '£ or ل.س', 'SYP'),
(214, 'TW', 'Taiwan', 886, 'New Taiwan dollar', '$', 'TWD'),
(215, 'TJ', 'Tajikistan', 992, 'Tajikistani somoni', 'ЅМ', 'TJS'),
(216, 'TZ', 'Tanzania', 255, 'Tanzanian shilling', 'Sh', 'TZS'),
(217, 'TH', 'Thailand', 66, 'Thai baht', '฿', 'THB'),
(218, 'TG', 'Togo', 228, 'West African CFA fra', 'Fr', 'XOF'),
(219, 'TK', 'Tokelau', 690, '', '', ''),
(220, 'TO', 'Tonga', 676, 'Tongan paʻanga', 'T$', 'TOP'),
(221, 'TT', 'Trinidad And Tobago', 1868, 'Trinidad and Tobago ', '$', 'TTD'),
(222, 'TN', 'Tunisia', 216, 'Tunisian dinar', 'د.ت', 'TND'),
(223, 'TR', 'Turkey', 90, 'Turkish lira', '', 'TRY'),
(224, 'TM', 'Turkmenistan', 7370, 'Turkmenistan manat', 'm', 'TMT'),
(225, 'TC', 'Turks And Caicos Islands', 1649, 'United States dollar', '$', 'USD'),
(226, 'TV', 'Tuvalu', 688, 'Australian dollar', '$', 'AUD'),
(227, 'UG', 'Uganda', 256, 'Ugandan shilling', 'Sh', 'UGX'),
(228, 'UA', 'Ukraine', 380, 'Ukrainian hryvnia', '₴', 'UAH'),
(229, 'AE', 'United Arab Emirates', 971, 'United Arab Emirates', 'د.إ', 'AED'),
(230, 'GB', 'United Kingdom', 44, 'British pound', '£', 'GBP'),
(231, 'US', 'United States', 1, 'United States dollar', '$', 'USD'),
(232, 'UM', 'United States Minor Outlying Islands', 1, '', '', ''),
(233, 'UY', 'Uruguay', 598, 'Uruguayan peso', '$', 'UYU'),
(234, 'UZ', 'Uzbekistan', 998, 'Uzbekistani som', '', 'UZS'),
(235, 'VU', 'Vanuatu', 678, 'Vanuatu vatu', 'Vt', 'VUV'),
(236, 'VA', 'Vatican City State (Holy See)', 39, '', '', ''),
(237, 'VE', 'Venezuela', 58, 'Venezuelan bolívar', 'Bs F', 'VEF'),
(238, 'VN', 'Vietnam', 84, 'Vietnamese đồng', '₫', 'VND'),
(239, 'VG', 'Virgin Islands (British)', 1284, '', '', ''),
(240, 'VI', 'Virgin Islands (US)', 1340, '', '', ''),
(241, 'WF', 'Wallis And Futuna Islands', 681, '', '', ''),
(242, 'EH', 'Western Sahara', 212, '', '', ''),
(243, 'YE', 'Yemen', 967, 'Yemeni rial', '﷼', 'YER'),
(244, 'YU', 'Yugoslavia', 38, '', '', ''),
(245, 'ZM', 'Zambia', 260, 'Zambian kwacha', 'ZK', 'ZMW'),
(246, 'ZW', 'Zimbabwe', 263, 'Botswana pula', 'P', 'BWP');

-- --------------------------------------------------------

--
-- Table structure for table `as_desktop_soft_version`
--

CREATE TABLE `as_desktop_soft_version` (
  `id` int(10) UNSIGNED NOT NULL,
  `software_id` int(11) NOT NULL,
  `software_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_ver` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `as_invoices`
--

CREATE TABLE `as_invoices` (
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribe_start_date` datetime DEFAULT NULL,
  `subscribe_end_date` datetime DEFAULT NULL,
  `invoice_amount` decimal(20,2) DEFAULT NULL,
  `invoice_discount` decimal(20,2) DEFAULT NULL,
  `invoice_charge` decimal(20,2) DEFAULT NULL,
  `invoice_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribe_month` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `invoice_paid_date` datetime DEFAULT NULL,
  `invoice_status` enum('paid','due','cancel') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_invoices`
--

INSERT INTO `as_invoices` (`invoice_id`, `user_id`, `agent_id`, `subscribe_id`, `subscribe_start_date`, `subscribe_end_date`, `invoice_amount`, `invoice_discount`, `invoice_charge`, `invoice_transaction_id`, `subscribe_month`, `created_at`, `invoice_paid_date`, `invoice_status`) VALUES
(1, '24', '1', '1', '2019-05-16 10:18:33', '2019-05-29 12:00:19', '200.00', NULL, '10.00', NULL, '1', '2019-05-17 00:00:00', NULL, 'paid');

-- --------------------------------------------------------

--
-- Table structure for table `as_login_attempts`
--

CREATE TABLE `as_login_attempts` (
  `id_login_attempts` int(10) UNSIGNED NOT NULL,
  `ip_addr` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempt_number` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `as_notification`
--

CREATE TABLE `as_notification` (
  `notification_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` mediumtext COLLATE utf8mb4_unicode_ci,
  `link` mediumtext COLLATE utf8mb4_unicode_ci,
  `status` enum('active','deactive') COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `user_id` int(11) DEFAULT NULL,
  `notification_type` enum('user','admin') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_notification`
--

INSERT INTO `as_notification` (`notification_id`, `title`, `message`, `link`, `status`, `user_id`, `notification_type`, `created_at`, `updated_at`) VALUES
(2, 'Test Noltification', 'Test message', 'bitbyteplay.com/cv', 'active', NULL, NULL, '2019-05-15 09:30:04', '2019-05-15 09:30:04'),
(3, '789', '96365', 'bitbyteplay.com/cv', 'deactive', NULL, NULL, '2019-05-15 09:51:42', '2019-05-15 10:28:34'),
(4, 'Test Noltification 5555', 'fdsgdfgsdfg', 'sdgdfgsdgf dsfgsdg', 'deactive', NULL, NULL, '2019-05-15 09:56:58', '2019-05-15 10:48:28');

-- --------------------------------------------------------

--
-- Table structure for table `as_payment`
--

CREATE TABLE `as_payment` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `payment_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` enum('office','gateway','voucher','system') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_load_date` date DEFAULT NULL,
  `voucher_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_payment_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_amount` decimal(20,2) DEFAULT NULL,
  `payment_currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_charge` decimal(20,2) DEFAULT NULL,
  `payment_discount` decimal(20,2) DEFAULT NULL,
  `payment_total_amount` decimal(20,2) DEFAULT NULL,
  `payment_request_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_txn_msg` mediumtext COLLATE utf8mb4_unicode_ci,
  `payment_txn_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_txn_details` mediumtext COLLATE utf8mb4_unicode_ci,
  `payment_card_details` mediumtext COLLATE utf8mb4_unicode_ci,
  `payment_card` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_request_time` datetime DEFAULT NULL,
  `payment_time` datetime DEFAULT NULL,
  `payment_ref_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_note` mediumtext COLLATE utf8mb4_unicode_ci,
  `payment_status` enum('paid','due','cancel','hole') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_payment`
--

INSERT INTO `as_payment` (`payment_id`, `payment_transaction_id`, `payment_type`, `payment_load_date`, `voucher_id`, `user_id`, `office_payment_by`, `payment_token`, `payment_amount`, `payment_currency`, `payment_charge`, `payment_discount`, `payment_total_amount`, `payment_request_ip`, `payment_txn_msg`, `payment_txn_status`, `payment_txn_details`, `payment_card_details`, `payment_card`, `card_code`, `payment_method`, `payment_request_time`, `payment_time`, `payment_ref_id`, `payment_note`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, '12', 'gateway', '2019-05-15', NULL, '24', NULL, NULL, '500.00', NULL, '10.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid', '2019-05-21 18:00:00', '2019-05-10 18:00:00'),
(2, NULL, 'office', '2019-05-25', NULL, '24', '1', NULL, '200.00', NULL, '10.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dfighdfkjgdfkjn', 'paid', '2019-05-12 10:07:40', '2019-05-12 10:07:40'),
(3, NULL, 'office', '2019-05-25', NULL, '24', '1', NULL, '200.00', NULL, '10.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dfighdfkjgdfkjn', 'paid', '2019-05-12 10:08:39', '2019-05-12 10:08:39'),
(4, NULL, 'office', '2019-05-30', NULL, '24', '1', NULL, '5.00', NULL, '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dfgdf', 'paid', '2019-05-12 11:11:34', '2019-05-12 11:11:34'),
(5, NULL, 'office', '2019-05-16', NULL, '24', '1', NULL, '20.00', NULL, '5.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid', '2019-05-13 05:50:30', '2019-05-13 05:50:30'),
(6, NULL, 'office', '2019-05-23', NULL, '24', '1', NULL, '100.00', NULL, '5.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid', '2019-05-13 05:53:30', '2019-05-13 05:53:30'),
(7, NULL, 'office', '2019-05-23', NULL, '24', '1', NULL, '100.00', NULL, '5.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid', '2019-05-13 05:53:36', '2019-05-13 05:53:36'),
(8, NULL, 'office', '2019-05-23', NULL, '24', '1', NULL, '100.00', NULL, '5.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid', '2019-05-13 05:53:41', '2019-05-13 05:53:41'),
(9, NULL, 'office', '2019-05-23', NULL, '24', '1', NULL, '100.00', NULL, '5.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid', '2019-05-13 05:54:03', '2019-05-13 05:54:03'),
(10, NULL, 'office', '2019-05-23', NULL, '24', '1', NULL, '100.00', NULL, '5.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid', '2019-05-13 05:55:11', '2019-05-13 05:55:11'),
(11, NULL, 'office', '2019-05-09', NULL, '24', '31', NULL, '100.00', NULL, '4.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'paid', '2019-05-15 08:43:21', '2019-05-15 08:43:21');

-- --------------------------------------------------------

--
-- Table structure for table `as_payment_method`
--

CREATE TABLE `as_payment_method` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_method_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minimum_amount` double(10,2) NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `as_plugins`
--

CREATE TABLE `as_plugins` (
  `plugins_id` int(10) UNSIGNED NOT NULL,
  `plugins_type` enum('public','private','custom') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_billing` enum('free','monthly','onetime','yearly') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_billing_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_billing_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_unique_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plugins_page` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_page_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_page_required` enum('true','false') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_details` mediumtext COLLATE utf8mb4_unicode_ci,
  `plugins_software_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_price` decimal(10,2) DEFAULT NULL,
  `plugins_published_date` datetime DEFAULT NULL,
  `plugins_update_date` datetime DEFAULT NULL,
  `plugins_unpublished_date` datetime DEFAULT NULL,
  `plugins_version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_update_type` enum('auto','manual') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugins_status` enum('active','inactive','disable') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `as_pos_requirements`
--

CREATE TABLE `as_pos_requirements` (
  `pos_requirement_id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_website` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_address` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_city` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_country` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_postcode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_no` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_unit` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_type` enum('global','single') COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat_percentage` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_variation_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_variation_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_pos_requirements`
--

INSERT INTO `as_pos_requirements` (`pos_requirement_id`, `company_name`, `company_website`, `company_email`, `company_phone`, `company_address`, `company_city`, `company_country`, `company_postcode`, `vat_no`, `vat_unit`, `currency`, `vat_type`, `vat_percentage`, `user_id`, `software_variation_id`, `software_variation_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Test name', 'Test company', 'Test@gmail.com', '225', 'Test address', 'Test city', 'Test country', '32', '2263', 'kha', 'bd', 'global', '5', '1', '1', '1', 'active', '2019-03-22 18:00:00', '2019-03-22 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `as_promocode_old`
--

CREATE TABLE `as_promocode_old` (
  `promocode_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `publish_for` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `use_limit` int(11) NOT NULL,
  `used` int(11) DEFAULT NULL,
  `expiry` date NOT NULL,
  `document` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','deactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_promocode_old`
--

INSERT INTO `as_promocode_old` (`promocode_id`, `title`, `code`, `amount`, `publish_for`, `use_limit`, `used`, `expiry`, `document`, `status`, `created_at`, `updated_at`) VALUES
(1, 'gh', '123456', 100, 'Subscription Payment', 100, NULL, '2019-03-14', NULL, 'active', '2019-03-14 05:38:28', '2019-05-09 10:14:37'),
(3, 'dasfds', '123456', 10, 'Subscription Payment', 5, NULL, '2019-03-14', NULL, 'deactive', '2019-03-14 07:30:13', '2019-05-09 10:14:19'),
(4, 'Test', '963258', 900, 'Subscription Payment', 9, NULL, '2019-03-29', NULL, 'active', '2019-03-14 07:44:55', '2019-04-28 04:48:27'),
(5, 'Test', '963258', 900, 'Subscription Payment', 9, NULL, '2019-03-29', NULL, 'deactive', '2019-03-14 07:49:49', '2019-04-30 10:54:51'),
(6, 'Test', '963258', 900, 'Subscription Payment', 9, NULL, '2019-03-29', NULL, 'deactive', '2019-03-14 07:51:19', '2019-04-30 10:44:59'),
(7, 'Test', '963258', 100, 'Subscription Payment', 10, NULL, '2019-03-14', NULL, 'active', '2019-03-14 07:57:07', '2019-04-28 04:48:20'),
(8, 'TEST AJAX', '345435', 4, 'Subscription Payment', 5, NULL, '2019-04-15', '1556443766.png', 'active', '2019-04-28 09:29:26', '2019-04-28 09:29:26'),
(9, 'sg', '345435', 4, 'Subscription Payment', 5, NULL, '2019-04-18', '1556443935.png', 'active', '2019-04-28 09:32:15', '2019-04-28 09:32:15'),
(10, 'asdf', 'Abcd123456', 10, 'Subscription Payment', 2, NULL, '2019-05-25', '1557395638.txt', 'deactive', '2019-05-09 09:53:58', '2019-05-09 09:54:10'),
(11, 'asdfdsfsfdsfds', 'RDGE963258', 500, 'Subscription Payment', 2, NULL, '2019-05-14', '0', 'active', '2019-05-09 10:16:34', '2019-05-09 10:16:34');

-- --------------------------------------------------------

--
-- Table structure for table `as_software`
--

CREATE TABLE `as_software` (
  `software_id` int(10) UNSIGNED NOT NULL,
  `software_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_banner` varchar(130) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_short_des` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_long_des` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_price` double(10,2) NOT NULL,
  `software_tagline` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_software`
--

INSERT INTO `as_software` (`software_id`, `software_title`, `software_banner`, `software_short_des`, `software_long_des`, `software_price`, `software_tagline`, `software_status`, `created_at`, `updated_at`) VALUES
(1, 'Test software', '', '', '', 500.00, '', 'Active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `as_software_variation`
--

CREATE TABLE `as_software_variation` (
  `software_variation_id` int(10) UNSIGNED NOT NULL,
  `software_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_variation_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_variation_icon` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_variation_price` decimal(10,2) NOT NULL,
  `software_variation_sort` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `software_variation_status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_software_variation`
--

INSERT INTO `as_software_variation` (`software_variation_id`, `software_id`, `software_variation_name`, `software_variation_icon`, `software_variation_price`, `software_variation_sort`, `software_variation_status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Test variation', '', '5000.00', '', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `as_subscribe`
--

CREATE TABLE `as_subscribe` (
  `subscribe_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `software_id` int(11) DEFAULT NULL,
  `software_variation_id` int(11) DEFAULT NULL,
  `plugins_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribe_type` enum('software','service','plugins') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribe_date` datetime DEFAULT NULL,
  `subscribe_activation_date` datetime DEFAULT NULL,
  `subscribe_amount` decimal(20,2) DEFAULT NULL,
  `subscribe_payment_terms` enum('free','monthly','yearly','onetime') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribe_payment_terms_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribe_reminder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribe_status` enum('active','inactive','cancel','expire','return') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_subscribe`
--

INSERT INTO `as_subscribe` (`subscribe_id`, `user_id`, `agent_id`, `software_id`, `software_variation_id`, `plugins_id`, `service_id`, `subscribe_type`, `subscribe_date`, `subscribe_activation_date`, `subscribe_amount`, `subscribe_payment_terms`, `subscribe_payment_terms_value`, `subscribe_reminder`, `subscribe_status`, `created_at`, `updated_at`) VALUES
(1, 24, 1, 1, 1, '', NULL, 'software', '2019-05-22 06:23:27', '2019-05-01 00:00:00', '500.00', 'monthly', '1', NULL, 'return', '2019-05-20 18:00:00', '2019-05-15 07:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `as_sub_domain`
--

CREATE TABLE `as_sub_domain` (
  `domain_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `sub_domain` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `root_domain` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `basedir` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `domain_status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `db_host` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `db_username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `db_password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `db_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `db_status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_sub_domain`
--

INSERT INTO `as_sub_domain` (`domain_id`, `user_id`, `sub_domain`, `root_domain`, `basedir`, `domain_status`, `db_host`, `db_username`, `db_password`, `db_name`, `db_status`, `created_at`, `updated_at`) VALUES
(1, 13, 'dfg', 'dfg', 'cdfgh', 'active', 'dfg', 'dfgd', 'dfgdf', 'dfg', 'dfg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `as_terms_and_condition`
--

CREATE TABLE `as_terms_and_condition` (
  `t_c_id` bigint(20) UNSIGNED NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('t&c','privacy') COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body_text` longtext CHARACTER SET utf8 NOT NULL,
  `status` enum('active','deactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_terms_and_condition`
--

INSERT INTO `as_terms_and_condition` (`t_c_id`, `added_by`, `type`, `document`, `body_text`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 't&c', '1557062037.jpg', '<h1 align=\"center\">\r\n				                 <font color=\"#085294\">Mehedi Hasan Shuvo</font></h1><h1 align=\"center\"><img style=\"width: 632px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAngAAAF6CAYAAACKtyWsAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nOzd95Nc93nv+fdJfbpP58mpJ2EGORAEERgkKlD2dVqvy+vr3ardH/Zv21/2rtcl77V177Vk2aIkBgAEQWAQBhMwOU9P53Dy/nAaQ5AEJFuUCGLwvKqmiCqwgZ6DM3M+8/1+n+dRwjAMEUIIIYQQR4b6ot+AEEIIIYT4/ZKAJ4QQQghxxEjAE0IIIYQ4YiTgCSGEEEIcMRLwhBBCCCGOGAl4QgghhBBHjAQ8IYQQQogjRgKeEEIIIcQRIwFPCCGEEOKIkYAnhBBCCHHESMATQgghhDhiJOAJIYQQQhwxEvCEEEIIIY4YCXhCCCGEEEeMBDwhhBBCiCNGAp4QQgghxBEjAU8IIYQQ4oiRgCeEEEIIccRIwBNCCCGEOGIk4AkhhBBCHDES8IQQQgghjhgJeEIIIYQQR4wEPCGEEEKII0YCnhBCCCHEESMBTwghhBDiiJGAJ4QQQghxxEjAE0IIIYQ4YiTgCSGEEEIcMRLwhBBCCCGOGAl4QgghhBBHjAQ8IYQQQogjRgKeEEIIIcQRIwFPCCGEEOKIkYAnhBBCCHHESMATQgghhDhiJOAJIYQQQhwxEvCEEEIIIY4YCXhCCCGEEEeMBDwhhBBCiCNGAp4QQgghxBEjAU8IIYQQ4oiRgCeEEEIIccRIwBNCCCGEOGIk4AkhhBBCHDES8IQQQgghjhgJeEIIIYQQR4wEPCGEEEKII0YCnhBCCCHEESMBTwghhBDiiJGAJ4QQQghxxEjAE0IIIYQ4YiTgCSGEEEIcMRLwhBBCCCGOGAl4QgghhBBHjAQ8IYQQQogjRgKeEEIIIcQRIwFPCCGEEOKIkYAnhBBCCHHESMATQgghhDhiJOAJIYQQQhwxEvCEEEIIIY4YCXhCCCGEEEeMBDwhhBBCiCNGAp4QQgghxBEjAU8IIYQQ4oiRgCeEEEIIccRIwBNCCCGEOGIk4AkhhBBCHDES8IQQQgghjhgJeEIIIYQQR4wEPCGEEEKII0YCnhBCCCHEESMBTwghhBDiiJGAJ4QQQghxxEjAE0IIIYQ4YvQX/QaEEEdHGIbYjkOlVsP3fUzDIBYzMAwDQzfQNO1Fv8UXyvd9XM/FdV0cx8V2XTRNI5tOY8ZiKIryot+iEOKIkIAnhPi9CMOQMAxY3Vjnp++/T6VaoTA0RGFwiMH+fgb6+sllsy/6bb5QtXqd7d0dtnZ2WNvaZG1zk2wmyx+9+y5T4+OAKiFPCPF7IQFPCPG1BEFAEARUqlXWNje4decO7//ql7TbbaonThJ4PjHDIJfNvei3+sK1bZviwQEra2s8nJvjwaNZ4vE4iViMZrNJYWiYbCaDqqqoqpygEUL87iTgCSF+Z77vU65WKZZK3Lpzh3/86T9z+7Pb7G9u0dfTQ9qy6Oruoq+vD8d1X/TbfeEc16VSr7NT3GdtbZXHj+bY3d/n3sOHXHztIn/xR3/MpQsX6M7nyWUyr/yWthDidycBT4hXXBAEuJ6H7/uEYQiArmnouv7cgOH5Pq7jUKpU+ODGdX72/r/xwY0bbO/uUavV8D2XZJin4djUmw3adhs/8L/JT+tbyQ982o5NvdWk4dg0Qp+DVoPKygq7BwfcuXeXt69c4Ufvfp+3r1wln81ixGLoz/l3iM70eXi+h4KCoihomoah67ICKMQrTgKeEK+4tm2zVyxSrVWxXZcwCMjncvR195BJp5/5mmqtysbWFksry9y8eYNPr19neX4eM53GSlo0m01CRSEIQ4IgJAhD6ITHV5kCKCioKCiKiqKoaIaBZVmEvsfy3ByJMCRnJTFjBhNj4wwPDtKVyz/zz6s3Guzs73NQLqOpCqZpkk1n6O3uxkokvtlPTgjxrSIBT4hX1JOVu53dXT67c4ft3R1CVSUWN5kYHSMRTzw34DWaTda3tphdWGBlY51ypYLv+2iahqZpqICuKCTiCdKpNFbCQtO/+O0mDMNoJdB1cT0vWn1SVXRNwzCMl2YFKggCXNfF8338ICAMQwxdxzAMdE37QtGEpukk4nFSySSJuImmKqiAYRjg+9GWd6XCysY6PQsLGDGTXDb7/IDXbLCysc7jlWVc20YNYbCvn9cuXGBocFBW8oR4hUnAE+IVVavXWdlY5+Pr1/nx3/89Kxvr5Hp6GBgZ5p2r1+jt6mK4vx+eUdVZqdVYWF7m1r17LG5sUgsCSCTwFAXF91DCgGwyyfjwMGdPnGBseIR0MonjutQbDZqtJq1Wm0qtRrlaod5ooGkaphEjaSVIJ5MkLYtEwsKyLJKJRBSCvgVc16XRatFsNmm1mjSaTWqNBo1mC9t18H2fVDJJLpMlm06TSMSxEhapZJKklWB4YADbbrO7vc2slUAJw2h7XFEgkaAWBCxubELMJGElKQwPU3jWGwlDigcH3H1wn19f/5jt9Q0q+/uMDY/wV3/911y7epWx4RGymcw3fYmEEN8CEvCEeIWEYYjrutiOw+2ZGf6v/+e/8MH1j9nZ2cZ2HGLVMrvVCsMDg5TPV/E6q3JPr0KFYUi9Hq0cPZifY29/n5bngqbRbLWw4nEK4xNcuPg6ly+9waULFzB0g3qjyczDB1y/dYv7s7Msra+xVyrhOg5eZwVPVxR0VcVQVEZHRrh65SqXLl7k4tlzDPT1vcAr97liqcTtezPcun2b6zeus7q+jhsGeEGAF4aEYYiu6xixGL35PBMjBc6cPMnVS5eYGB1joL+f7q4uHMdlv1qh4XrsFYs0Wy0UTaPluaxvb2F7LoXhYer1BmEYfuXfwPd9SuUyS0tL3L13j2KxiN1us1UusbS/y9u3b/N//Oe/5dKFC5ixGIZhSAsWIV4hEvCEeIV4nsfW7i4r62v8+qMP+fTmDVaXHmOmUpiJBI5tUy0WqZZKNOo1HMfBNM3DYoswDAn8aEuy3W7TbLVo2W1cx8VzHULfZ2RslL/5y7/ie2+/zdhIgUQ8wZ0H9/nlxx/y0UcfszI/z17xgKYS4oQBYRAQ+D44LjguVjxO99AgsaTFxNgYU2PjpCzrBV+5z6Usi6mxcQ4ODrh9f4ZKo05xc4tmuw0xA2IGqqahqCqryyoPP/2Mmx98wL/98pe8+eY1vnvtLS6cPsOV11+nv6+XicIof/cPP+bB3ByKHoXplqLQbLVot9u4rkvgB6ja5z3yPN+n1WpRrVYpH5SoHBzgOg6mZUEYsvr4MQlFZaJQQNVUxkYKDPX3f2tWQYUQf3gS8IR4hfi+T7F0wNziIrML85QqZYIgiM7HKQphq4Xr2NGqmvvFytqnRUHPx3ddfNfD91wSpklvVzfnT53hjQsXmBqfRNM1ypUKS6urfHb/Prfv36O2u4fjuqhJCz0WQ9VBU1WsjE5Kj9Hf18vY9DQXL77G6RMnGOzvJx6Pv4Cr9WzxeJzB/n5OnzjB1v4ucSPGyvw8O7t71D2Hpu/hBwFBCJ7jUG40abaaHDg2iUyawtAIY8MjWIkE0xOTNBoNHs3P0Wi12DsoYrsuvqriuy7B865/EOJ5fhSsHQffcQiD4LDaNggCSpUyswvzDAwOkkom6e/pkYAnxCtEAp4QLynP87AdB9fzouIEXcfotDZ53lac5/vs7u9z/9EjHiwuUPN9sCxCTSP0feA3V7oqioKqqaiaCoqCoiok4ibppMXU+Dg/+M67vPHaRaYmjxESUipXKJZKtNttcpksU8emcIZHDs+c6TEDMxYjlbQoDA4xOTrGiWNTTIyN0d/bRyadJm6a36qtRU3TsCyLE1PTDPYP8KPvvMvSygqPFhd4vLrC2tYm9UYT23HwHBclDNE0jVg8Ti6Tpd1usbm9RU93N5l0mhNT0/zv//lvOT49zc/f/wXzy0t4foCiKvDU9X76GigKqJ2iFEUFlBCIqpUVTQPLoub7PFhcoLevn5GhIY5PHnvu5/Rky9f1PDwvCqiGrmPGYui6PCaEeBnJV64QL5mg8zBeXV/n4aNZVtbWSKfTDPT3Mzo8wvDgIOlU6pmvDcOQtm1TqlQoVat4QRQkPM9FVRVilkUqlSKZyxBPWeiGThAGOK4bBQDXxXYd6o0GhmEw0NtP9tg0E4UCp4+f4OT0NH09PXieR61eJwxDunJZ3n3zTb577U0URTkMKr7vRyFFj6pmzViMuGkSj8cxjdi3tsnvk8/BSiSwEgl6u7uZPnaM7771Fm3bjkK36+J7PkEn3MGTUW7hYc3Kk+kfQRAwPDDIW5ev0pXr4sHcI5bW1qhUqxiGQb3RYL90gGlE5+g0TYMwJBYzsJIWyUyGVDZH2GgQEuJ7bvRvGgSUqlVKlQpt237mSuAT9UaDja0tVjfW2d7ZoVarMVYocOrESUZHRqLK6G9RyBZC/HYS8IR4yTidBsMzMzP89Kc/5eYnn5Dt6mJi+hhXr1zlh+9857kBD6J2dGFnpU5RFULfp91uk0ln6BsYZLQwyrHxY3Tnu1BVlWarRb3eoFSpsLO3y8rGBvcfzTI7P0exdECj2aTVbrO2tcW//Op9FEUhDEPi8QSZdIruXJ7pyUlOTR9nanycbDqDaZpfCBzfphW6/yhN07C0BFb8875zX/7cbNumUquysLzMw/k55h8/plguUa3Vabdbh9fMD0Jq9Tqlcpm23WZ2fg5NVdnY3mZseJj+3j7y2SypVJKEGae7p4epY1Ns7u6yurbGzs4O1VodxdBRzOiahoS/tQXh/sEBv755g+s3rrM8v0j54IDLb7yB57hYlkU+myVumn+Q6yeE+MOQgCfES6ZWrzO3uMCdB/d5OPeI+cUFknsZyu0GmXyOS+fPEwTBF1bLnqYoRE12UaIChyAAIJNOMzk2zplTpxgaHCAIQnb3o+rOtt3Gtp2ozxthZ6s22gquVipUSyV838cJfLww6gUXj5mkkkm6slk2NjbY2d6muLfPxMQEA319pFMpzFjsm75834gn1912HGr1Otu7uywtLTEz+4CH8/PMLy9xUInaw7Qdu1NBrBJToz6CqCpmIn54jUNC/CCg1W4ThAGNVgsrkSAIQoYHh7hw9hy6ptNqNqnUaoRBeFh5GzVUfvb7DMPw8Lzew7lH3J65y9bKGo1qlVQmTf/IMPmebk4fPyEBT4iXjAQ8IV4ST7ZmV9bX+If//hN+9eFH7Oxs4+gadqtBdXGRfFcXK1euMTo0TDqVIvalAKUoCjHDIGUlsOImB56PjkK+t49Tp07z9tVrvH7uPPG4Sa1Rp1yrEoYhSctisLeP8dECr509y8mpaUY+G+J21wwrj5fYXF2ldlDCNzQCTQVVo+41adTr7G5vs7S4yK9++T7jhQKXL12OWp+cv8DI8DCGYWDo+ku9ive0MAxxPQ/XdVnf2OD23Tvcun2bm7dusry2Rst1sT2PUFEInzQh9n18P8Bzfbq6uxgaHmFscoKL585x+bWLTE9MoCgKjUaTrb1ddovFw8bQYyMj9Pf0kkmlcX2fpuNQKZVRXB8rZpJKJIg9p0WK47rUajVWV1e5e2+GR4sLeI6HomssbK5T+9lPqTTqpKwk+Wz2N57vFEJ8u0jAE+Il0Wq12C0Wuf/wITc//ojZh/cw4gmsTBrHsWk3W6yvrfLxJzcxTfOZveM0TaM7n2d68hhbm5s09vbRkimuvvU2F85foDA0RCwW6zTltToFDipx04xW3DoFD7lMhlPT0/zFj/6I+7Oz3L5zh09v32ZxYZ690gF2EBBES4WgKrR8n5brUn74kPv3H/DL93/Bu+9+n2tXr3H82DHGRkdJxE1UVUNVX84AEQQhQeDTatusrK4yt7jIx9c/5v33/43HKyu4qkKgqqCqKJpKSBQGtTDEBHp6ejg2Nc3rFy9y8cIFzpw8ST6XI2VZxGLRlnY6mSJpWZ0zddGKXr3RwPU8CsNDfPfNN8klk1z/6CPCIODksWOcmJqiu6vrmWcaS+Uyt2dm+OjWLdbW1rDbbeKdv6/ebFB6eI+EpvLG+Qv09/XRnc+T+BZVNAshnk8CnhAvCdtx2Cvus7O/R63RwHU99Diomoaq6Si6Sr3dZnF1lcLIKtPPqJpUVZVcJst4ocDO9HH2i0V0XefMqdOMFwromka5WqFcrRCGAZqqYZomPV3dTBQKUeADzJiJGTPJZbL05Ls4f/oMb125ys1Pb3Hr7l0+fXCfrd1tQj+AwIcwOgcWeh6BbdOs19ne3GJ5cZH33nsPRVXo6+0lnUx+q1qi/Ec4jk2t0WB3b48PP/qQf/mXf+H6jevslw5ouQ6qaaLoerRdqiigaiiaylD/AG+cPcel8xe49NpFpiYnyWezpNNptC+NGWu1WqxtbrJ/UMS2bfzAP/y9RDzOsbFxQj9gZ2+PIAg4c/o0U5MTdOfyaNpXR5bVm00er67yeG2Vht1G0aN7SdU0PBdc16Nar7GxvcXW7g5WIiEBT4iXhAQ8IV4SQRBgOw6OYxOEIagKIQqhAqECoOL5AY1GND7L97zD17qeh23b1Op1iuUSpXKZeqtJoKp4wEG5zMraGq4bVYDanWrQMAwxTZPe7h5Wx8YYGhigO58nl8lgJSzi8WhWai6bJZVMEjNNUtkMyUya5dUVWs0m7VYLp1NZGngeCqCjYPB5Ne2TWa7Bb6sG+BYLnpqt6/tR8LIsi0Ergdcpa1E7M2pjsRjxRIKEZTExNs6Vixe5cPosp6an6enq6vx5Ac1Wi2arSblapVgqsbm9zdLKCnvFfWw7OrtnxmKYphn9uUaMg0oZD0BVqTebHJTK7GcPSCaTpJPJ6P/ttD7xPY9Gs0mj0cTzA0A9vJ9CotXXIAyxbZt2u03wVKAUQny7ScAT4gUKggCvEwZ0TfuNg+EVRcHQNHRNR4man0W/EXY+CNBUiMdjxM0YqqZGZ6waDcqVCuVK1JNuaXWFhaUlFpeX2SvuU6832N7eRlGUKOB5XvQg7xRfKKpKLBYjYSUZGRriwqlTnDt5mvGxMUaGh8mm0oeVlpfOn+fk1BR/+sP32C8W2S0W2S8WqVSrVOt1HNvGMHQSZpx0MsnQwACFkQJ9fX0kLesweLyMDF0naVn09fVx+coVBoYGeXd7m1qjEU37cD1ipkkmlSKbydDT3U1fdze93d2kO9cwYZrYjkOz2aRSr7G+scHyygozsw+48/Ah65ubtJoNnE5jYwBUFVXVMHSdmBEjCEMarRapVIrZ+QVURaXeaFKt1aNw3gnk6WQSVVOJmzHi8RjRAl/w1P0EhAoKn/dYjO675/uP3M9CiD+sl/e7qRAvMb9TvVo8OGBpeZkwDBkfG6M7n8cwjGc+GM1YjN5OIIjHDAiDww81DNAD6M3keOP8ec6eOkUYhszOz3NvdpZmq0U+lyOfzXLxzFneuXyFlY11/unnP+P2Z5+xt7FBpVrFVcAlCpNapwo3VBTCZougVGZ1bY1bH39MT76LoZERRkfHuHDuHG9ceI1Tx4+TTqVIp1L0h9EILtfzcL1oIsaTqRiKoqCqClonrOr6k9D67Krfl4Wu66Q0jWQiQXc+x6njx/H86HMPnqpq1bSoUtb4UmNq23EoVys8nJvj1p073JmZYXV1hc31dfZLB9hKdIZPVZSoErrTV8/v/NcAjBCymQyjw8O8duECf/qD9xgbHqHZbtNsNllZW+P2vRmsRIIzJ04QN+OcPXWKer3K0vw8pb0iaueeUsIAhZCEGaOvu4fenh5M89lVz0EQja8rlkosr6ygKAoT4+N0d0Wtdr681SyE+MOTgCfEN8zrDInfLe5z/ePr/P3f/R2O6/LHf/ZnXLt2lZPHpg636Z6W6IzIGi0U6B0cJrG+get5OM0WVjxO71CescljDA4M4Xs+H9+6ha5pjBUKDPT2kkwmSSYsYoaBruscGx/n4tlzrKyvMXP/PvcePWJlc52N7S0a1RrtRoO2beM+CRGKAkEQDbmvVrHnFyhublPa3WVjY53N3cu8+cZlCkNDUdsPXUfXdRLPuAZH0dMBVdM0npOFnmt3b48Pbt7gxs1PWJx7xPLSErVGg6br4KtqFAxVNZqMoSgYihI1hk4mSWbSDA8MMjY0wtkTJzh35gyjIwXy2SyGYeB1qnqbrRaNRoOt3R3u3L+P5/sMDw4yODjE2LFjlFotavU69WYLVVVJZHP0DkdBfqh/kNhzRp0dlKOxaB9fv84//+QnxIwYf/03f8PVa1fp6+4hn8sdjlETQnwzJOAJ8Q1zXZeV9XU+uXOHX7z/C2bu36NWr7PXrLO2t8P/+bf/6zMDnqZpJOIJuvLdTIxPsLq+zu7ONrVyGTORoCuXI5lOUaqU6e3u5syJE/R2dZPNZrESia+spGiaxmA8Tlcux+TYGO++8w6NRpNKtcrWzjbrm+ssr62xsbNDtdEgDKNVKss06c3n6O/qYaCnl8HBQQaHhhgZHqY7n/8mL+WR0pXPc+3SGxQGBtnaeoOtrS229/fYOdhnr1Smadt4noeiQCaZZLi/n/FCgZGhEQb7B8hmMiSTFrlM5rCZ9BO6phE3TZKWRXc+T3dXF8ODQ+wdFNne26FUKZNMpejK5XAaDVqtFqlcjr6BQcbHxunK54mbseeusK5tbvB//9d/4P33f8Hao3kyqTTpvl4c4I0LF0glkxLwhPiGScAT4hvm+z7FUom5pUUWVpZp+R4tz2VxaYmevj6WllcYHhgkmUgQj8e/sF2rKArZdJoTx6I2J77dplmrEjdNunLR2SoUOuOyPCq1GnsHB4RhiK5HW4MxI0YsZpBMWGRSaUwzRl9PL309vRBGBRm7+3usb28xtr7G5vY21c7YMV3XsRJxeru66O/po7+7h658nnwuRzKZfIFX9eWXtCySlkVvVxdjhQIHpRI7xX129nfZOzig2Wp3Ap5CJpViaGCAsZECIwOD9PX0RucXnxzLDENa7Ta1eo1Gp8jlybg5z/MP+yH6no9juwDkslm6clnK+/tomkp3Lsf0xCTHJydJp1JfCXdBENBut2m0WiytrHD/wQMWHy/huy6G77GwsszQ0iITo6OHRSdCiG+OBDwhviHRKKqAtm2zX9xncWmJ5c0NvFgMPZ3GsVusrSzz81+9jxf4nD5+gqmJia9Me8jncly68Br1eo1KtcxusYiVSpHPd5FNZfC9gPnFRX750Yfslw6o1GoEQUgiHsdKJMhlMuQzWcZGRjhx7BiD/QOk0yksq7N9q+n09vSQy2aZnpg8rAoN6ZzNU5XOoXvj8AyZDKT//UkkEhiGQXdXF+NjY7iei+d5+E/O8RGtvj6Z3xvNp1VxXAfbdWk0G9RqNTa3t5lbXGR1YyNqfVOr0Wy1aLXbqGr0g0JPvovBvn40XSebypDPd2GldjDrdYaGh7l88TXeuPAaXbncV96n63ksra3xYO4R73/wazbX1vB9DyOTwY3FWN5cZ3jpMfvnzmM7DolEonN+8OU9ZynEy0S+KwvxDXFcl2azyX6xyN7+Pvv7+9TqNeKJBIYVB01lt1rlJ//6c6rNJpqmMTI4eDiFwHFd6o0Gm9vbrK6vs1+qgKZjZbPU2zazj+aYm5un3qjRarexPS8aLdZ5oCphtMCjKaArCmbMJJlOMzpS4OK5c7x29izHxscZHSmQtKzo9y3rxV60V5DaqVoGfuv5Rc/zouKMcpnltTUWHi9y+94Mt+/fZ31jnVathm3beIBPp0C2k6+UMERTVUxdJxGPk0qlCcKQut3GymZA09gvlVldXyedTKIoStQKxzAIw5B2u82jxQX+8Wc/5Vcff0S5WsWwkhhmjCAIqNVrFA+KFA+KHJRL6Hp0xOB55/iEEL9fEvCE+IZoqkrMMEiYJjHDQFWVw6H0qqqixww8P2Dv4IDtvV0qtSq2Y+P5CTRVY2dvjw9u3mR1Y/1wy3Wgv58wDNjb3mZ/Y4NSuYyrALqOYcbRDQNV7cxGVaMHvBeGuL5PvVple2+PtaVlHnx6m9snT/L6G29w4bULHBufYGykQG939wu9ZuI3K1UqrKyvsbi8xJ3bn/HpzU+4P/uQUrOBEwaYpolmGKCqoH4e9KPVZLBtm4pdAc/DUCCfy9M7OEDfwCCD/QPousbK+hqrmxuMDo/w9uXLDA8M4Ac+tmNTqVXZ3ou2kBVFQY9FFeBBEBDd2tE4NV3X0VQZcybEN0kCnhDfEFVVMQ0DKx4naSVIWtHUBkVRCJ70NIPPW4oE0ZZurRado3q8ssLqxjq1ep3hwUG683n6e3spFQpsrq2xnltmc3uLim3T9Dz8MCSI5mEREqKESueMlgKqimYY6KpKQtNJJ5NY8cSRmgn7KlEUBcMwsCyLTDqNpyq0fA80rRPsPu+ZGLVcVqIeeLpG0kpg6TrZRJzhwSEKY+MMjRTIZ3Ok02lcz2Nje4vVjXUG+3qBkGQiEa0OB+Hh/fpkmz4IAhRFIR6Pk7QsEokE8VgMw9Bf2jF0QryMJOAJ8Q1RVRXFMKKK165uxkZHWd3cYGdvj2arhdY5zxY345hGNHu03miwcnDA7MICu8V9klaC6clxTk2foDuXxw98HMelUilTPDhgeXWVh4sLrKyvs7O3R6VWw3adzhmuqA+dqiioikoqmaSnu5tjY2NcOnOOE1NT9PX00t3dTTwe/8rZP/Htk+9USE8URnnt1Bl2f/ADHi0scOv+DIvLK+wVizQaDfwwiEK+oqCp0ZlJ04iRSafp7+1lvFDgzPRxJgqj5DvNkJ/0YyyWSzycf8Tm9jbXb3/K0uoqJzvzbcMwxIzFiMdNgiDE83x8z8NKJBga7Gd8dIze7h6SVpKY8fwqXCHE758SPtkjEkL8h4VhtILhuG60nep50dSH33DWqG3bzD9e5O7Dh7z/wa/5+S/+jZX1dTTTJJPJMNjXz8npaU4fP87E6Cie51GrN8hls7x+7hwjg0OYsRi6rhONNY22ekOic36tVou2Y0d9z5pNKrUq9UYT13Xw/YC4GSdlWaTTqag3nmVFq4mmidbptyZePr7vHxbxNJoN6o2o2KJWq9Nst7BtG03XMIwYqdpLgeMAACAASURBVKRFNp3BsiwMwyAeM7ESn9+zT99TT875rW9t8unMDOVKhXQyia7rPF5Z4d7cLLMLC+zs7VGrVvFtm/FCgfe+/wO+9847nD1xiumJiS+0bXma0+nP5zgOhq5jdo4wPGkALYT43cgKnhBfg+O61Oo11jY3WVpbZb94wOjIMGeOn6QwNPTM18QMg9GRArlMltAPmHn4kJWtLYIgjGbGug7lapWNrS10TWe8MMqpiycYHhjESiTwPZ+9WpEgCNA71ZRPKmTTnXmjTzyZMOD5nYKLMDysetU1PWqcKw/RI+HJhIyYYZBJpQ5/+PA87/DfP1rBU9E1/ZkTUxzHOay0fTIfWFVVkgmLwb4Bvv9Wlo3tLR6vrLC0usL69hblajVqy+N50SxhTaOvf4DvvfU27157k3QyhfEbCit29naZmZ1lZWONvu4epsYmKAwPHxZ0CCF+NxLwhPgabMdmt1hkfnGRu/dnWF1fZ3ryGHQept35PIl4/AuvUVWVlGVhxeNMjI1ybHKSjZ0dDvaL4HioYTSWLJ1KkctmScTjBIHP7v4e5UqFaq1KvdHAD/yoVYlhkLQsUqkU6WQKK5HorNClScTj0YzSmGyPvWqenibytDAMCYIgWu1tt6nVatSbTZqtFrVGnXq9TqPZxHVdXM9FUzVSySSZdIZcNksQBliJBF35PKVKmYRpohKC42JqGl19fRybnGC8UGCgpxee80NEq9WiWCox8+ABN299wvzjRcZHx8D3ScTNzmxdCXhC/K4k4AnxNVRrdR48muPD69e5c/s2jxcXedh/n4ezs1y9fIXvvPkmxyYmsBIJDP3zh5WqRoPYC0Mj/MWP/ohswuLnP/sZlWqV3lyOwtAgYyMFBvr62N7b5YObN3i8vMzO/h7VWhXXcaLzdKqK2glwpmnSncsx3NvHeKHA9LEpJscnmBgdJZ/LoYKEvFdcGIYEYUipUmFpdZXHy0vMLy5EE0v2dimWy9i2jeM4BJ53WDBhxGJkMhkGenuZGp9kenKCkcFBIMBxbKoHB9S2d8nmuvjhD97ju++8w/DgEEpUwv2F9+B60ZbswuPHvP/BB1y/eYOF+Xn2dnfZnJqK+vTpBslkknQq9WIulBBHgAQ8Ib6GSrXKvdkHfPbwASu7O+zUa+xWK8wvLvDg3j3uPbjPm1ev8d0332K8MBoFMlXpPPMU+nq6+e61N9FUjcXVVR6vLGMmEqysrHJnZoZqrUatXqfRaNB2HFAVlM6KiELnnNThR4AahJhBSDaVpn94iDevXuV/++v/hVw6TaiqKHK+7pUWBgFhELCyssJ/+fv/l4+uX2dnc5NKvYatKgSqgqKoh3N1FUUhpHN/baxz717Ir82PDsNXLpMhn81gWUkyvb2Mj43z/e++y9tXrpDPZr8Q7oLOyuHK+jq//OhDPvzoI25ev87y2iq2HxBqGtrODsH9+6RzOY4fO8bwwOALvFpCvNwk4AnxNdiOw17xgP1SiabrEBo6hCGeqrK2v8f+z35KsVhkoLePrs52bdw0D1fS6s0mC8vLzC0/plgrs1vco7K/S+B5tGwH2/MIFAU0DVXX0XXtcCUuDAJ8PyDwfULPJXRdTCNGLJ/n2MmT/PEPfsiVS5cYHSmgyaQJASidecSFQoG//LM/p7+/n3/+159z/+EDGpUyjuuiGAaKbqBqGpqmoqhqtPLXGXPWbLc4OCiihiGmrmPFTFRDx1Yg25VnfnmJgb4+Tk5PE+8cTwjDENu2abfbzC8s8E//7SfcuHWLluPgGwah5oOi0HYdSpUypVIZ13Ff8NUS4uUm3/WF+BqihrH+YQsSFAVV0zBiMXzfZ3d/n+WVFRaXlhgZGaG/p+ew+tX1fXaLRWbn59nZ3WV0aBjLMKiXSjTqdWzHw/Y8PCBQFAIlWkmhU92IpmHoUdsTU9exdJ3e7m4KYxO8/tpFvved73BiagrzqUApXm1P7oOuXI7Xzp0jnohj+x6ZXJaNlRX2D4o0vei+Czr3M3S66HUmqqihghqG6ICp65i6TjKVItWVp6evj63dbR7Mz9HVlSeZTGJoGiFQrdXY2dtl/vEiS8vL7O7vY2Wz6Ik4vm0TBAEB0Srf4deTEOJ3JgFPiK/BMHSy6TSZVIparQaBD0r0IFU1DVXTKJbLfPDJDfR4jMsXXsNKJKg3G2zt7DC7sMDC8hLZTIY//9GPyGeybGxtsr65xX6xSLFUotFq0rTbNFttWnbUigVA1z+voO3v7mGwr4+p8QmmJ48xNDBANp0+bKQsxNM0TcOyLE4fP8Hw4CB/8v0fMP94kYXlZbZ2d9kp7n9eSev5AOi6TsI0sRJxLDNOMmHRnc/T093NyNAgw4NDFCtl3v/oIx4tLmJZFu22zWB/PynLYnl9jRuffsr7H3/MfrncWSHUCBUFhRDCAEPTsBJR2x5Nl+MEQnwdEvCE+Bry2SwXz52nWq0QOA6lvd1o5SMMozNvpknNdbg/P0euK09/Ty/5bI4Hc4/4xYcf0Gg2effNtzl/+jSFoSGSlsWZk6dwOm0nPM/FD4JodSMICDoD5yEKkYqqoKpqp+IwRtw0MU0TXc7aid9AURQ0Ren0QLTo6+nl5PRx2raN43ZannQmVTx9v6md+03tbPXqunFYpR0zDOrNJsP9g9x9+ID3P/qQT+/e5ftvv83p48fZ2Nri3twjZpceU3ddlLhJqKoQBISejx6GDHb38PrZM1w8dy46wyeE+J1JwBPia+jt6ubdq9cIfZ/9gwMWV1ejVTY/QAmirVRV1zDjJq7rsb69iW7obO3uEBJGZ5Wmppgan+iMclJJaNpXWqsI8Yekaxp6J+x9HelkkunJSRzX5cH8HPVGne3dXXRNY2tnB891iRsxNE0lDMH3OluxqoZpmoyPj/Pda2/x7tVr5HO539NnJ8SrSQKeEF9D3DQZ6O1lYnSUibEJxsZW2dvfo9lsYdttCAN0VcM0TBzXZWNrG9d10TWd0ZERBvv6SadTNNst6sUGtm3j+360CtgRVd1GPc3MWAwzZhJ/qtu/TJ4QfwhPZsw6rkvbtrEdG9uJxt5FvfSeukcVBU2LQloqmSSdSnJ6eprtvT1a7RZzjxfZ3tvFdh2MWHTfEgZ4jo1pxsl0ddHb08vEWNTWZ6C3N2qxIoT4nUnAE+I3eNKC5EnLiK9QorYlQwODfO/NN+nOZbkzM8Odu3dYWltD0bSogtCxOTg4wLFtqtUqxycnOXPiJL09PWxubfHpnTs8Xllh76BI27bxfL/TCiU6L6VrGknLoiubZbBvgImxMYYGB8llMqRSKTR5GIrfIz8IqDcalKtVNre2WFpZYWt3m4NKhUazidcJfyHR14iuacRNk96ubibHxhjo6+PU9HF6urq4/2iO+ceLFMslqrU6tuMQBiFKCKHnMjgwwIXzF7hw7hznTp1maGDwmf3znvitX5NCCEACnhBf8aQlRKPZpFyr0mg2iRlG5/C3RSKRwHi67YiiMDQwQHc+z4UzZzk1fZzhkWE+vnGDpaUlqtUqK56HqqgM9PUxNjKC7/vcuvsZG5ub3Jm5y8bGJrZt4/o+gaIQdvqPERIVbngBMVUlaZocn5rmez/8AVcuX2a8MIqVSEjAE79Xge9zUCmzvLbKjZs3+cXP/5W5hXkato0TBKCroGpRQREKShiihiFGZxVvZHiE1y9eZHhoCE3TGCsUsB2HxysrrK6u4rZadKfTTExMcO3KFb5z5RqXLrxGLpOJZtZ+pTmyR6vVotFs0mw2cVyXpGWRy2RIWhaqzK0V4isk4AnxJUEQ4DgOD+fm+NcPf81n9+/R3dXF9PgE50+f4dypU/T19H7hNU9GQsXjcd6+eo2Tx09w4cw5fvxP/8jMw4e0XIe2Y7NXLKIQclAssra2xvbuLi3XxQsCVOXzJsYhEAQhvufhtVq4jQbZVIresTFOnDnDpdde58zxkyQtS7Zoxe+dpmn0dfeQTFj4rs/W9g6leo3yo1mq9TpGMomeSKDp+uERAj8M8XyfRr1OdWGelbVVBvr6GR4Zpquri939qDI3nkjQlctx/tRp/urP/4IrF1+nO58nl8k8914ulUvMPHzI3Qf3mV9aYv/ggItnz/L9t97hwunTxExTvg6E+BIJeEJ8Sdh5UO0fHDA7P8+HN26QTaXYXFujVinjODbHp6bp7+0lZSW/8FpN0+jO5+nO53Fdl7nHj6nU66yurlIvlVkPQop7u5QODtja2abeaGAmU+gxE1TlyRuAIIAwRFUgaVmYySTjIwUuX73KtatXODE1RV9Pzwu4OuJVoKrqYYXtiakp9q9eAUK0MGR5fQ07DHH5/D7lyZaqpkUNix2HSrlEtV6j1qiTz+VptlrUyyUShsHYSIGzp89w8ew5jk9OPvd91JsNdvb2mFuY5+atT7h19y6LKyuU63USpsn5k6fxfB9DeuYJ8RUS8IT4kiAIcDyXtmtj2zbNRp2D3V22l1d5dP8BH964yaWLr/Oj732f0ydOkE2no22lL0klLabGx9jd3qK8tcXW3j679RqoKm4YgK6TyGRRVC1qJBsGKIAehhiGjhEzsZIWI0PDTIyO8dq5c7xx8SLjo2Okk8mv/H1C/CH09/Xyo+99n+nJSU6dPMlnMzMsra6wvrlBs9HEdeyopU/UzQ4FBc2IkUhHW7jFgyLVUgn8gND36RkcYmp0jGPjY6SSz67atW2bSq3Gg0eP+Nkv/o1btz9lZXGRvWKRlu8RTyY/byX0paIkIUREAp4QXxJ2PqIViWhVItA1HF1ju1ph68Z1Hj2a5frNm1y7coX/6T/9CWdOnsQwDHRNw/O8TsXsFrfv3aNUq/KD994jbpo0W22a7Tau5+EGPn4QhTpFUdBUFSueIJ/NMtjXR39vL329veSyWbKZDNlMBiuRwNCNF3uBxCslZsTozsXIpFJMTUzyJ+/9iEq1SrlSYXdvj529PbZ2dylVKjTbLfwgiAohAE1VMVQNQ9ex4nGsRJy2bbOyucFn9+4xPTFJT1f35187vo/rutyfneW//o//zsc3brC4OE+pUkExDHxDJ1RB0XVUTe2M2BBCPIsEPCG+RFUUdE3H0A0MI4ZuxFANNzpUrqpouk612eTTmzdoVsrkM2lCQoYHh+jK5dgvHbC2ucHM7EPqjQZDg0P80Q/fY2p8HO+pFYcnTYsVOJx+oWkaphGLZtbG48RjMXRdP2wuK8SLYugG2bRBOpliqH8Az/NoOw7tdptWu43tOlFlbRgVB4U81RxZUaJee7rO4soyP/n5v9BstngwP4+m6QwPDtCVy1MsldjY2uLXH37Av/7sp8zOzaHETbREIprJ3Dmrqus6mh61W1FUVXKeEM8gAU+IL1FVlZiuEzdNrHiCRCKO7djRRIkwxDBNAqBhV9ja2eHug4d09fQSi8VIWRb7xQMezs+zsr5OPpeLeuSNjjJeGH3Rn5oQX9uTHzaeFBWRyfyHXu8HIccnplheX2N1fR1D1zBjMZKWxV5xnwdzj7j74AFbOzs07TapVBLVNPFsm5AQQ9dJJBIk4nFM08TQdemZJ8QzSMAT4ktUVSUej9OT72JspMCJySlW19fY2d2l2W6hayqKpqGlklQ8lxszd4hZCTKdbdTVzXVu352h1qjz3nfe5eK5c/R0dR82jY3me3q4vofX6eT/ZBzUk19HbVmih5imaaid6lohXrSoyXGA7/u02m2arRaO636hL92TX+u6hqFFFeaGYRAzDHq6unj7yhUsK8HPf/VL9ksHZNIZUqkki8vL/PrGdT6+e5uy76KlU6Bp+J6L3W6RTCTo7+1jrFBgojBKT1cXCZm3LMQzScAT4kuePJzGCgX+9Ic/5OSxY1y/eYNff/BrZufnwfXw1eggOZpGvdlkbXOTmdkHeJ7L9u4ufhgw2N/Pyekphvv7KZfLPJqfZ3t3h529XfYPDtgvlShXKtiui+/7KJ0Hp+v7HJuY4K1Llw9ncsbjcWKGnL3793gSQDzPw3VdXM/rTAcJgCjAa5rW2YKPzn5JgP73cz2PdrtNqVLh9swMH966yeLSEkbnOoaHRw0MctksPfk8PV1d9Pf2MdDXT39vL8P9/bTaLe49mqVcqfJ4ZYVGs8ns/Byr6+vU6w0UVUM3YgS+jxIEJBWVk4VRvvP2O1y9fIXJ8XFGhobl6IIQzyEBT4jnyGYyJOJx+nv7OqPBYpjxBLOzDzkoV9CTSbR4nJhpUqnV+GzmHhubW3Tn80x0VhgsM8HMw4f8+H/8N+7M3KVerdCs12m7Lrbn4XS2fUPPhZaNqqoY6TQoCq+dPoMVjx+u4onfzu8Eu7Zts7e/z+bODtt7u1RrVWzbJgQS8Tj5bJaBvn4Ge/vp7e4mFouh65qEhX8HvTMr2bZtmq0mi8vLfHTjOm6tRhAEkDBRdANVUYipKqauEzcMrFSKVCbLhXPn+av/9Kd05fO8fuYsS2urbO/u8GBulr1ikVqzQTyRwA0CXNvGbtTpymQ5ee48b197kzevXuXi+Qtk02lisdiLvhxCfGtJwBPiOXRNi5q5ahqT4+O4roftODRq1WirNfAJHBvfjVOr13A9l1q9hq5rFIaHiMUMHi7M8WBujvc//ojZR7OErkvoeVHfME0HVSVUIHAcglqdXDbLaF+0BdXd3R1NzZCVu+d6esJBrdGgVqtRbzap1Wvs7O5GAW9nh8phwAujgJfLMdg/EK0o9fSSTqWwLIt0KkU6mXz2xBIBfH4GL5FI0N3dzVihwPLSY5aKRcqVKmo6iRqLoUTdusH3IAhQdB3FMCg3GuSzOU5PHydmxEglk6xurLOyvkalVqfVbkdzcG0b1fdJJSwKw8O88fol3nj9EpPj43TlcpgS7oT4jZQwlAZCQvwm0SzZqFrw0eIC//qrX3Lzk094+OA+ewdFQsPAMKOq10w6zfHJY5w9eZIwhA9uXGd+6TGVWhQADVVH01SistkQPwjwXRctDEloGmdPneZ//su/4s0rVxnsj1aX9Fc8ZIRheHh+0XYcHNfBdaPt11K5zNrWBivrazxeWWV1Y4PiwQEHlTLtVivq0WY7+J4frS4RomoqmmEQi8UwYibxeJx8Nkd3Vxejw8NMjo0yNlKgMDhMPpfDMAwMQ49WcGMxYkanevMV39L1PI+9YpGtnR0+unGdH/9/P+bewwe0OuP2NMOIRuiFChDi+wFu4KHrBtlUmumJSd65ehVVUbg/94j5x4+p1Ku0222cdgvFdujJd3PqzBkuv/EGP/jOdzlxbIp4PI4Zi73y11+I30YCnnglBUFweE7LdqIxYgDJhIXZGXukPuMB4jgOtUadxeVlPr55k1uf3ebOvRn2Dw4wYrEoNBgGRixG27bZLRZpt9sYnVYnYQgBEIYBEGAoCknN4PjEJO9+73tceeMyZ06eor+vLzor9gptGUbficLojJdt07ajRtP1Rp1SqczG9hbrW1ts7myzs7/P3v4+e/t71CoVGq0mtuvhBQFPvqGpnbOUivLVdmlhGM36DcPwsEmuAuiqimnoJBMW6WyW3p5eent66O/pYah/gJHBQYYHBsnnc6SSKUzTJN75iFb7lC+PUT3S/M7X0fbODjMP73Pjk5v86v1fMr+8RMN3ccMQUFEUFVWJWksGQYDjuiTiCQb7+4nHYti2jWPbUfNix6Gnq4sLZ85y6eJFrl2+zLHxCdLJ1DO3ZIPODwC2bdNoNQGIx0xMaTEkXnES8MQrqdVuU6vXKZXLLK2tsvD4MYqicOnCBSZGR8lmssSfMZ3i6dcflEs8WljgVx9/xK1bt1haWGRjbQ3H9wh0jUCJWkIQBChBgKKqhLqOahjRdmAyyfGJSd65dJnXz53jxInjDPQPvLJbg74frbLt7e/zaHGRucePWVhZYm19nd2dbUoHBzRbnZ5rvo8bBPhBgKqE0WqOokRBQv1iNSc8I+A9+e9T1ctRX8Jo9FYU/JROo14Vs3PuzErEyXd10dc/QOH/Z+9Nf+Q47DTNJ+6IvO+sqqy7WLwp3qRkyXar3ddMb2MWaPQsdrGLBfb/6v20M43Z2cEsMDvT7e52+5AtyRLFQ7xZJOuurDwq78y4I/ZDZBUpt+2x27QkivkICQiimFkRmVnxxu9439lZji0scXx5mRMrKxQLhaMFjjcN13MZmib7+/s8ebLGrbt3+elnn/Jk/Tn94ZDRaETgugieRxgEBOPcZVkSkULA81ElmcrcHEvHVrh8+TLffvsdThw7Ri6TxdD1X/nalm3T7XVZ39riszt3CMOQY0vLLM3Pk01nSCYTv/bvT5jwTeXNu4pMmAD4foBtO/T6AzY3t/js5mcMhgP63S67x44xPTVFuVQik0qRSiSPWnKHgsHQdSpT09iOw7PNDZ5nczQS+1H1DxVZ05A1FUmSkEURWRBQNQ0tFkePx0mlUmRSKU6tHue9a9c5sbJytC37JnA0O2eOGJlmZLcxMhmZI/aq+zxeW2Nt/TnPtjfZ26/SbbexTPNIyEmyjKiqyIqCLCnjLdjouX/ZPeuvuot9WQhKkjD++4y3mT0c18FyHDqedyT8dMMgnc0ys73F7s4ue3t7VGs1ZqaniRsGsVgMQ9cxDIO4YbwRgl2RFTJJBV1RSafSpLJZJF2jVCrR6fXo9XpYwyH2aIhj23hhiBcE+J6LZzt4toMsiCQScbLZHDPT0yzOz1OZmv7C6xzaCPm+T2/Qp9PrUavXqe7vs/b0KZ/duUUikUCVZQq5HPFYbNyanzDhzeOb/VtnwoRfgSgKyEo0D9fvdlh/8pS7D+7zyac3yJeKVCqznD5xirMnT3Bi5RjFfJ5MJk0ynjgSe2EYYpomu9V9TM9h/tgKi6vH0LXoIh+PxTEMjZhhEI/FyKYz49ixNPFY7AtD/fo3cFM2CEMCP4pjc8f+f5bjYNk2rXabneoumzs7VGs19ut1GgcHNFstWu029nCI7dg4YbRlLGkaSV0/inXjqGJ3KLpDftdexMvCUBQFVEFGkSTQtCNxF46Pqz8csvb8GZvr6/zs44/Q43Fy2SzFfJ5SPk+5UGS6VGK+MkulUiGXzb5o5SqRPYskioi/YhTgdUXVNAr5PIl4nOMrK/QHA0ajEcPRiG6vS6fbpd3tMBy9EPbDUVThsyyLIAwxPSf6TplmlPQy/q75vk9/OKDT6dI4OODxs6fce/SYB48fsru7w0G9waDb5a0zZzl34jSKLKMqyqQ9O+GNZSLwJryR6JqGLEkIQKaQR0rGGIngmCM629s839ri5zc+RY/FyGYyXDz3Fm9fucL1i5dYmpsnlUziui6tdpun688RBJE/ef8PePfqNZKJBJIojWd/BERBfNFCBBhfsA792g6NYuOx2Gs5PH4ojA7nGW3bxrQtev0+vX5UZak3G9TqDTa2t1nf3qbWqNPv9xiNRlEur+8TEh6dI0EQEEQBSZCRX6qcfpkIv+Z1wzAkDAK8IMT3XNzRkJFlsre3ixiGyIAcCsiaipJMUCiWWF1YYGlunnKpSKlQJJ1Ok06mSCaTGJqGpr2YGzt8/deJMAxxHIfhaITneaiKQjGfRygUomMZC+QXLfCAIBibJgc+/cGAn33yCd//8Q9ZW3/GQbuN4zgoikKv32d9e4uf37rJxzducOvu57Q7HazRCMd1CAUheh5JQErGyeSyTJfKZNMZJPmbdeM0YcJvykTgTXgjEUURRVEwDJ10OkOxVCZXKDAcDfF8H0WWsQOffrtFvdWkdtDk5t3Puf/oEf/H//y/cHr1BN3+gINOl06vh65pRxmdqWTyC6/le9EAeG8QCZ7u+NEb9Ol0u/SHAwq5PNcvXmRupoLyGlUdDisrnu/TarfZ3Nnm6cY6D9fWWN/apN1u0+m0GQ5HWOYI07Kx/QAvDKNe6OEChBDNuwlfqMp9fREEgVAQ8fHRFYV8LkchmyWVSBDTdVzHxTRHNA4O2KxWWXv+nHuffEpC19DjMWKJOJlcnlyxxOLiEqdWVzm2uMhCZZZcNossSa/Vpm4QBHiux+7eHj+/dZNG64BkPEEmnSaVSJJORo9UMkkqkYxGGX5BeHX7fRLxOK7nMRgOabU7dPsD0skk69tb/J9/8+/58UcfUj9o0ul2EAQxqtDJCp7nIisKmWyOUrlMOpNBM3RkRX5tzuGECa+aicCb8MYiCAKSKJFKJJmZnma6VGJzcxPXtZBUDVlSQBBwXZdOp0P7oEU6Fmft2nWSiWRUqTJNVFXFiMUivzrhxYbucDSi0TqILlTdLq12i263R7fXpTueS+oNB5iuy7GVFU4dW/2qT8l/F+9wW3E0ilpv5ojBaMRwOGS/Xmdza5OnGxs8evaMneoe/X6f4WhI6PsIQYgkyyiGMbbQkBBFgZc3T1+vna+QqDUcjJMyQkRZRjNiGHGRZDZDLJ1GTybptdq4vT6jwYBuvY6156LpBrFEgs2NTeq7u+wsLbGwsEC5VCYRj5OIx4mP2/vx2Ivt7q8z3X5/bHnyDENRSccTRxF+6VSadDpFLpsjnU6TH7e047FYNEMJyLKMoRsEfsDIMmkcNOkN+qw9e8btO3d4/OQJghS1tpWx1U3oB1jmCENRmS6VqExNR1V0aZJOMuHNZiLwJrzRKIrCXGWGd69cxbdtRq0Wm50OgeoiiRqaoqLKCo5pYltD9vd2+aef/oRas0k2nWYwHLFQqVDI5cllMgR+QPPggINWi4dra3x88zMePV2j2qjT63YJbBvfcfBsG9dy0AydfKVCqVQEAdSvSYv2sHXmui6mbWHbNrbj0O31qNdqbO5ss12tsrNfpX5wQLN9QLvVwh4MozZtGOKPVxsMIx61qsWo7Rod38tVut99fu6rQBRFFEHADwLqB01a3S66qhIzjLGvXpapYonjS8vENJ3A9+l0O9SbTRoHTQbdHmavT3d/n4+qVX72s58RagqpTIZyLk+5UGB2epr56Qrzc3OUymVSqRSaqqJpGoauH1V7m3dXQAAAIABJREFUv+o5PlEUUVSFkJB2t8uTJ2u096o4loWiR0tHkqoiahqpdJqpYolTx1Z55+pVTq2uks9mUWSFfDbLyZVjNFstur0et+59Trvb5f7DB+zv7SK4LpoaRzWM8WweBK6L6LoUk0neuXiJty9dZnZmBkWeGIRPeLOZ2KRMeKMJwxDHdXEch1v37vIf/vN/5qNPP2GvusvINJEVFUmW8QIf13VJJBIszM1RzBeIGwbT5SlOrq4yP1MhlUwgIPDZ7dv88KcfcPvuXYbDIY7vEY7bbWL0oniWhWtZLM7P82d/9md8971vc/XCxX+2NfhlHP+hh5jl2DiOi+3YDEcmw+GQ3f0qa8+fsb69xXa1SqNRp99uMxwOsf0AJ/AJw6iWJQggjWfn+Gci7vWbKfttOJypjB6M5whBkWU0TWe6XObU6iqrS0uUi2UKuTwCEPg+7Xabze1tnm9vsra1SavZJLQdcFwIQwQEFEPHyKYpFAvMT1dYXlhgdWWFytQM8Xic2Hh+U1VV9LEA/CpavLv7VT65dYsf//QDvv/9v2NjawtF15F1HQQh8oAMQgTfR1Vk4skkF86d4/133+XyW+cJwpDeYMDW7i6P1tao1moMTZPmQZPN7W0Gw0GUHyxK+J6HN55dna1UeOfqNf7q3/yPXDhzFnVsSP1N/sxNmPDfY1LBm/BGIwhCVBFRVd46dZpkPMHZk6f4z//tv/L5vbuYoxFmv4+kKMhGdJGqNw5ot7sghNiOzeW33sJ2bX704R0ePHnC840NNra3aHU6GJqOommIkoTv+7iOjed5SLKEkUmzuLrK+9/+Du9eu04ynvhSjz0cb7m2Ox2ebW6ytr7Ozt4ue9UqjYMmrXaL5sEBvX6fkWlijzdixTBEECAURBBEJCkSci/7z71pt42Hc4OHAu9wicByHMyxWfPO9hY/H+exZrJZ5iuzLM3NcXx5mX99+k/RdZ3+cMB+rUa1usdudY/tnV22dnbZ3tlh++FDHt5xiGsGiXiCZDZNoVQkXyxRLJWYmZpmdnqGY4tLrCwskMtmEL9kkVfM5fnO228jiQKPnz+j1utGxtWOjSzLKKqGpEgEvo/tOnRqNUzHplqv8fHNm5w+fpwzJ08Sj8VY39riwZPHAHi+RygKyLKMOzKxXBdN14knElw49xZ/+T/8Be9cvsJcpUIy8eV+jyZM+LoyEXgTJoxJJZOcPn4cQYBmp40iy+zsbFPb38f1PUI/wBmZeLaDKMlIkkS316fROqDZbvH5w4fcuneXTq+HZduoeiTuJEEk8H28ccxWGEKhkGd2psLF8+dZWlgkn839Xo/N930cL6pURv5zZpTbOhyyX6vxdH2dp+vr7OxX2avX6HQ69Ho9RuYIIQwRJSmaS1QUZDWy+HhZw70wDP69HsbXnsPkjKh6KR0lZXiOQ7Pfo95oIIyreju7u+zv7zEc9PE8n8W5efKZLLlMmsrMNCvdY+zuVdna3WF9fZ297U0O6g363X4kBNtN1ne2SaXSZDJZZqaipI3d7W1291aYKpdJJBJRzq5hYOj6UWXr9zXLp6oqeTXH0sIiF8+fpzscsLO3S63eIHAdBAREVY0WLCQNXwgZWibPtrY46HTwfJ94PE4YBHT7PfqDAb7vE/o+YeAjBgGaIqPqOuXpaWbn5vjW9bd558oVTh5bfW2WkyZM+DKYtGgnTHiJMAwxLYtOt8ujtTVu3fucew8esLu7Q2O/SvPggKHrgiQTTyYoFAoszM7huC7rW1vs7e8TBD7y4QJBCGEQELoeiqKgx2NkczmunL/At69f59rFS8xOz/yzzdvfFdd1GY5GUeqDY0d2Jd1u5D+3X2V7b4/17S32ajXarRZmv49lR75zXhC+2GZFQBDCL3jOTdpevx1H1iBBcCT4whAkAWQBVEXFiCeoTM9w7uRJTh5bZWZ6ilKhiKqqSGJU/bVMk3qzztr6c55vbrK5s0uz0cAZjHDN0VESiKiqqOkk2UKe2elplmbnqUxPM1Mqk8tmo83WZPJoji9ujBeEXiG9fp+d6h6f3LrJBz//OTfu3KbdamENR7iui6DICKI4XkoK8QIfUZSYmZpiaX4eVVHY3N6m0WwyHA4RfI+4IlPI5SlOTVOZneXsqdNcPPcWJ1ePRybhmjb5bE6Y8BITgTdhwq/AdhxGoxH94YDmwQH1ep3Hz55x6/591jaes1fbpz8YEDL24Aq86AKLECVXSDKGqpLNZJieqXD65EnOnjrFiWOrlAoFsplMZJwsvrptv0N/tuebG/zgpx9w+95dNre3qTcajEZDzNEI2/NwPS+yKjkScS9yW5kIud8rR79yD2f2ghA/DBHCECUkGhnQdRKpFEsLCxxfWeHKhQtcPX+RUqGAOc7otW2bbq9Hu9Om0WzybGODx8+ecX/tMevbW/QHAzRFQVMUFElClWWMWIxYLE65VGJhfp4LZ8/xh+9+m+X5hZcWYF7NMfpBQH8YxQHWm00eP13j3sOHPHj0iOreLu1OB9NxcPzosxgQjvOX5fFWbUgyEVkPrS4uc/HMGU6srFAqlSjk8yTjiaPZwwkTJvxzJi3aCW8ehxfY/87F7HA2LzPe+rNXVnjr3Dm+993v8uGnn/LXf/Pv2KlWCcMQRVbQdY1kLE4mlSaXybA8N8eZ4ydYXlhkenqaqVKJ9Dj6TJbl34uI8n0fy7bY2Nri73/wj3z40ccMO10sxyYQRQJJRFZVVE1D0VRkSRrHfB3Ozk3u937fHL3ngnC0/SqFIYEfYDsug04Hb2QSeB7ra2vcuHWL3Xqd+cocC3NzGIZx9Fye7+O6kbnw8vIyi0tLJJIJOu0OnXYHX5QZ+U4UD+Y6iH6AGAToms7NbJZarcGx+QVmyuVXasMiCAKyJJFJpkgnksxOTXNscZH3rl1nv16nWq3yfHOD+08e83x7m1anQ6fXZWRZWJaF67kIgsDKwiL/+1/9T3zr6lVy2SzJRAJNUVF+mwWK3/D7PmHCN42JwJvwRjEYDqnV6/QGA/zxBqiqKuiaRtyIEY/Hj+wnpPE8jyAI0eySqmJoOslYgs3tHeJGjGwqzez0NIV8Hn3c7spm0uQzOZbmFzhz4gTzlQrZdBpN08ZbqwFD08SyLBAgGY+jqa+2vaTrOjNTUxxbWsLsRUsSI9/D8n18AoIQQkICzyN4KfZLfIXVxAm/nMOzG4yrXGEQQBhACKIsEU8kUGJxNEEknk6RyGUpZLOoqnK0wGE7NoPhkMFwwMiyGAwGVPf3adRq9Ls9XMeJNnDF6LNr6NEsqC5JxCSZWMzASKepTJUxXnH+8eHP1x8OIYw+i7qqkctkyWdzVKamaS8sMjc3T6FYZn5rk4NOi3any9CMIsuaBwfsVKvEjRiVqWkWKnMYhn6U8vEyvu/juC6WZTE0o1g0y7ZxXBcBxl6XCcqlEol4/JUe64QJX2cmLdoJbxRPnj/jv/7jP3D73j263S6maZFJpSgVC8zPzrIwN8d0eYpiPk82k43C4nX9SPj0+j22d3f58MYN/uN/+S9IksT/+pd/ycWzZ5EkGVmWkWUp2hiUZRRZIQjDyBTZso5MghvNBtVajXjM4OrFS8xWKijy755gcWjVMTJHHLTbNBoNarUa23t77NXrVJuNKPO13aLVamEOBli2hX1o1CuMPdUil5Oj9i2Tlu2/mJfjucJxK5YwassGIYiCgCFLxIwY8VSa6XKZSjlamJivVJibnWVmaoq4EQNBYGSa1BsN1jfXeb6xwW6txn6tRqfdptftMBgMcAMfSVWJJ1Pk83kK2RzFfJ7pQpGZUom5mRnK5SmKxSL5bJbY2Ffud32PgyDA9Vx2dnf59NZNhiOT6XKZYqF4ZNZs6DqapiEKAq7nRuMCnofn+Xieh+973Lp3j//rP/0nfN/nr/7iL/jWlWhDNpVMHUX8mZbF0DRpt9s0mg32avts7u6wvbNLvdmk0+2Nk2rSXDh7lj//oz/m+PLKK3lPJ0x4HZhU8Ca8USiyQjKRJHQ9ttae8nTtCYEgoMVi6PHoApTOZCiXy5w9eYrzZ85y5viJo5k5x3VpdTsMzSHxRJxCLs+JY6ucOn4CQRDGYs5iZFm0Wi2q+/usra9z5+EDnm5sjKO7OrgjE8+yuHL+PNPFErOVyis5vsOLdCKeIBFPMDtTwXVdbMfBdRwcN/K5cxyHWrPBs/V1nm9tsbmzw97+Pr1uh16/h21b2K6L47q443tASZQQBZHwC5uiL153wsvbxJGIC14SdoSgShKGohLTVBLJFMlMhrnZOU4dO8b87CyFfJ54LH70HLZlMTJNfvbxxzx+tsb69jZ7tTrddgdvOATPQ5YkZEUhnkoxNTXD1Mw0SwsLLC8ssLK4SHm8rKGpGqqioIxHD16uUr9qdnZ2+H/+7//IjTt3kHUdJWaQyWTIZrMcW1zk/KnTrC4tMT01RS6XI6braJqOOLaaGVoWlZkpmq0DBuaQVrdLuVRiOBodzfTdf/KYO/fvce/hQ2q1Kt1uF9t2sIYj7NEIMYRjq6ucPX+eZCI5MT6e8MYxEXgT3igyqRQXzpyle3BAbWebp09D3MBHJCDwXIauQ6vXZWNjgwf37/OPP/4RszOzfOedd/jTP3if+UqFmK4T03VkSUQQIk+4Q4HjOg5PNza49/Ahn926yf27d9ne3aFtjuhbFq7j4vs+uqoRN3SkRBxF1367maLfAkkUkTQNXdOO/tuhCFmYnePU6gkGwyHD4ZBuv0ev36fdabO9t8fG9jZP1p+zubPDQbOJ2e/i+z4oCpKqIo8rloeRUKIoHAmZN6ktEO2lROI+DMbZvK6L59j4nkcIJFMpKpVZTh8/weljqxxfWiKbyZBIJkmlUiTjcfqDAY+fPeXT27fYqVap7tfot1oM21167Tbdfp+BY2MKIXo8zkyxyJnV45w7cYoTx46Ry+aIJ5Ok0smjqLPD9j98OSJcEAUURUHRNaREHFuEVreD1agh7UgoqsKdO7f4p7//PnOVWc6cO8fli5c4e+oUx5eW0cabsOL4ocgK8Vhk5CyJEk/Xn/P9H/2In3z0ITt7u7SazWjRSRIRhOhz6IUBru+hiSLFfI7zp09z4cxZMqnU7/34J0z4OjEReBPeKAxdpzI1xdzcLKWpMqlcjoFlIajK2BRWJHA9bHtErdFgv9lka3cXVZUp5fN4fmQ/EoYvMmfDMGQ4GtHpdqg1Gnx+/z43bt7kxq2brK8/o93rIagqoSgS+j6iKBKLGeSLBYrlEoYRQxS+PP+uwwu9ruvouk4xnwc4mmPqDwfsjAXezPQ069vb7Ff3aDfqdHo9+raN5Xr4QbQYEPrBWOiOo8jEFxfbbzKH83BhEBAGfiRsx1vIhqGjJRPEDYNUMkmpWGZhnD6xNDdPZWoaSRLxg4DBYECzVmd7Z4dbdz/n/toTdmr71JpNAttB8nziikohlyUvSTTMIWosxvTMDMdPnODSlctcOvsW6VQKTVNRfsmc2pfFYUvfMGIUyyXypRJho4Ht2Hiuix/42KMRDWefxkGTdr+PZdo4lo3nepSKRTKpwzZs9AAYjkY83djg8wf3+ejGp3z46ScMBn2EIERWZDQtjiTLBL6PrKpIyQQJ3aA0NcX83ByVqalXPms4YcLXnYnAm/BGoaoqhVyOlaVlzl+4yMBxWN/aZK+6T7/fRzfiyIpCTEnhuS6ObeNYFvcePGBkmnx293PmKhWGoxG24xxVbja2t/jpzz/mk89usP7sOTtbWxy026CpJAsFBEGMWqWjyDi4kElz5thxTh8/QSrxaj3w/qUosowUj6PrOulUmtXlZb799tvYtkOn26VarbKxs83a5gabu5HNRafTwRwOccxRNMsXhPiBjyAE44v9+Mm/AT56YRgSElXpCDkSdKIgoMsKhq6jx+MkkknKxSLTpTKLc3MsVGbJpNNIoshoNOKg2eTJkydUazX2avvsV/cZtNrYoxFBGBCKIigSM6USuWyWUj7P8eUVzp85y2A44m9//EPqBwfMlMoUxxnIqWSSmKF/bc5tKpHk9PETNOoN7rsO3XYLURDQdANFUQjDAM/1WHv8mHa9waPHj1g+tsK1S5f51rXrBGF0w+A4Dvv1Or1ej2ebmzx5+pTNrU1s00SRFVQtMt9GAM9xMIcjUskks/Pzkdny5cusLC9TyOV+b+bOEyZ8XZkIvAlvFKIoIooix1eOkctlefvqVT765BN+/NMPuHXvHkPbxrJNFCly+9cMIwpQH/S5++gRz7e2SCeTJBMJDCNGKpGk1Wrz7OApP/7hD/nRBz9hZFmIWtSiEiQp2pZ0XcQQEvEE6WSCy+cv8mff+yO+de36UQXtq0YQBCRBQBJFVEWBWOzoz/wg4NTx49GyiG1jmiaWbTM0RzSbTWqNOs83N3m6ucFutUqr1abf7+O6Nq7r4gcBPgIh47xaQfjazvK9PEcX/fvLFjIhkiiiqyqJeIJsNkqQOLa4yOzMDJlUingshuO4mKMRB60Wn926xc7uLlvbm3TbbUJvvDkLhEKIHo+RyRc4/tYZVpeWWJpfpFwsUswXiMViGIZOPBYjbsS4/+QJNx/cpzcYjLNdA0RBQJLEr1WKw3S5zJ9/74/JJhJoksio36c7HEamxp6HpChIukyoKDSHfWo3b/DwwT1GnS6pWJxsPk86lWKvXuNHH/6Mfr9Pu9tlOBrieR5q3EAIozau70UpLZIoks9kuHj2HO9/5zu8c+06lalp8tnsKzdynjDhdWAi8Ca8kcRjMXRdp5DNk01lOHHsGB/duME/fPABa8+eYvW6UdqAriGpKn4Q4rounU6bRm2fQi7HuTPnALj94B7VvT2e727Ts0wCQSCmyEiqEvmUmRZ2t0s5X+D4qVOcOn2ayxcucOHsWSpTU69FZeHlWb40L8SPHwTYjo1lWQyGQ/qDAa1Oh3qjwdbuLjt7u9E8Wb1OrdmMZvmGfTzfi2b5FOVXzPJFr/tlLvkfVmP9IKouua5DEAQIooisyOQyWYr5PMtz85xYWWGmPEVynEDSHwzYbzS48/ABtXqdQbvDsN2h3+4wGA6xPBdXlkhnM8xMl5kpTzE3Pc3sTIX52VmK42pdMpEkOa6iaqp6tL19KHxlSQLCF+MBwddz3vFwFOKtM+cYmTaxeJLHjx7x9MkTGgcHaOk0SkxHVqNqnuPYeJbJ050tfvbZJ0xPzxCGIaok83hzg2arhaJpCIKIH0Lgh/iOjW/ZIIASj3NseYU//va3effKVVaWlpifnUMf50BPmPAmMhF4E95IDit5UizGXKVCOp1C1XQc3yeTTLK/s0O728H2gyi+y3UJBQHfdTHNEbqqMTJNWu023V6PWr1OZ2gSygoQWWCEtoPruigIZPNFTqyucunCRc5fuMDK0hKFfOGX+nq9DhyKDlEUUWSZRCzaKAawbJtOt8vufpXt6l4k8Go1qrUa+9Uq7UaDTrdLz7axXJcg/GWzfOJ4lk/4whLLq9Z74ctbruPnFwQBTVWJGZF/m67rxGIx8tkshXye2ekZCoUiuq7jWha9XmSds7axwcbuDru1fTzTQnQ9dEkmnUwynauQLpeZma0wUy4zMzXF3MwMs9MVKlNTpFOpLyzC/Mqfl8OfN3i1J+IVIwgCsixTKBQ4feoUqqaRNGIoIUhPn9L3XBzTIlQiGyEkiRCBznDIs60tBqYVWaGYJsPBkNFggOEHiOP/PwR0VUMzDLLpDFOzs5w/d44/ePc9zp48SSqRJGYYX6uq5oQJXzav59VlwoRXhCiKR9W8d64kOX3iJFs723x+/x73Hz9mc3eXvVqN0aCPMzKxzBGSAH4YsrW3S7PTgTA82kIVRDGqSLgumiyTNQwWZ+e4cvkKly5cZHVlJUoj0CPTVsu2j4SSJElHyQavM6qqks/lSCYTLC8s4rgOrutFFdBul+p+NMv3eH2drd0d2p0O3V43muUbmeNZPh/X94+G9iUxWuCIzPmEo83V35aXhdwLXswGqqpKMh4nk0qRz+UoZHOkkknisRhCCEHg0+/3+eD5c5oHDToHBzgjEykUQJIJFImpUplMMkU+k2FxdpaTK6sszs8zNT1FJp1BURTU8UNRVVRFeS2quL8JQRgeZeKGYUgiHuf08eMszc1xfHmZC+fPc/P2LW58doONnW36jo3jeeMZTZFuv8+z9Q32643I8280xA9D4rEYhqKiGzHUmEEskWSmXGahUuHMiRO8deYs87NzpJNJDF1HEr9eLesJE74KJkbHEyb8Aq7nRskPoxGmbTMajWg0m9TqNTa2t1nf2ebZ5iZr6+t02m0UQUCRRGRJRlUVkuk06UyOE6urvHv5CqdWVymXyhiGgecfCp0e7XYbz/NJp9Pkc1nKxcgM9nXHdV2GpolpWbiOg+f7yLKMpiooioIoiARBgO06WONoquFoRPPggFqzzvOtLdbW19ndr9Jqtxn0+zimFRniCgIB0bameJSd+6KieCj+fhWHv+4kUUTTNGJGDMMwiBsxYrEYiViMuKYDIaZp0un1aPd60cyb6yJ5PmIYEIoCmmGQzGaZnp7hxMoKywsLlAolCrl8dNNg6MQMA13TkEQRPwzGnoQuruse/cyGrpNOJtF+gwrezbuf89f//t/x+NlTSoUil869xbeuXuXCmbPEXoow+6oYjqLt84NWi263i6wo5LJZMun0ke+eaZrU6jUerq3xs89u8HhtjW6nRb/bxXHc6DviB7hhSCabZXVpiZWFBZZm51icm6NcKlMsjOcTNY1YLEbMMCY+dxMm/AKTCt6ECb+AIiukkwrpsWt+GIbYi4uRhchgQLfX56PPbvDX/+FvGFkmC9PTrMwvMj9bYW6mQqlYolQsHiVixAw92rTd2uL2/fvcf/iQnc0tqtvbJBMJvvWdb3P58mUS8fg3QuA1Wy0+u3uH+48esVOt0up0KObzLMzOcnz5GMcWlygXixSyOWRZHse3+diOg2Vb9MezfAftNrVGPbJs2dzk+eYmu7UatUYD0zSxHRvfj6o/kiyj6Try2E/wn2XrjluGhhaJrsNzrasqsixjWhaDQZ/9vV3Mbg97OMK1bdwwxJMlRE1jqlhkfmqalYUFFufnmJ2pUC6XyY2rfIl4HF3T0FQVSZKiRZIwmt1sHBzwbHOdJ8+fsbmzQ7VeRxREdE3j1LHj/On777OyuPjVvWmviOFoxMbOFp99dpMPf/IBw9GQmfkF5pcWOX3iBOdOn2ZhdpZiIc/y0jLXLl+hWtun3mhQb9TZ3ttla2eXZ1sbbFarTJXL/Nu/+De8c/kK6VS03BTNJ2qv9Ub2hAlfBhOBN+GNwvN9bNvG870jU15RFBBFCUmSIquQ8cUZeKnKYmDoBulUmnLRZb9RJ5lIkMtkOX/2HJfOvcXCXHTRz2ez5NIZNE3Dtm2arRab29t8fv8+t+/f58Gjh1R3d2lW96MNzNMnMS0TP/C/4rPzaugN+jx48oQPP/2UzZ1tms0DCmOBt1fdZ3dvl8r0zLiyk8LQx5UuXSedTB3N8pmWRavTplqrsTE3z9zs3NgAeJ9Ot0O/16Pb69IfjbBdBz8MccfWNdFyblTRe9H+FhElCVEUI+HlOISuC2FIfzCg3evR7/fxLQsFgWQiTiqTJZ7Nki7kqYyjw44tLbE4N8d0qUw2nTnyV3NdN5o/7PUwLQtzfEPQ6/ep1vZ5uv6cJ8+esbm7Q7VRRySaI7NNk+uXLn5l79erxA98TMui3mzw8Mlj9us1ivtV1qs71JsNDtot3jp1moW5OfLZHKvLy8xXKrS6HQ7abXb2dtnc3iaRStI3LZKJBEvz8xxfWYmqv7+i7Xp4k+D5Pr7n4Y+XUODFPKCmaeMllQkT3gwmAm/CG0V/MGB9a4tGs4FtmriugywrKJpOPB4nlUhE7R5FQddUVFU7inVSZPmF8AMkYKpQ4P1vvce7164Rj8ePBrsFBGr1Ok/Xn3Hj9m3+8Sc/5tHaGqZlYZkmjuPgEiDGDVRdRx23Lr8J9AcDnjx9ysMnj+l0omzUbrfDzu4ON+/cQTMM0uP837npmWjpoFxmqhQ98rkc6ng2zdB1VhYXWZyb591r13EcB8u2OWi3aBw02dzZYX1zk63dXXZrVdrtNrZl49g2XhDgh9EsmCSICIBtWZijEZ7vI/oBShiiIiCpCoqmsbSwwHR5ivlKheX5BRZm5ygUCuRzOfTx+6QoCpIUicRev0+z1cJ1XVrtNvVmg1qjQbVep9qos723x369FvkFjoZYloXtRW16ggAhCJgtlhiNRl/12/ZKEIXIYkfVdcS4wTDwceo1mp02T54+5fv/9ANOrq7yR9/5LlcuXODY0gq5bJZ8Nks2nWamXObM8ROkkymePl9HCvlCy933fdzx+bMdB8exscbLTOZ4k3s4HGCPxwMUVUXTDYrFIkvz82TT6a/s3EyY8GUzEXgT3ij6/T4Pnzzio49/zpOHD9hY38AXBeRYNLytjEVFMpGkMjXFwswMczMzTJenmJqaYqpUIp1MRr5avo9rWZhmZHqcTCSRJZlm64DtvT0+/vQT/tvf/S33Hz9m6Lp4YYgsSQiKghiG4PsoqkI6lSKVTKEo34yvYzC2TrE9l1AUEcfxU4EoYHoOw57NQbfNs60NPgEUUUSVZHTDwIjHyeXzTJfLLM7OcWJpibmZCsVCkdT4vBuGTiGX5dTq8Sjc3nUjfz7LZL/RYGdvj91qlUbrgGa7Rbvbi7z4/KhCKooikiyRTaco5wpMFYrMTE1RmZ6mXCxhGEaUCDFu90aJCgGu52E7Ds2DAxrNBtt7uzxeX49arrUarVYLazTCMkeRiAsCvJeqSKIggACiJKGIAp4XRZq5wG+/E3u4GPIK37hXgKLIpJIp0qkUiqogSCKiIoMiM3Id+pZF68YN7ty5w5kTJ/jXf/avePvqNeZmZijk8oiCiG07jEwTz7ERxoI6CALqzSb79TrV2j7V/X1qSoBPAAAgAElEQVS2q3ts7e2xu79Pf9CPZj5dF2dk4o1GSEHIwtIiJ06d5u23rx+JyAkT3hS+GVeUCRN+Q7KZDFfOX2TQH7C5vcV+r4PpOsha5HcXtfeii6cuScQliYSuUyyXKc/McPXKVf78j/4YXdPJ5wsEQLvXo9ZsHG3v3X34iL/9px/w4w8/ZHtnm6E5wogZGGPBEAYBgS8iiBCPxVmaX2B5fuEoZP71R0AQJURRQhhvM4qigCRJR5WvIAiPYr6cIMDxbHq2TdDpsFmtcu/hQ2KqSsrQyaTTZIslZqZnmBpX+xZmZ1mcnWemXCadTB7N8i3MzXPu1GlGpolt21iOjeM4+H5wNI936LWnqWoU1zaey4sC77WjBIX+cMButcrmzg7bu7tUG3VqzQbVajWKbet26ZkWpuPgjjdHRcYVJ1EYzwZK42UQ4WgmLwhDCAQEMQRRjB6/djXkC2f26OcXReEoEu7rovPisTjL8wtsbW9Hn2dRQJQkJFlGHrfGPdflYDDgswcPqfcGrO/s8q/+8Htcv3SJ/nBAtVGj3euQTKcp5gsYhsH23h7/5R++z6c3blCvVmk26gwsi6HnYfn+i5SRMMB3HDzbxlBUTiTOcu7MGa6ev0guk/mqT8+ECV8qE4E34Y0iZhjMV2ZZXTnG0vIST7c3OWi1sMbB8OLY8d5xXazhkLZtIwY+u406yZ1tHN+jMjWFoqjEYnEyvj9uGbZJJVK4ns/O7i4PHz/i6fNnBIQomoYsK4iCgOd5BL6PJIqkEkkK+TzFfJ5MKvWNscqASK4ciY4wyvSKslujP43EyRe97Q7npjzXZWBZ9Dod9j03qtolUxSKRaZLJaZLJRYqs2zMzjNXqZDNpMdWJnES8TgxXSebSke5uL+mxHW4QBMEAaZl0RsMGNRqDEdDur0eBwcHbO3usLm7E7Vam80XZs29btRmlRXEcZVJluWjeT/gqLr2S40KwpcevwUvfPDCV+4J+CpQZJlMKkUxn6eQz5NKJPHHok6UJGRZHo9EBJiOzdPnzyjlc5w7cZLV5WV6/T4H7TaW7ZDL5Ukmk1RrNdY3N/nZRx/z2a3PGHS7jIZDAkkERUVUo/xdAQg8D0WWScZi5HN5lpaXWF05xnxlFlWdbNlOeLOYCLwJbxSSJKHrIvOzs7x3/W3isRj3Hzzg4cOHVOt1JClKoFAUBXQ9Uh9BQOB5tFttbt68hW07FApFfN8nlUrS6XV5trGBZVnEYwbrm89pNep4tomeSIwd+AU8x8W2TIQgZGZ6huWlRd6+eo1SsYj80nzfm8IvCh9RFBAFEUnSUDWVQ0UYBgFu4FOt7dOo13ggiiiKgqZopNNpisUi09PTLFQqLM3PUylPUS6UMAw9muUbz1BKokgIRxu7tm1j2zajkcl+o87e/j7r25ts7u6yX9vnoNGg3+9H7VbPwwsD/HHLVUsk0EXxaJHjC4sd/DKvvd8Nz/MwLZNOt0t/0Kc/GKBrGt1+l+FwiGnbyONj/Cr93w6TN0qFItevXMV2HJ5vbLBX3YsqlbqOpKrouoZrgzUY0GrUWd98TqmUZzgy2dmv0hv0SaeS2I7D//t3f0u9XuPh/ft0Oh0kRSFeKETPN66KCmFI4LrgekyXy5w8eYrTp05x8dxbzM/OouvaG/f9mjBhIvAmvHEIgsDC7CylYpHrl69y49YtfvLxR3z62Q2qu7v0BwNQJBCl6OItSoiKiKgodE2Tj27dRNd1Mql0lEubTFA/aLJb3UPXNNaeP6cz6EftSUEk9AMs20aWJTKZHMV8nnevXuOPv/tdrl+8RDabxfN9TNMEwDAMlNc04eJ34VAgvRBKL5BeSpzwxhWhkePSGvR5trONeFtAkyR0XUPTDYxYjGwmQz6bY6pYZKpYJBGLEYYh3X6f7WqVar1Gp9ej1+thmRa2ZWE7Dm7gExAeZeZGRnsgCCLKeMP6yxYLtWaDn9+6yQcffsjNmzdZ39pEVlR63R6KJKMoSjTHNq6afdm4nveFz+/i/Dz/21/9W46vrPAPP/kJH376CQetFiNrhDMaoakaohCllXQGfR4/f04gCFi2TbPdpnnQotvv0+v36PZ7WJZF6IfIsfi4PCyMt+AD8H1wXXKpFHNnz3Dt8hXeuXqNS+feIpPOYGgTcTfhzeTNu4pMmADRVqyiEDcM4rEYZ06d4r3r1/nhBz/h5u3brK8/p906QBjPSImKjCjLCJIInoDtuPT6A2KGTjadptvtsl+vo8gKzVYLy/MRFRXLdQktC9+2WVhe4d233+Htq9eOjHHLpRKCIFBvNrn3+CEgcO7kKUqFwld9ir5W/CpRddhiDYMQ2/exBkPC/oBQAHFnG0mUUGUJQ5KRBZGQEDcIMD0Xx/ej2KsgOErGiGLSBGTxxezc14Fuv8/9x4/5/NFDGu02o+EI3C7VjXUePSyTjCdwXQ9N1b4SgdfudLj76CEQcvbEKYr5PDNT07xz5RrFYonL5y/wyWc3+Ojjj1l7/gxP8xAkKfqOeD5b1Soj28b1PIbDIe1ul1a7g2lZhEd97JDQ9wk8j8D1IAgIg4BcNsPKiZNcunCR97/9Hc6dPk0+myWd/GaNPUyY8NsyEXgT3mgkSaKQy1HI5YgZBp7vE4/FKWYy7O7tMbJtRo493or08YIAAQE/8LEdF89zIQTX9egNhkiSxHA4xLbto4H6RCzO9Nw81y9e4r1r17l25Qq5bBZDN2h1OuxWq6w9f8bn9++TTiWpjA2S4V8Wx/XVEx7981sPmf2WHHqcwWFHNxJ8QRDgeR62b9Mft+443GiVJARVQRobEstKNB/5i4Lu6xTyo0gyyXiCZDyJLCsIoogky6iqduQjeGiw/GUSjtvorXaL25/fptvr0253WF1eoTI9TSqZ5OTyCoVsDlUUCcY+hfutA4bjip9t27RaLWzHidrnts1gNKI/GuJ5Hto4yi0kRJZlFCOGJsvEVI2YpjFbqXDqzFkuXbjI1YuXmJuZ+VLPwYQJX1cmAm/ChDFTpSJ/8v77XL90iYNWm529PXaquy9ZbrRpj+OUDm05LNvGdhy6/T70ekBkf4Lnk08kSOfznD11mj/97vtcOHuWXDZLMhFnOBxRre1z+95d/usPfsD9Bw/odztcOHuO82fOMj83iyIrka3KayjyjhYqvgSN9LIQE4SXjKtlCS1UCQHhxYbH0dzcL2sHf1miLnxpyyL8DU5UIZfjvWvX8V2XjfXn1Bs19FSSyvISly5f4v333htvnOpfxo8PROfK86LYte3tbX704x9z+959ktksZ0+f5s+/9z0unDlHNpNmqlDgu+++y5mTp/jW/Xv8wwc/4cGjh7QPDrAsi26nE32HxiJbliTKhTy6pmFoBpqmoaoK2XSaQjZLMRcZT89OV5ibmSGfy5FOp0nEvymb6BMm/O5MBN6ECWNURSWfUcmlM8xX5jh78hS260SmxJ6H67k4jnNktmuaJs+3tvjws0/5/MEDqtUq3W6X+dlZTq4e5+rFi1w9f4HlhUUy6TSWbVNr1Hm49oQbd27z8aef8vmD+3T7faxxxa/S67JXq9NoHpBOpUgmEkivmcCTJJl4LEZM17FME991CaUoKQRBeLXbB7+El82ov3ZGcWFI6AfgBwhBiCwIKOLYI+/XIMnjc2oYSAKRSbIoIqsquh6NGUSZt19eBS8IQ4ajaPFjt16n3u2y1zpA7HbY2a/y0aef8Nap07x99SqXz19gbmaGpYV5Th5f5c/+8A95trHBz2/e5NPbt3j89Ck71T3SqRTT09O8dfo037p8leX5eYxxnu/RwowcmY6rqoqmqEfG06/jjdCECb9PJgJvwoRfIGr7Sciy9M8qIlHbNWoB+r7PVKmMJIlIghhtMMoyZ0+f4S/+5E9479rb5LPZoyirZ5ubfP+H/8SPPvyQzZ1tao06g+EATdPQDO0oQs2yLSzbJuF5v3cx9PsgZujMTk8zUyzidLr0un18VSEQRZBfVCS/Ti3Q3ycvhIeA77k4oyGebRNLJEjmcpTKZTRN+7XPYVkWe7V9qvtVrOEQ33EwRyPa3Q77jTo7+/sEIWTSKeLGl5RnfJQh7GI7Ln4YIMoiuqaDIFA/OODjWzdZ39nhw09v8Aff+hZ/+v4fcuHMmcgiKJfn2NIy87Oz/H//+PcMbYuZcjkSd1eucv3iZeYqM1HM3HhhaSLiJkz4zZkIvAkTfgsEQUASJCRRQpGVaMB7YZFqtcrW1hYNaiQ0nXK+wFSxiCRJmJbFQbvNwyePuX33Lrc/v0NvOCAUQhRNRVFVgiCaPQqD4IXP2Vd9sP9CkokEJ46tclBvEI4srMEQ03MJggDHtl8sM4y3KL/oife6HnXEy953kcNOQBhGSyCEIaHnIwkimhFjZmqaheVlzp48RTqV+rXPazs2jYMm9YODaHEnBM91GA2GdNodGs0DErE4ccMA40s40F9GOL45UiKT64AA0zbZ3N6i3WqRSSaZr1Qo5HLks1l0XWeqVGKqVCKhGygIZOIJliqzrCwskstmIrE4YcKEfxETgTfhjSYIAizLYmiamJaF7/sEYfDLKwVheGRkqyoqhmGgqhqryyvU9mt8fus2/mBIp9mkuldlv14nn82yV6vx/R/9kB//7Kc8fPSQQacNgoBmGMiqepTsIAoisnKYrjAORn8NKxalQpHvvfdtKuUyS4uL3Ll3j+r+Ps1Wk1a7jWlHSyte4IMfIIpEbeiXZ+NebrN+jTna7zy07BjP+gVh1MIMQ5AI0SQJQ9FIpVKkMpF339lTp7n41lucO3macrH4a19nOBqxvrXNxt4uZhgiahqh52EOhtTrDTZ3tonFYmRSKTJfVhyXEKWTaFrkayeP85SDIEQSBQzNwHMcHNNk0Glz/8E9gsCn3e3yp3/wPjNTU7Q6bWrVKr3mAcFgSFaPcXxxmdXlFVRVozcYYJomjhstYARB8Eu/E2EYjj0UJQxdJ24Y6Lr+lXoCTpjwVTMReBPeaGzH4e7jR3xy8ya3792lVqsxMk0kRUYYRyvBi9QDVVVJJ5NUytOcWFlhZWGR+bk5EqkklcUFFjptHFHg48/v4Arwnetvs9+o84OffsAHH3+M49iEqoosyciKAmGIY1sIokA2k2VuJjLrnZmaQhKjtu/rhqoo5DIZLp+/wKn/n703fZLjsM80n7yz7qqurq6+TzQajYsAeAAkROoYybI9M/LuzDpiYyZiD0ds7Nf9U/bjxkTsemd3ww7PhI+x57AsiRJJkCAIgCDOvtH3WfeVWXnvh6xugrIoyyOIEMl8IpodAKPRVZlZlW/9jvedPc2/+Oc/wDAMOobB7v4eCysrrG6ss7G7w9HREd1WC7NjYHsuVuDhwYlx8LMxX88KvxfF8dboSQpGr9IaBKFvnioc5+rGiSUT5PP9jAwOMj0xwfzsLGPDI6RSKVLJJIlY/GR27vOe1/H112y1WF5bZXV9A8txUWMxHMeh1TVZ3dpE6mXnDhYKDBWLX8ixkESRVDJJPB5nanycseFhNne2MUwT27LQtFD0+QF4nsvW4SEH9TpuEHDx7DkEQeCdD2/y8YP7mASMn55leHKCRCpFtVZje2eHtc0NltbW2D3cp9FqYfe2cE8+APSi/zzHJR6LUSwWuXT+Aq9ducLF+bMn4xEREV9HIoEX8bVH6P2n1aizcP8+h6USUiqB2KuuEfS2HoMAQQBJlIjJMhlFY2ZikqvXr5MfGKC/UOD6tWuUKhV29nYRBDgzM4NhGDSaDdpGB0kUw8QBKWxjeY6DY5hksxkunJnj0oULFPL9qMozsUrHv58vl22Kpqpoqkoumz2xLjk1NcWlCxdptdu0Ox2OSkfsHx6yt7/P0+0t1jY32djZptGo0zVNhCAIK6axWFghejap4Vh8/4Ye/88fa/+ZKDXbNPE8l0AQ0HSdTCbL+Ogo02NjTI2OMTI0zPDgEMXCAIl4nGQySSadDpckegsn/9CZDIIAz/fwXI9mu8Xe0SFHlTKiIKCoKggCtu9RqlWI7epMT4xjds3f0NE4eVDh996xOX4u/X15Lsyfo1qt8fjxYxrtBqqsICkKshIgiKG1UMfoUG82wszZI4G79++zvbdLcWCAc/PzpJIpFtdWef/Dm9y+eZP1rU0ajoXpuni+F/76Xl50aD4t4Ns2XqtDsVAgfvW1Y1/qiIivPZHAi/haI4kiuUyW6YkJ9nd2WV9aoVKpYnctfMdBlMJ8UYTj7puP71m0HYcj06LZauHKEpPT02TSGVRNw7YdyrUqoihycHSEbdu4vfzZ4yinIAhwPTcUCUFANpXmwtxZLpyZJ5NOYZgmjVYYSeV5HgQB8ViMQj5PPP4FDdE/R46FWSqZJJVMnvx9o9XkqFRm/+iQ8c1NRkdGGVl/ysHhAdVKhVarhdHt4vR87QSEXhf32JT4s8f0eSDQswDpGel+WrUDn7AVmEqmiOkaqVSKvr4+isUi05NTzExMMjU2znBxkKGBAbKZzK8lyh3Hwex2aTVbdJpNuu02aq/iF/gedrdLs9GgrKo0Gw0c234ux+AX0TEMSuUyhmmGPnySRCqZJJNKkUmnOD9/hkq1zO7ONvVmA9dzCcRelJggnAhzz/OoN5oYRpfdgwMqtRq5bBZBFKnUqjxdf8rG+jqPHz2kVKkgxDQERUGUpHBus5cG5/s+vuciej6qKNGXzXFqcprpiQlymeyXsvodEfE8iQRexNcaRVEYGxmh0N9PXzqLrCjkbn/E0tIS1UYdWQJRkj7NvRQIqwdBQJD2MYG7jx6wurlBvr+fZCodipZyGbPb5c6DTxAFEdu20WM6vufj+R6u4+B5HqqiMDg6wqXLV/j+d77D5QsXEQSBJ8tL3HvwgIXlZUyrC4LA7PQM/+x73+P09MyLPmzPjWQ8gT6iMVQscu70HF3botFoUqpU2N7bZWV9jaebYT5sqVzGMgysbhfH93A8j+AZ4Rw842t3srf6D4irk2WWZ74Lvdar54ezmIooosgKmq6j6TEK/f1M9nJvZyanGB0eZqC/QDaTRlM1tF67VJWVX0vc+UGAZds02226VpdsLEYxncYTBHwCgiD0SdQEEckPEP3f7ILKzv4+f/3DH7LydA1RgLgeY/70aS5fvMjk2DjfeO0qiVicw3KFru9RbzQwTSOs8skysiwjiSK2Y7O4soQf+Ozs71KqVLAcm539PdrNFpVymWaziSeJJIsFBKH32iNACPvhYT607+P6AX3ZHHNzc1x79TV+77vfY35uDl3TwjzpiIivMULwZV9bi4h4Tli2RaPV4tHCE/7Tj/6Ojz+5R/WoRL1ao+u52AJ4goAoiL3ZMBACwg1JIaxSIQonKQrxeJzBYhFFVsJqXKuF2TWwLQtVlNBlhfNn5vlnv/9Pee2VV5gaG6PVbnPz9m1ufvghjx89YnVtFcf3ETWN169d43/7X/5X3nj1NWRF/soOkPu+j+t5n/oNdk0M0+Tg6JCd3T229/fYOzhg7/CQ/YM96pUKrXYb0/NwfP+kiygKQpgnKwp/fzD/OPGiJ+bCyqCAKorEFJlUMkWuv8Dw8DDDxSIjxSKjw8OMDg1TLBTQNf1kGUZVFWTp+Z+PAPA9D9fzOCqVWFxZZnt3l0qtFkZ4BQHJZJL+vhyFfH9o/Ds8TD7X91wfh+/7OK7LB7c/4n//N/8HNz/8kMCyUUSRUzOnOHf+PK9fu8brr75GKplkfXuLj+7c4W/+83/i0eICXdfB8T1UTSOmx0mlUmRSKRzX4eDwEMMwkOXe8fPD80IAghh+oAp6Cyt+4CMFAWoAuiST7cvRN1DgyqXL/NPv/Q7n58+SSaXQ1F9uORMR8XUhquBFRPTQVI2BvMZrl19mbHiU1bee8vTpU9Y21lnf2WZjZ5fdowMM08TpdsGywA9AlE4qDMdVIHyfbrNFs9lE1jRkSca3HSyjjUTA7NwZXr58hW+8/gavXL5Mvi9Pq9Xik/v3+Ys///d8fP8+ludjBeHMl2N0ONrbY3dvj6NymXxfjljsRflh/GYRRRFVFFEV5SSZIAgCxkdGOX9mHsMMBZ/Z7VKulNnd3WV1fZ2Hy8usbm5SqVaoNRr4jo3Qq8IJioIgSWHFzvMIbCcU6IqCrGnksmE6wszEBBdOn2ZmYpKhoSHyfXlisTAK7PhLU1W+iCkvgU9n3IaKRbKZDJcuXMSyLTwvnEdTFPnTqmHPAPh507WssKK6s83R3h6Neg1FUXEUhaX1p2xsbXCwu0NC03jl5Vc4PT1NrrfNe+PmB9y7/wmLK8tYtgseWI7DQekI17JwDRPfdU8q5AKfVsjxPRAF0DSUXhzbyMAgk6MjTI2OMTM5xfT0NKemphkqFqMUi4iInyMSeBERP0cykWB2eppcLstgcYDR8THGt7eY2Nxia3eXWqOB0W7hdDo4loVzvE3ZqxxJoogsitieS9006ToOtmsRV1UKQ8MU+/q4/NIlrl29ypXLlxkuDmGYJivr63zy+BHLa6scVEroyTRKLIYPBE4X0+xQqpQp1aokk4mvrMD7RQiCQEzXiek6+dynf98xOhxNTDI6MkpfocDQ0BAHR0eUymU6zSZGJ7TZsHwft7f5qogiMUkmlUyQSKdJZTIU+vsp9g8wOz3N+bk5JkfH6M/nif+WHGNZlv/e/OIXhWX1PPhKZQzDCC1JFAVJ1zFNk1q9ydLTVe49fkSmr48L82cZHhzilUuXEQBNVtAVlaNajYbRweh5ISZicbJ9eVRJxvV9vOOsYCGsviqCgKJpKIkE8WSKXCbD+MgIkxPjTI2NMz4ywtjIKP3PuWIZEfFVIRJ4ERGfQzaVJj4zy9TYBNeuvIJlW7Q6HRqNJs1Wi06nRbvTod3pYNk2frjih65pxGMxao069x4/YmF5mY2NdYYHBvjuN7/Nm9deZ3x0lIFC4WQIf2N7i7/+2//Me7c+5KDdRoonkVQVUZQQRBFBlDFsm+2DfXb29yj2fvbrTkyPMTwYVtrOz8/T7hhhUH2rzeHRIXuHB2zv7bJfKtExOgiCQCaVYmxoiInRMYaKgwz0F0gkEifnLabraKqK/Buohn0ZMbpdtvf22d7fx7QshN41KYoSkqoixZMctlr8x5/8iHq7TSqZ5ML8WSbHx8llMlw8d56tnR3e+/AmP37nZ2zt7TI5ORnO7507Ty6TxTDDXGcIEAUBTVVJJhIkEwkSiRTpVIpMJk0qkQgrlr3YMjWas4uI+Fyid7CIiM9B7g2GP1vFCXrxTF4vquw4sswPgpMtztAMWaTZbvPG1Wvc+OB9/vRP/4SYJFHM9zE5Mc5gsUgqkQyD2nd3+fjePe58eIvVxcWwYpFKhjNJoWYEIdw+NEyTjmGGm7V8Nvnhy2Sh8rwQRTGMetM00qkUA/3ByTlyXBfbcbBsG6e31CIIYdsztHAJB/GV3vzX1/H4/SJ+/pryfQ/LtrBtCz/wT65HBJA1lbgi41k2KwuL6IHAuVOzJOMJxkZGyOVyyLKMIAgsLS8RlyUmBgb473/wB3zjjeuMj4yQTibxvHBp4vh3ij0T5WNjcan3PTpHERG/OpHAi4j4RxDm1Mq/0gsnpsfIptI0m01+Oj7O1u4uNz+5T90wuXT+Ai9fvIiiKCyurfJkdZlKo47reWiiiCzJJyau+AG4Lqog0pfJkM9lURXl00izng3F8eP7OnNsnyKKIoqi/Na0WL8s/KJrSlVV8n05+nJZVFEE1wU/rLQJvYzYQHRxPY9Ko86T1WUGBgfJZDJYlsWd+5/w8YMHLK6uUO9aTIyNc/H8BS6emQ/NkCXpBT/riIivJpHAi4j4R3JSIXIcHNfFcZ3QUqP3/yVRQpREVFlB1zSGBwd56eJL+AhUqxVu372DJstMjo7Ql+uj1WnTNgxc3//MsHngP1PVECVi8TiDA0WKAwPoukar3aFcqVBvNpFliZgeI50KDXX1fyC8PiLiWbqWRb3RCLeRLQvPdcmk0xTyeXRNo1gYYLBQJBaLI4ihIPN9HxFOUkYQRVzfD1vknTa2bVOtVnmysMDtO3dwfI+RkRFeuniR4cFBdE3DsizarnNiHwRh0VrqWdMosozSMwb/un94iYj4xxIJvIiIz8HzPFzXxXFdLNvGsi26loVt22E6RaNxYkZsPROhpCoKmqqSTYeZo7Is84PvfZ/xgUHeffcdFhcXWc/lWZ2cYmzcQSAcOJcVGXpOG8dWK57nISoKyb4+hicnmT8zx9TYGEEQ8GRpkZ/duMHHDx8Q03UGBga4ePYsb119nYnR0S9ljm3ECyAIODg65J2bH/BwYYFKpYJtO1w6f4FvvvEG86dPMzU2xtGZOYYnJ9kslfB8H9u2kSTp01lFEWRFJhGLIyCwf3jI9uYmW2tPqe7tc2Z+nm++9RaXLl1CQODhwgKlUol6s4Fl29iOc1I91FT1UxPlTIZ4PI6qquiadjKDpxx760UVwIiIX0gk8CIiPoe20eGoVGbv8ICllRUeLS2wur5Oo9Wi025jdTo4di8E/fiHBAExCA1ZFVVFz6SZmpri+suvoMoy/cNDTAQ+B60m//Yv/pyBQj/n5uZIJpOoiooohJUKz3WxDAN8n8mJSc7MneFb168zOTpOp2Nw79FD3r1xg1sf3uTBg4c4vo8cj3Httav0ZbJkUikSiURk9hrxS3Ech06nw+PFBf7sr/6KWx99hGuaqKLEwfY29XqVavMbXDp3nsnRcX7nm99GlVUWlxbZ2NzAFUWIx8PrXpBQFZVkMslhucTb79/gqFRGCAIm5+cojAxhew43bt3k/bt3WF9fp9to4tg2gQC+IJxEoYmEs5KKqqIlEiR6Yu/U1BTn5+aZm51luDjIQKGfTCr9Yg9iRMRvKZHAi4j4HCRRQlVVZEmmUirx4O5d7t7/BAcBQZbRNQ1Jlk+qDic92tAMD79r4jSq7Bzus7CwELZOdR1JUWi2mpRrNdZ3tjBMkyDwcQhQ4zG840UOQVcW1fsAACAASURBVCAWj3Nmbo4/+L3f5ZuvX6eQz7O4usLf/N3f8e7ND6hVK5gCOL6LV6+xtrbCzz54H0SBKxcuMjTwxQTPR3w5Kdeq3Hv4gHdufsDTtVXq9RqSLONKIotbmxx22tQ6BgP5AmdmTvGD3/ld0okkvuuws79H17YRHBdZllHjcRwCVjc3ELa2eLi4gNk16c/lSKfSVJ885tbHd2k0m5QbNbqWhSJIiGJvY+OZ189xNdxrt+nu7xO4LgoBRvUSQ339yGfmUVUVSYyqdxERn0ck8CIiPgdZlkkmEuRzOUYGh5iZmKRcrrBbqWA6YeanKIRij+M5pB6CIISVPcfC7HbZaO0Qj8XI5XIkEwk6pkGn08HsdlldX0eSRCzHRZJkbMtCkWUyfX0UBwaYnZnh9MwMQwMFTMti//CA1bVVtna2w0xQXTvJ/Gx3Oiw/XWNoaIjZqelI4EX8UlrtNgurayyurtHqdBBlCVnXkBSVdtektb3FaqHA/uE+MxMTFAcKzM7MMDszw8bOduiNZxq4toWiaViOy8b2Np7nU63X8D0PWZJwHJd2p0OtVsMwTSRFQlYUFEVD6hlQn3Cc/et5eIQzrzE9xkg+z8zEJCODQ+R7r6PIyiYi4vOJXh0REZ+DpqrIkkRc10klEly4cIE3Hj7gv/z0bR4vPKFRrdK1LAJJQpCk0Dni5+beFEFCUkQCyYcgoFGv02w08AHP9wkcl42dLURBQERADgIkIWB8qMjFCy9x4cJFzp05w+jgEM12m4XVFe49fMDh/i6O0UFLJFA1/SSLte04LK+vMzg4yFtXr31mGzIi4lmCIKDRbLG0usLy+lMM10aJxVA1DUWScawuttHhcH+Xew8fkE6nOTMzy+jwMN9+8y0GCgUePnrIo4cP2drdQQh8jE6b9U47zPL1wsWjWq1Oo1YPRVwQENP13vatQNCLYnv2MQVA4HkInkdM0xgsFjk3f5bf+/Z3uHLhIgP9/fTlcifLFxEREb+YSOBFRHwOJ5YossyQplMcKHJqaprvffNbLK+t8t6tWzxaXGB7b5darYpv2Xi9QXFfEMIbVU9bCYEQbhwGAaIooOg6ajyOFoujKApGq8nuxgZxXeelV6/xxuuv8+qly5w/e5Z0KoWmqCw/XePt927wzgc3OKjVCGQZBBFBEJEkGUUNMztLlQoHh4c0Wy0s20ZRFFzXxTAMLNtGVRT0nplv5P/21SXo5e1atk2328V2HDRVJR6Po8gyjuPQ6rQ4KB1RqlbxfJAVDUmUEQSRQBDxZZmDWo23338fLxDIZ/s4PT1NPpfj0rnzPDp7jtuzZ/jgww+4f+9jjHaHkakpEqk0luNgmQaOYeB0u/h+QCAL+IAfzjEgEM7bib0PIpKiIGoquVwfY8MjnD8zz5tXr3J65hS5TJZYLIYoir22bkRExC8jEngREb8CoiggihKpVJJUKkm+r49T0zOUKhVK1QrlSplmo0Gz1aTbtejaFpbt4HoeohCmW8Q0HV1T0TWddC+rM5VKoSgKDx8+5P/+t39MuVolFo+RyWTIZLJk0xliug6AYXbZ3ttje/8Ay3URZRkk8TgBl+MZ9dCE2e+1vcKvvYMD7j16wNrmJiODQ5ybPc3U+ASxWAxFid4Gvoq4rodpmqxvbfJ4ZZndg7DNevn8RcZHR8LsZD/A/4zJ8PEoXBAmqMgyXddj9/CI3YMDzG735EOPKIpkslky2QyxeAx8n4mhIf7nf/WvuXDhJSzLotls0mg2aDabdK0uXds+Mev2fR9NC62EdFVD1zXSqTTpTIb+fD+FvjyFfPiViMdf5KGMiPhSEr2zR0T8V5CIx0nE44wMhTmyjVaTZiuMMOt2wxuZZdm4roco9gReTEdTdXRdJ51IkE6lSMbjSJJETFF5+PABC0uLmK0OO5tbjAwUKeTz5LJZ4rp+Uo2xnV4smiiEwexAQJiHKwCyJCEr4U2YALrdLnsH+zx89IiP799naHCIWrVGxzSZm5kh3xdleX4VabaaLK2tce/BAx4+ecz+wT5Gq0Wxv8BAfx5JDC1OZEVGliQ8z+u1SMP4FEEIr68gCLBtC9uy8H0P27ExzC61Rp29vT12trYwWh3GRsc4e2aO1668zOWXLuE4Du2OQaPdotXp0O12sewuHcOg0zHxAx9NU4npOrqqhq+LVIp0Kk0mlSYei6FEM3YREf/VRK+eiIhfA1mSSMbjxDSNQl/+JLbM9/1nEgE4SVcQhbAl+vPxS6dOzfA//Q//I7fv3OYnP/oJtz+8RavRYHtnhzNzpzkzexpBhHhMJx6L0Wg2Cfzjyp1AEPh4rouihKH02XQGXdcxLYut3V0+efCAT+5+zJ3bHyHIMu9+8D5XX3uNf/Uv/iUvv3SJZCIR5Xp+RbAdh3anw8cPH/Anf/Hn3ProI5qVCoHrIroe/X15kskU4yMj6LpONp0hlUxi16o4joskiUjC8WxbcFKB1jQNBIHDoyMWV5ZZXF5maXGRxYVFNFXlX/43/y2vvvoq01NTKIoSbtaqKplM+kQ8+oHfi/d7JpZMFHuvjc++LkRRfHEHMSLiK0Ak8CIifg1OxNovGfb2fz75opeL6vWSKo6F4NjoGLFYjMJAkQcPHrC8tMTHH3/MhfPnefPNN8kV+pkaHw83EdstDk2D4PjnPQ/ftonH48xOTjE3M0MmleKoXOKHP3ubn73/Pmub61StLm7b4ahRxzQNWs0Wb73xBt//9rc5NTUdzeR9iTmeuVvf3OCHP/0p737wAXfv3WV3f58gCJAVhSeb61g/fZuObfEH3/9dMqkUczMz7B8cYLZDS5RAlggkOby2PI9MIs6FuTNMjY9xVC6xtLDAe++9x8NHjxgYGODKyy9z8cJF5ufm6O/vx7Rsdvb3T8Sb3Ht9HBsTK3q4HCFG11lExG+USOBFRPyGsW2bRrPJQemIw6Mjjsolas2wndvuGDiug65pJ3Ysc3NzFAoFMn19PHzymK1yif/z3/0Z6VQ6bKnm+ognkijNJmJvezdwPYJulz49xrWLL3Ht0hX6sjk2trd5tLjIwsoKbcNAisUQNS3cbmy3ee/mDWy7y+zUFMVCEV0PUwIikfflIgiC3jKFxdONDX78s7e5dfcOluuhJhJh01UUqbY72Csr9OfzfOv160yOjXHt0hW6rTY7q6tUuwcEqoqggtgzGo4nkuRzfVSrNf7N//P/0mw10RWF8dlTXDh7jlcuX6avr4+1zU3e/ehDKrWex50kk0wmyfbmTQf6+yn2FygWClGcXkTEF0Ak8CIiniNBEGA7DrZt0Wp3qDUalKtVSqUSuwd7ocCrlEOB1w4zaB3XRVdV8pks0+PjGJ126A0mCiSzWQ7rNdY2N5BEEU1VT9q6qqoCod8eQYAky2TSaabHx5kYHUVXVbqmSa1apd6oEwggyRKCoIQ+fbZNtVZlb3eHrZ0dpien6O/rQ+79+26v2mg5NqIokohHbdwXje04dIxOuKCgqCet0CAI6HQ6lKtVtnZ22NvdoVqrEktlUHS9VyX2cVybesOmVq3SNU10VWVidJTp8XEy6TSSLEOv4nx8jQmCQL3ZpFKr8eDJIzzfZ2ZqipFslkAUOCodsX+wz/3Hj3m6tUWlUadr26HASyTIplJk0+kTgTc8OEShUKC/r49cJkMqmUBVNVRFiT5YREQ8RyKBFxHxHPA8D9txME2T/cMDtnd3ebK8xMOFBTa2t6hWKrRbrbBF63l4QYAbBPiEolAkfDHe/fgu//Hv/pZUKk0ilUTTdFqdNoIkYHS73HlwH1EUsRwLSZKwLAvX84jH4+QHCpw6e5aZ2VkGCgUsy6ZWq9Go1zA7bRRdR+m5/wuCEHqRIVBvNPnk8SMyuT7On5kjFoth2zb1ZpO9g322trdRVYX5uTMMDw5Fw+8vAMd1MUyTvYN9FpYWsW2H8dExhgeHyGbSqKrKfumIR4tLfPL4EfVGM/RV7LVIgyDA88GxbZxul0a9Rq1Ww3FcBgoFZmZnOXX2LAedFq12B6NrIksSmqbRMjrcvv8Jvu9jWF10XaPVabOxvcXS6jKdVptWq4lpmFiOgwv49ObrABkBSRBQ5LBNm0yl6MvnmRwb58L8PGdPzzE2MsJQcZBYLIYa+dtFRDwXonfpiIjnQLvT4bB0xM7eHvc/+YQ7t29z/9FjDhs1DMdCOr5p9ZYs6AmsZ29jXhDQcV3ajSalRvNkIQNAIJzTq1nVsN12XGGzbBRZZv7UKa5fvca33rjOqelpAgEqjRqH1TJGz4OMY9PjXjIUogiKQsd1WN/dYWJ7i9GhQSzb5qhcYnFtlZs3P+Sdn/4Ux7Z5+dVXeeWVV3jtystMjo+jq+pJhSfi+RNur9p0bZuNrS0++vgud+7c4e7t26iqyje//W1ef/115mZmKPYXqDcabGxvsb67Q8d1QFHCc3zsxdjz0fF9H6NrclgtU2nUGCoWOTU9zb/+7/6QkeFh3r/1IfcePMCyu/i+H/rlBa3wkhFEOo5Lu2WcPEY/+HRh4tjA+Nn1CDcIcAOfru2B1aXUbLC6sc6jBw+5feMDXjp/jldefZWXLl1idHiYYmGATDrKl42I+HWJBF5ExHMgHosxWCySiMeRJZFEKomSTvLe7Y+o7+wg2GGbU1QUBEVBEiUkKdyqhfBG6QUBvueFCxiui+95oaN/z/4klUwyNDxMLB7Hdhxa9Tr1SgXBcSgkU5w/Ncv5uTNkUml830eRFBRJQQwA3+9FQD3zoAUBRAk3AMM0MQyjtwDiUq3XWV1f58naClulQ6qVMruHeywvL7C7t8vV164yOznF+Oho1Lb9DeG4Ltt7e6xsrHPro1u887O3WVxept7ukC8MsLixzuDICMX+fvK5PhzHwTAMDNPEDQBR+kx8XmiJGIAfIAbCp9eHIJJJpTk/d4by0RHLjx8jOw6SJIUbttksqqJgGgaHe3s02+2eibeAIEmIkoQky+HihCQh8Wmiix+EG7Oe7xE4Dr7jhItFksTgUB+vvPoab7zyKmdOn2ZmappUKkVM01/A0Y6I+OoRCbyIiOeAoigoikJcjyFJMqlMBjUeR1I1FpaWqBwe0my3cIIApxfH5LkeHt5Jjq0gCIg9ewhd01BEEUWWiakqcU2jWCgwOT1DKp3Gsm12d7ZZWV6icnREu9Vkc2Od1cEhZmZmwhtlL2JN0zREKbzZB4FPEHzqnwcgEJo4i1KYiuH7AR3DoFStUKrVsIMAJ4Byrc7K06foiWTonwakEwlSqVQY/N5rq/m+j+u6AP/ghvHXlQDwn4npUnrGwdBr99s2rVaL9Y117n58l1u3P2Ll6VPKtRqBouAEPpValXK1QscMPeUEQUSURERR+sz5JeBkBk8QBCRZQtM1UskEMV3HdV1arRZra2tsbqzTbjXRNZX8wACzp+cYGR1DU1VazSYbT9c4LJUwLAvTtsORA9/H8/0TMemefJjofZoQBGRRQtFllFicdDJFvlhkfm6ON69d46Vz5xgqFMP5z6j1HxHx3IheTRERzxFJksj39ZFOpRgbGuGbV19nY3uLjx8+YPnpUyr1OtV6nY5hYHZNHMdBEAQUWQkNX3UdXdNI9YyQs+k0QwMDjAwOMTY8TKG/QCweIwgCniwt8e7ND7hz72PqtTp/8Vf/gbt37vLd736XSy9dYmRkhGxfjuHRUQYODzBME8u2kfwARVZ61RwPRRLIpFJkUmk0TSMgFHhHpRKlchnX91DjMRQphen53L5zm06zidFuY5hdpiYnGR0aIpfN4vk+jUaD7e1tHNclnU6TSqWIx+PEdb23GPL15dgk2DCNMOWh0UBRFMbHxshmskiiSKPZZGd/n/WNDW7dvsWN92/wZHmJQFaI9+WxPQfbdzkolzk4OqLd6UAAmqaRSaXJpFIokgB+uHwT+AFuryKs6TFyuTwjY+Pkcn3Yjs36xjqffPIJP/7Jj9nc2UGK6Zy/fIVXLl/hrdff4OzcHIIgYBompXKJ7b09dg/22T86ot7bBm91OnQti263i9nt4rhhZJ+iKMT0GIl4nL5slnw2y+npaa5cuMjk2Hh4bcRiUa5sRMRvgEjgRUQ8R0KxJqPIMvFYjP5cHyNDw1y6cAGz28V1XWzHwbLCfFDXdU8yb3VNOxkwl2T5xD9MVRVURUVVj/3DRIIgIJ/N8dLZc5SrVZ4sL3H/0SMWFxb40z/7d/zdj37E9evXyQ8UePXSJVLJJA8eP2bl6Rqe6xJIUti2tW3iosRYscjY8DCZVBJREHvbmiYd08TrbegqqhbOb9k2jzc3KHUM7j55wrfeuM73v/Md4vE4HdPg1p3b/PEf/zGb21sMj40xNjHB3OwsZ+fOcGpqmmwmQ1zXT7aBv8oEvY1Uo9ul3miwuv6UheVFFpdX2N7cZHdrm4mxMf7oj/6Iq6+9RiIWZ2Nnmx++/TY/++B9tnZ2OCofYfgBMUVBEUV8m16VtUO708HufUjIpJKMDQ8zVizyRJTAtsH3CUQf33WRRIGp0VEunjvHmVOzbG1vcu/uHd5//30q1Rr5wSLf+/3f56Xz5zl7eo7+3geVRDyOIAj4gc/05ASXbQfbsbHt0M/R7Y0UHC8adS0L13VD7z1ZDnOPNRW1t/Eb03USsd5G9lf79EdEvFAigRcR8ZtEIBRoapZc5tO/9nsmx77/2aQLodeq/WUcD74fW2UU+vJcvfIyL507z9r6U258cJPFhQX+8i//Ei2mc2p+Hh9IJBL05/N0ul1s18X2PHxBIBaPM1IcZGRggHQi+QsSBD4d3JMkCT0Rx3FddvZ2aDZqjA0OUr10CccJb+pdx2GvVuH+8hLLGxsk7twhm8/TV+jn5ctX+MH3v8/F+bOkkim0r3hFz3YcWu0WDxae8Nc//CF3731MtVymXglbq6ZtoaYTWK4DQYDruFSrVdaervHo8UOanQ6CLKMnwki74LjteZwj22uFiqJIOpFkpFhkZDDcRvURsD0PXVVJplIkdJ1EIokfwPLKCqsLC1hml8LgEG+8+SbfeON1ZqamT7JhVUXB931s20ZRlHCGVBZRZIUEfz8b9tiw+1Pz7uMMZzFKpYiIeAFEAi8i4gXwq970fN+na1l0DINWu02r3aLd6WAYRtgKc8LKiabrxONxbNcjncsRSyUpPaliWl08RUGLx+mYJqIUDsMHjgOArOkk02mKAwMU+vuJx2Phdm9v/k9RZIRuWL0JggBJlFBkhcDzsCwLw/dpt5q0Wy26XRNBDGe8BEXBIcCzTIyuSaXd5OneLu1ul3Qyhet4nJ2bo1goIPZ+31eJ45SSUqXCk6Ulbnx0i3dufsDi0hKBY+M7Lr4AviggKApiT7yZXZN2q0W71cRotXAdB01RUGQFAQHXD0U0goisSCiydHItxeNxCvl+ioUBEukUshaK5yAIegsQMh3TZGdvD8swePr0KTFNZ3x2lnQuh+167OzvYxgGVrcbpk8oYcUtHo+TTCRIJVOkkkkS8Ti6pn3mGj7+cBKJuYiI3w4igRcR8VvEs356Rtek1W6zt3/A9u4Oy0+fsrK5wcHhIa1mA8vqnmwzSlIYA5VIJMjncriOg57P0W00WNrcwPd9EMJqj+t6YeaoKJLO5hgZGWV6apqxkVEkSaJrWSTicQb6+ynk8hjtNrbTJVBUEHpLGQgQgO94dNodDssljiplctlwjkwQBCRVRVNV5F4lyPN9nm5u8H/9yf/HvQf3+cMf/AFXX36F/lyObCbzlZnB8jyPeqNBuVbj1t07/Pu//g/ce/SQltFBVGQkTUUURVzHwbJtBAQkMWyL1+p1DsslOu0OvuNBACKfVnWDIMBzHHRFoZDLM9BfIBGPo/Ra/Il4nOmpKYaHR1nf3cM0TZxuF8NzsSyTWrMGQSjC1EQcPZOh1mnxwd2P+Jsf/5BOp4Pruniee7K9rWk6qXSGwWKRUxMTnJ6eYXx0lJGhIZKJJPGYTkyP/OsiIn7biAReRMRvAbZt0+p0ODw6ZG19nYcLCyysrbC6sUGzXsdotTG7XWwCfEI/O4FQ3B0TBL1s3OM82Z7tiuN5SKKAriiokoznh5u8siiSTSaJ9cyUK/UaiXgCSRIZGxnhrWuvI3ge/6VWZb1WA1UjkOVPvfREEV+AVtdgv1TioFwik0qjSgpi7/EhioiShCwrJ9595VqND+7eYftgnzevXuW7b32LN155lUwqhfzMNumXjePt4UarxQd3bvPjd3/Ge7dusb27S8cwiMVixFXtU+84z+t5ywmokoLv+RyWSxyUSrS7Br4IBJ/62AVBAK4LlsXAQJFvXb3Gm9euMTY0TBBAxzDoGCbtjkEiFiefStOwbRzHQRFFJEnG9jy6jh1eA5KEadvsl8s9I+SwSvvshIDQ2/gO9nZ5vLjAewjEYzHiqRSZXI6ZiQnOzJzi/JkzzExNMThQJJVIfO2XaSIifhuIBF5ExG8BB6Uj3rt1izt37vDw43ts7mzTcm0M1wFBQBLCNpwgisjHs3rPTKgHhB53nu/juC6ubeNZXQAkTWNsYpKX5ueZmZjEMAzK5TJbm1tsrq1x8513qVYrXH3tKq9evsyFuTMU+vLkMhkIAh4tLbJXq2H7Hm63i6ZpoWGzJOAHYJhdjioVypUasxMBiqoiiKGp8zHH7TtZkYmLcVzbZnVpkaQsM5Tvp9DXx8jwMPlsjmQi8YUf/+eBYZpU6jV29/Z4/PgR9+7eZXVpEUnTiPdm6E7O2fEonSAiiGHmq+f7HJXLHJSOaJsmvgCCKIAg4vs+lmXh+x5aMsXY1BRvvnGd1y5fAURKvUWbjx/c5/bt2yw9eoTrOszOnmZ8fJz+/n7i8ThrmxvcX1hgY3MTu9MBQNI15J7NjSxIJ1XaY4KeyXbg+7h+QN3oUO60YH+XteUlbv70Z4yPjnL+8mVefeUV3rx6jYnR0S/24EdERPw9IoEXEfGiCQIMw2BrZ4cny8s8WlqkVC4h6np481UURFUGSQpvu8/4mh0PtgdBAIGPH0CAgKqoJ7YZxaEhzs3P89L588xOTWGYJuVqleXFJTRBYHtvl4XlZUzbptNqUS9XGBocJJ/PE4/HOXXqNNVWm63dbTqGEVafgqDnpxcO+vueH/qsiaCoColkglhMB+E4K5fQo00QkSWZQHQxbYtqrcrmzjbLa2sIgoiual9agdcxDPb2D1heW2NzJ8yCtW2bRCyGLIVvtX7g4/eMfyVJIhbTSSQTKKqCIBCaAnt+uDwhCIRN0rDi5wc+iUSC8ZExTp06TTwep1KpcnR0xN7+Pg8WFrj76AFbm1toisL0xASXX32V02fCjdh4LMbIyAiZdIZ8OsPh/j6NVhPLPzYj9nsVYeFEkB+nrgAIgghSKPg828O1bYxug6OuRavdRk4mGBwawjSN43LyizsZERERkcCLiHjRBIAiK+SyWUZGR2h32iQOMziOg+04J7m1x5Yq4Q+FJaBjCSD3zGR1VSOWSlEoFJgam+Dc3ByvXrrM1Pg4sXgMXdN6rUSPypVX+Ob169x//Igbt26xs7PDT370d9x4+20Gh4YYnRinODTE7NQEsZjGz9732djeBgFc3yPwPQRBJK5p5LMZ+rJZVEUllUgwNzPDweEhh0dHVKs1PCk01xWUcEZLECUEWaXW7vB4ZQUHAcfzyGUzFAuFF3Mifk1qjTqPlha5efcOyyvL1AwDQVXC53o8Q+f62F0L3/Po68tRHBhgbmaGVCKBoqjkMlmymQwxVQ3bo0EoBsMZO52x4RG++cbrjA4OcveTexzu7bOzucn+/j6W6+JJAvNzp/nG1au8dO48UxOT5PN55N4yxoX5s/zet77D+tYmt+/d49HiImvbm5TKZaxOB8u2cHvt2uMPC8CJWAuCAFGAuKoh6Tpqn4wiqwwNDjI+MUF/Po+qqGGc3gs6DxERESGRwIuIeMEIgsDYyAh/+M9/wO9++zt02m0Ojg7Z2d9n/+iQcq1Ktd6g2qjT6fmeBQHEdJ10zxA5lwlNZEeHh5kcH2egv59EPEEiHieRSKDI8smcle04BL5FIplgYnKSvnyely9fYXt3l43NTTa2t9jb22Xxp29jWhbZQgFZ1XBdj1wmS7PdotuLURNlgXQiwcjQEKPDQ8RiOoMDRX73O/+EhKbz4c2b3K83sBwHXxTxemkdgiii6Bpd12Vrfx/b8xksDHDx7Fn84NMZwy8DYQJYQL3Z5OnmBg+fPOagdETXsVE0DUEKfQt9L4zrklyHhCwzPzHB69de5xvXrzM4UETXNUZHhhkbGgrtagIIXA/P9dB1Kdw+dj3e+/AWrm1RL5WIaRpDw8OcOn+OybFxJicmGBsZIZ1KkUqlSCQSSKIY+igqCsl4AqFPYGhwkIvnztPpdOiYBkflMhtbW+zs7VGt16k16jRbLZqdDma3iyCAqijhEk82S18uR3+uj+HiIMODQwwODJBMJEgmk6QSia+8v2FExJeBSOBFRPwWoKoqeVWlL5sl8H2mJiYwul3MXvqEZdvYvWgo3/cJCJAlKYxIkxW03sZqPBYjkUigqepnbrKu62LZNh3DYHtvl/XNLZafrrK+vUWt0URA6OWGumGmqevgyTKmaVDf2yUIQg883/Mxuya2ZRH4PpooIisKsZ53miSK9GWzvPbSZSRBxPN87CBgb2+PSqWMUTfQUylERUZRVHzfp9FqEhBQqpRoNJt0u120Z6LPftvxPQ/Ltmm2WpQrFQ5KJZqtNgCKHJ4H17bptlrEVI3B4iCjIyNcu3qVb33jLa5cvEgiHsey7fA46jFkRQFRxPV9PMvCR8B1PeqtJp7nIQigSRKqLGO4DuVGnYbR4dHqSmiETUAuk2ZqbJy5mVNMTU4yNjJCIhY7EXt92Wx4vQUBk2PjnDs9d5J2Ytk2juvgOA6u5/Xi7ELrHE1VUTUNXVXDxRFdR9O0X8nDMSIi4osjEngREb9FCL0A93g8Tjz+981k/7FYlkWjU7aXqQAAIABJREFU1aJUqVCpVDgql9nc2WZ9c4Pl9TU2dndpNFsIgoCqasRjOqqmhgkFAoiKim8fVw0DREFAU1UUScQyu/hApVplaXmJVDxOcWiI/nyBXCZDId/P2bNnCUSR5eVl1laW2Ts8xBMEfN9HFET8wMexLQxBwDA6dLtdHNcNo6t+/cP5heAHAY7rhjFdpoHZ6eDYFqqihc/RDzNgk/EEI8VBZk+f5vTp08zPnSGfz2N0u+wfHVIql9nf32d5ZZlKrYYPKKqGFtMRJRnL6p5UN1VFQdRjuALU221apolthTFotm0RBAGZdIrNrbB9u7WzzcT4OIV8nnyuj/6+vpNoOkEQQnNjTSP/og9mRETEcyMSeBERX0GsXlj95s4O9x4+5P6jh6xtPGVrZ5tOp4PVtbBdB7c3yC8IQq/C10Ho2ZQEhKkEnuei9BYC+vN5JsbGEAWRcrlErVajUqvytz/8Ie/duEFmoMD4+AQXZucYGRqmP5PlrWvXGB0cpFgs8nBhgfWdbRrtFoosIeAT2Fb4e8wurt2LufL9F3wEf3WCnj2Ka1u4polndAhcBxSFQAhwXI9MMsXU6BgX5+c5OzfH9OQk+D6ra6u8c+M9Hq+usL29Rf3oiE6ng0PA2NgYuVyO/v4CfuCzub1NuVLBNLt4nkvHNDCt7snGa9AzVz5eurFKXapHRzx++AhV10kmk4yPjjI9McVL589z+cIFJkbHSKW++okiERFfRyKBFxHxFeJY2C2urPCTd9/h7r27rK2sUKpVcUURTwitViRBQFBU5OPheQgXN4IAv5cr6vk+siiS0XRmp6f5xvXrXLrwEqempkjEYrQNg2azxc7eLhvbm6yur7O5u8vtO3f56N0biKKInIijxGJoukZM01B1lXhMx3ZsPN8L49o8Hx+XTqdDqVJh//AAI50mHouFaQqygqIoyL3s2hfVBjwWTp7nhVY0bq+d3e3Sareo1moYhoHvuuFz8j0kQSWm9SqjusrW/i5LG0+xuhaOaeB0zLCaGY+R68sxf+ECp6ammBybYHR4hHQ6RbKXQrK6vs4nD+9z4/33WXn6lJZlYfk+kiie5PqKogjPzi8GAd0goGMYVNttdvf3uf3RR7z94x8xMzvLy5ev8E/e+iZnZmcjoRcR8RVDCD4NN4yIiPiSs769xU/ff5/3b97kzq1b7O7vEcgSyGFUldCbazsWSQJhu9TxXFzLCue9XBdZVVD0GLOnTvHdb7zJa5cvMzc7y1BxkEQsjizLJ0HzhmnSMTpUajUOSyU2tjZZWFhk+elT9kpHobj0vE8XJ3qGx2bXxDZNPMtCDmB0bIzpmVlOnz7NxOgog8UB+nI5BvL9DBeL5DLZ3syh/IWLvCAIwq1m26HZalGqVSiVyxyWjtg7OGR7f5/l5SXWVpbZ2trEhXDLNBYjpseQZTm0lun9W7IkUcj1MVwY4PT0NPPzZ5gcn6BYKJDP5UjEE8RjMWRJQpIkXDes2O0fHrC0ssJH9+7x4xvvsbK6itM1cW0HUZaRNA1Z01Ck0DA66BnuHb/NB56H77ngegiux8jQMK9cvcr111/n29evMzU2/oUe14iIiN8cUQUvIuJLTtCzUHEch729fe58fJfbd26ztblOxzSJ5bLIooLn+/Dzrc/jqlQQoMgKKV0nrmpk+3Lk+wtcvHCBb33jTc7NzdGXzRHTdQBsx6HbNTHMLh2jQ6vTodls4roOoiShxGMo8RiSEm7vWpaF7dgEQYCqqMiyFEZraSqiHyAi4AWwsbtNvd1ie3ODQl9fKPD6+xkqDpLv60OPx8PB/t5XovdnTdPCqCzx15vcO46Ks2zrJAPYMEwM08Q0wvm6er1OqVLhqFzisHTEYaXCYa1GvV7H9QMK/QN4Aidbw77vYxgGtmP35udUpFh4bI6PkyhJuK5Ds9kMj29vqSYe09F7MWBZJYOmaqRTaeKJJI7vk0ulqZRL1Ks1DNui63q4rhumZDzjYfeZlWRRxPVtzEYd17YIJBFNU5mbmqaY7w+rpS9AREdERDxfIoEXEfElJwgCjG6XRrPFUalEu1ZF9BwK/f2kXRdPFPEIlwH8nsATBJAEAUUQ0TSdeDrD0NAQE8OjjI+OMjU+xtTEBGPDI2RSKWK6jiRJJy3gvYN9tnd22NrdYX17m/XtbXYPDzAtC6e3kOH0slYDAhRFQRAFPNfFdRwyySTzc6cZKg6iqyry/8/eez/ZkWX5fZ/0z5vyHqYa3gONtjM7u1wNKYrkTzKkgj8opP9LoVCsVtpdhkRR5O5ouZwZjelGO3hTcAWU96+ef+nz6oeb71UBDXTDAw3mJ+INOgaoenlvZt577jnfc46m0Wy3qTYaNKs1ttfW2ZhbIIxC2UlBUVBNEyOTJp3NMjwwyNjQMHvGxxkdHmZwcJD+/n4K+TymaclMT8N4qsdPxIkRvufFBp2H67k0mk0qlQpbsQG3uLLCUlzPr9Nq4bXbhK6HqiiyHZuioFsWmXyO4YMHKRcK5LNZgrgl2Or6GjN3ZdKEoetoui4LPSPY2t6m2Wpx/+EDfvUbBSPORh4fHmHf5CT7JieZGp9gcmKCsZHRXgh1qH+A/NkcRw4cjDOi53m4sMjC0hLzK0usrq7SiXsV+7Hx3g3TqIqKqiikM1lKqTSWrqOGPq3qNhubm2xVaxQL+aTUSULCe0ASok1I+IkjkKU6gjCk0+lQrVap1mpUqlU2tyusb21Rq9dxXBfX8yDuZZvNZKShNDLCnslJBvr6sSwTKzaQLMvC0OUZsJuNe/veXX77h99z+cplVpdX2NhYx/YDXEXIGne9lmrKTr9cIfA9H89zyaTSjI+N8/G5D/lv/vm/4NihQ/LfK9J7FgQhnuvi2jbVWo2l1VWW1lZYXF1lbXOTWrNJs9UidByE4xF5ntS6WSbpYpHh0VEmx8aYGB1jfGSE0dFRhgcHyWazmIYBSO9ju91mfWOD5dVVlldXWVxZZmFlmfXVNdxGE+EHaJqGZplgmWgpi3w2RzGfZ2RwkMnRUSZGRpkYHaVcKmOl05iWha7LkKoQsjftrbt3+L/+9m/5+uJ3LC4v0bY7mKYVd65QeiFbEUnjOxIRmhBYQiFt6AwNDTM6PsaZ02f4s5//CYcPHKSYz8t2cYAfBLjxfXU9F9f12NquML+4yMraGutbm7Q7nThEKzOgU5ZFqVhkeGCAwb5++stlyqUS5XKZTCaDrmmyVuHbeJgTEhJeGYmBl5DwntFNBHA9D9d1sV0Xz/Nkd4Io7HWR0nWdtJUinU6TTacxYgPoSczOzfEff/dbvrhwgauXL7K6vo5QVYSqyhZqqtrr2ND9BEGA58cevUggEJw8cpR//V//t/zi08/YN7VH9rt9wvULIfB8n45t0+l0aNu21Ox5MjS8vb3N1uYmD+fnuTM7y4OFBZbXVnFtm6xhkDVTZIsFMqUSYxMTHJqepr9cBqBSrXJndpblpUU6tRrteoO269D2fcx0irHhEfZP7eVYnO3aP9BPuVwmZaUwTYN0Ss5XJiPDxKZhPjX5o95osLC0xO8vfMn/+m/+miu3bqCgoKjSY2caFnpchLr3iUKZRRyGKFGEEkWMDg9z6sw5Pv/0U/7xL/6M6b17n3qvfN+X82Xb2K4js5Lje66qmix8bMrkD8uyejUHE49dQsL7RRKiTUh4z1AUBV3X0XWd7EvW0nNcl1q9zu17d/nq62+4eOkS62truL6Hmc1ipFOyrIrY1ZxeiF4oNgwCFBT6+/sYHhjis/Mf8+m58xw9eOipBmXXWOrWZusrlR75+zAMqTXqbFer7FlcYHRygomHD5mfn2dlaYlapUKjVmejUcdbWmB2aYG1tZXe76nUatybm6NRq2EJhZSqUSwVmezvZ3RinMnJST7YP82xQ4fYNzVFuViimM+/UOHlQi7PkQMH8TyPe3MPCcKIta0NtqtVgiBExUcRoGjqThaspoEiEFGEbzt4ts3y6hqBuISmakyNjZPNZCgVi6RiT95uDMOgZBiUCoXnvt6EhIT3h8SDl5CQ8D26ZVLmFhb4w1cX+PKrL7lw4QLLq6to6TSqYUjDTlEAEbfGlWVWRBgRBj6WZZHL5xkYGODcyVN8/uFHfHjqNKPDw+RzuRf2GHU9lGEY9jyUzVaL7VqVmbt3uXLjOjdmZlhaXWF9Y5N2p03WsjBiA80PQ9quK0PUQ0NMjI5y/MgRTh8/yZGDBykXSxRyOZm8YZpouo6mqi91vc1Wi9X1db67eoUvvvuWi9euslXZot1s4bqO9H7GoW0UNc6NUOR8RhGR7xPaNuOjo3z66ad89sln/PyTT9k7NdUrk5KQkJCwm8TAS0hIeIQgDNmuVtnc2uKLCxf4q7/+K67evIGrKAhNRTMMVFXphRSlYQe6opDWNPpKZSan9nDy+AlOHT/O0YOHGOjro5DPk81kXrqWnRCCIAzjUGSHRrPB/MICl69d5+qtmzxcWGBpdQWn0yHyPTLpNMNjY2TzeQDazSbrKyt0bBvVMEllMkyMjrFvaopTR49x5uQJ9kxNUcgXyKYzj9Tge5lrFkLQsTuypdn2NjP37nL15i2u3rjO4sOHVKrb2FFIIESc9boT7o4iQej7KGGEJQSnjh3nX/3Lf8Xnn37K4MAAfeUyemLkJSQk7CIx8BISEh7BdV3uPnzAjZkZfvflF/z2D79ncWUFK5NBNwyUXm01ZOsyyyKdTjE+PMLRDw5w5MABDh88xN7JKcqlIoV8Hk19dRqvIAjY2t5mdXOdqzdv8oevv2Lm1gxbyyvUq1XswEOoMDY2zv59ezlz8jTnz33I+OgYAMurK3x78TsuX7vCg4dzrKwso0SQ1k2K5TID42McOXqEn3/8CaeOHWN0cJiBvj5Zy+4VIIQgjCIazSbVeo35xUVm7txh5u5dbs3ek9nItoPrunFrsvjnUAh8H7fTYXJsjD/7+Z/wi88+5/iRIxzct7+XeJGQkJAAiQYvISHhcRQFQ9fJpNOMjY5y7uw59k9/IOvMaVqvaK4U7aukrBSZTJqJ0VGOHjjEgX372TM5+co1YO1Oh1qjQWV7m8WlReYW5vjuyhX+8PXXLC0sooUR+UyGseERBgYHmZ7ez8FDhzh76jRnjp9gZGgYgLWxMVmDL5tlqG+A2VIfW5uyltzyygoL62tsbG0SuS7tRp29U3uZnJikv6+PUqHw0rpGRVHQNY2+Uom+Uon+ch/95T6Gh4cZGhliaXWVTsfGcZ249djOz3Xr9JWLRcZGR8mk0zLTOUmQSEhIeIzEg5eQkPAIQghcz8NxHFng13UJ4tp23U83IUBRVTRNRVU1LNMgbaUeycx82esQQmA7DrVGgzuz97l28yY3Z26xMDfHyuIClc1NwlCQLRQY3zPFsaPHOH7kCIcOHGSov59SqUipWIwNIZnU4QcyO7dWr1Or1dmoVLhz7y43Zma4eesmy/MLtBsNNE2hf3CQsckppvbu5diRo5w8doxD0x9QKhRIp1KvpHXaoxnPDq7nE0UhYRg90l92d4ayHtfMy6TTpFIpLNNMsmATEhIeITHwEhISnonHl4rXbVA0Wk2WV1e5cuM6//C733Ht2jUqmxvUq9soiiwzMj4+welTZzhz+jTnTp1i79QUmXSmV5hZ/YHkCBEXfg7DENtx6Ngd5hYWuHj1KpevXOHK1cssLy/1CjcXy330Dw5x8uRJfvmLX3D62AnGR0dfKmHkWXjT856QkPB+kBh4CQkJ7x5CcPnGdf7nv/o/+MMXf2R1fp5GrUYgBEbK4tCBg5w+eZKPPzzPqRMnejX1Ulbqpb7WcR2q9ToPF+a5ev06X3/3LVeuXePOvbv4jouuKBRKZUb37OFnn3/O//jf/UvOHD/xg4ZkQkJCwtsg0eAlJCS8k/hBQLPVpFav0W42MDSVocFBhsfGOH3yFB+d+5APz5xh3+QUpcL3Cya/CCkrxehQinRKFoDWDQPDNDFMg/WVFaqbm7SbDWr1Gs1mEz8IXsn3JiQkJLxqEg9eQkLCO8ny2hrfXrnMN999w9cXLmBoGqdOn+H48ePsndrDxPg4wwODr0Tv9zhdXdz61iZLy8vMLcxz48YNrl65TBCGfPzpZ5w/f57zp04zNjySeO8SEhLeORIDLyEh4Z0kiAsZt1st6vU6iqKQy+fJZbMYpokeFyB+nYRRRBAE+J5Hq92m1WxKPV6xSDaXw7KspP5cQkLCO0li4CUkJLzz7F6m3pa37F24hoSEhIRnJTHwEhISEhISEhLeM15vfCMhISEhISEhIeGNkxh4CQkJCQkJCQnvGYmBl5CQkJCQkJDwnpEYeAkJCQkJCQkJ7xmJgZeQkJCQkJCQ8J6RGHgJCQkJCQkJCe8ZiYGXkJCQkJCQkPCekRh4CQkJCQkJCQnvGYmBl5CQkJCQkJDwnpEYeAkJCQkJCQkJ7xmJgZeQkJCQkJCQ8J6RGHgJCQkJCQkJCe8ZiYGXkJCQkJCQkPCekRh4CQkJCQkJCQnvGYmBl5CQkJCQkJDwnpEYeAkJCQkJCQkJ7xmJgZeQkJCQkJCQ8J6RGHgJCQkJCQkJCe8ZiYGXkJCQkJCQkPCeob/tC0h4BoQAIRDxnyjK8/28oqAoyvP/3Nvi8fHC98fwUx3LrutXfipjedIYfmrP1PMiBCKKfvx9i/9eUdXnn4vHn/Hn+LlH5v9N3gMhQPD961Z4O9cTX9NzzaMgnj9ez3rSvY7edQHP+utf5L160efoZel+nfqerwU/YRID711GCCLPI3I9gmYTv14jcl0UJd5MnumFVtCyWYxyCb1QQNG1d+ZlFGFI5LpErofwfSLfRwTBzn/7PiKMAIGiaSiajqLrqIaBYuiololiWaimiWqaKJr29sYSBHIsnofwA0QYELkekevG4wh3bpeixOPR5FhMQ16/YaBaJloqhaK/+VdTBAHC8+UYgoDI9+P7s3sMonf9qm6g6Hrvo1omanw/FMNA0X6CAQIhEGFI2Grhrm8StNs7hjg8ulHHG6tqGGjZLHo+j5bNoKVSoD5l7PHPiCAkaDTwqzXCdpud3fIHiN95ISJUy8IoltDzeTnvpvna3+mw08HfqhC0WvJZCaNHD2CagpbLYQ4OomWzr3+diecybLVxNzYJ263eYeTHjHL57sn1Q8/Je6emUy99LSIICBtyrQ5bnV2Hosd/oPt/yJ9TdA3NstBzeYxy+dmupTf+Fn61RtBoIsIgfpSe1RHwPPfn0WdUCIGiqui5HEaxgJ7PoxjG05/9hDdOYuC9w0RBgLuyRmdujto337D5219jLyyhW2lUTUfZtUjsoEAUyc04ihAoFM6cZui/+if0/cnPMMolMEwU/S0ZQ0IQuS5BvYG7tkZ7dhZ7bgF/cxNvq4Jfq+M3GgTNJsL1EGEICGlAmBZqykLPF9BLRayREdKT46SnJknv24s1PIxeLKBa1usfRzzHoW0TNBo4S8vY8ws4i0v421X8ag1vq4JXqRC22wjf39kMVRVV11FMEz2XxegrYwz0Yw0Pk56aJHvgA6yREdRUSn5MA+V1GLCxlypyHIJGA3dlFWdpCWd5BW9zC29rG3d9HW+rQtjd1KMIRVFiY9RCTaXk4l4qkhodJT01SWpqkvSeSczBQdRMBs2yfjKLfuR5BPUm9W+/Y/l/+99pXrse74EKiqbKQ4aqggIiCgl9Fy2bIT06RvHsWUqffELh9Em0fF4avo/dMxFGCN/Dr9bY/v0f2fi7v6dx+QoKcrNUNC2eq8feaUAgiMKAwLVJT00w+Gd/Tumjj8js3Ys1MYZqGK9pUiKE79O6foPlv/hL6hcvI3yPyA/iy1OkVy8S5E8cY/xf//cUz597ve9ibCQL36d59RpL/8tf0Lh0GRH6gCIPsqoWGzk7RqiIQiLfRbVSGIUi5tAIpU8+ov9Pf07u2FE5/y9glEaOS9Bs4m5sUvvyKyq/+S2tm7d69/NxY1fEY4gCj8h30YsFstMHKH/0MYP/9J+QOTD941MQrz+1i5fY+tV/ZPuPXxI2m9KAjd9R5UnvnRC97xfP6vXremgBpbtuhAFKKkXp3Fn6/+TnFM+fxxodRsvlnu13Jrx2EgPvXUYIosAn7HTwtip05ubpPHyIlS2ipzI7pyXx6GYgfI+w3ZaeGCEwBwfwazUi35Mb9LN4C17TeEQUYS8sUvmHX1P/7hLt2Vn8rYo0HrpeI9+XXrBuiCze/FBVFFWNPXjSwFAsC3NggMz+fRQ/PMvAL/+czP59r92bEXlyk27duk3lN7+lef06XmWboF6XG18kYg+YI4272NshdjuDup48M/Z8WRZ6Po/Z30/2wDTZQwfJHDxAanwMa3RUGuevEBEERLZN+85dNv/hNzSuXMXb3MLf3pZeu66H1XHl/el6I+KTO5r6qBfSstAsC2t0hOyRIxTPnSV/8jjZAwdQU2/A6H4VCIEIA8JmE3dxic792d5YVSuFlk6hGCZxnBIhQsJqHXd+hbDVkc9lyiQ1MYHR34+ezz/+BXJz9D38Wg1naYnO/fsoioJqmtLz1f39XRSlZ2QFTge3XUdEoTS8Ox2iwH9sDXi1hLaDt7lJ+959mjdv0b53D9Ww5PoThxQjzydst1FNk+b1GxjlEtlDB17vYavrwerYuCurdB48RAS+PAymM2gpC5RdxrKiIEQkjUBRx1vfxF3bwOwrkTs4TXpqUs6/aT73pQTNJp2Hc3Tu3ad57TrtO3ewFxbQsnnpXd11DXIdDOVzZncIOk30QgHNTOHtnyby3GceP0Ig2h289Q3sBw8J6nWEANUwpFdy91gUeh5qwnAnYhJFP+zIE8jDjWHIg6mmIURE5PmoKYugXif0PHhb+0rCU0kMvHcYRVHQUinMcpn01AT5o0fRrTRKKCCMpDcAIT15u515linDsYaBmrLIHjyAOTiAalovphV6RQTNJs7SMo1Ll6l+eYHmlWu4m5tEti2Nt9hYUA0DJZORXkZVRRHd8Yby4/uErRZBrYYQAm9zA29rk8h10AsFhB9gDPRjlEuv3OvV/X53bZ32rRnq316k9uUF2vfuS2PIjz0IqoqiG6iWgZpO9zwDojv3XQ+r78sQfMdGBAGuquKkU3JMmxu462tkDx0kZ9uwdy9aPvfoov1Cg5Abo1+r0bk3S/3b76h+eYHWrRnCdkfeDwUUVUMxdbSUhWJkQdPkhimEvP7d98OW148Ar7KNX68TuU78cbGGhzH6ymjZ7Evfg9eKqqKaJsZAP9kjh4g8l7B7X6MIEcl/tjtsG7kuYaeJu75J++59tFyOyAvImeb3DbxYr6eaFubgANmDBwhbLWlE+/4u3V/87wW9d1zJpNH7SljaOOnpfaSnJjDLZRnSf43vdGTbuEvLOIvLhK0OCOkd6oWiFQVUVcpJPBdnaQl7YZHU5ARG/2u7LKn90jSMUpHckYOEdpug1ZZzGYUQRbv0b4r8CIEQRu+9C9ttvO0q7toG3tYWlmGgvcD7FXY6uCurtO/P4q6vEbqePLilLFTT6h1Uux4zNdb/6aUCljoqn4UDh0hNTqCm0884fhVF1zH6+8gePIBfrcn3zveJHAdhO/KdVHY8wPK7zZ5Hcccke7pxpvTmbeeAJ8IQhCLD3LqJqumgvb29JeHJJAbeO4yi65iDA+j5HObgAPmjR2jff0Dr1h0692ex5+cJGw1UrRuOkO+fNTpC4ewZsgc/IDU6QnpqEnN0BKOvLN32b/IljCJCxyHs2DSvXWfj3/8tjYuXcNfWCVotVN1AK6dRTWmM6uUyZn9/zxhQdF1qWzyP0LaJOjZ+rSbDoLU6ItaH+Rub1C58jT23QPHDs5Q//5Ti2TMYfWXUTObljSIAIQgbTey5eerfXmTzV39Pa+Y2oWMDoJgmmmXJU65uYJSKWPG8dz0DvX07PkUH7bYM51a28SvSIwPgbmzgbKxRu/QduWPHKX34IaVPPiF//Bjm0OCLDyGKCJstgkaDxqXLrP+7f0/jyjX8al3qOzUNvZCX3rh0GqNcxhoZRi8VUdMpFE3eD2nUdAhbbfxqFb+yTdho9ow+++E8/rb0cKYmJymePU3p44/IHTmMlo29z+8gqmGgFAsUTp3AKBaw5xdw1zdwVtewHzykfW8Wb3MDDKMXAlR0HS2TJmw2aVy8jLO0hLexhaJpaNkcWibdG6+iqmBZGH1lSh+fJzM1if3Lf4Szukb77n0aly7jrq71IosiConCEL1QIL1nD5kPpskdPUT2g/2kp/bIg9tr1mz6tRr1q9doXLtO0GrL7+quIV2PrqKgWgaR69CauY1eLpE7ehhrbPT1HCpj7zeaRubQAcb/p/+B/n/8S9zVVVozt2nN3KYz+5DIc+V6pyo97bKiaAhFHrBCx8FdXaFx7boMKWcyzxdijI0db3ub9v37NK5cxV1ZQbiu1KIqCt1TQfcwpJomWj6HUS6Tmd5H9uABsgcPYo2OYg4NYQ48m1WsqCpqKkXu2FFS42MM/fN/StBoEjQaNG/cZOs//ic6Dx7uCqGHUjPX14c1NIQ1MiT1fpb19CSN+EAStFq4yyu4a2u4m5uEjo0IIxRDl+MTsTGdOPHeKRID711GUaQGy7LQ8nlSkxNkpj8gNTZONZvF29zE29yEWOQuQ5ga1tgoQ//sv6R4/sNY9G70PGRvjChCBAFBq03z2nVqX39L7cLXtO/cwd/elmGUVAotkyFz4AMKp06SP3YEa2REhrYK+R09TPfUGHu8/Mo27uYGzWs32P7dH+jMPugZHK3bt3E3N2jN3Kbywe8onj1D6fyHZA8eeHEBsBBEjktkO9QvX2H1r/8N9e8u4lercdKLIvWBmbTU0hw4QP7YEXLHj5GenEQvFXfGsntjJF70g4DOwzm2f/9HGpcu075zF29zM9bH+fibFdzlVbyNDSL3x7U5TxxCEErtV71B/eJlqn/8gtrX3+DMLxI2m9IYyWTQs1ITmDt2lOKZU+SOHcUWS6yWAAAgAElEQVTo70fNZnZt1Dun+MjzcFfXcBaXaVy5SvWLCziLiyjohO027VsztO/cxb4/S/PaTcqff0rfL35Oet+et+pNfiqxwWb096OXSmSOHCZyXLxKhe3f/g6/0cBdXe15rhTihJN0Rnp2NzZjwb+Nu7aOPTdP+eefk94z1RuvoigoloU1Ooo1PEz26FEi16X+7XdyA11dIwrD3jsUBQFaJkP+1EnKn35M/sQxrJHhHY3V65jD+P4KP8BdXaN+8RLN69eJHDv26OrSDdXL+tRRSRF5Pu37s2i5HMUzZ+QBNZffeX5eJYpUIRvFInouR/bAB715FL6Pu7SM8FwpSxEKaDvvn6KqaJaJiCK89U1qX30DikJqzxTW8PAza/HCTgd3fYP2zG1aN27SunmLyLbl+5RK7Uo0UqRsw/NQ02mMgQEy+/fLg89H8fqk66Bpz34Aj+deL8gEh9T4uHwngwA1nab27UVE9ABFEb33H93AHByg+NE5ih+dJzs9jdFX/uEQv6LgrKzKaMU33+B9VSPY6qCgokZW7M2PIAzffCZvwg+SGHg/BXZnXaZTMmuumyQRie+dmhRV3QkPWNZbyS4NbZugVsNZXaV+6VKsU7shNyXTJHJcrIEByj/7jNInH0m92b596Lms/DdP80gIgTU2SsY7QGp8HKNUov7Nt/HmuI5mmkQdm9atGez5BaKOjZZOy+y+ocEXChFGvo+ztEz7/izVP35B/dIlnMVFtEwGzTQJbQdjtI/C2TPkT50gM72fzL49WGNjaOn0M3lX1HQ61iH6dB4+JGi35AIKCD+MPX7RC5+Qg2YDd3kVe26e2oWvqf7hC1p376LFOkbh+5iDA5Q+PEvhzGky0/tJ7d2DNTLywxmxkcAaHSV3+DDpqUn0fJ76txdjQ74Kqnxu3bU1/EoF4Tmk907Je5FOS0PhXSTWe2oCiCI03ZCe8p5ov/vZMRjQdVSkYeSsrkoto22TmhjHHOiPx7vjuex6oNQ4aUMxze8bQQL5jgOqrsl3Py2Tb14nIoriRKEtWnfu4MzN429uyYNZNoteyMd6QQNF04hinV7QaCBcF299ndadOxiD/aSnpkilXmOiTWyUo6qoioJiWo8eDqNoVyhZiTWkCug6hCFhp0PQbtOZfYC7vII/OfnMCSJBo0n79h1aN27hLK3sJDl0NcKGIaVvQUgYBPFZNQ65q4qUpKTTzx6W/YE5QFXlWJ+IQEQCRNQ7lGjpNEZf+ZkiAiKKSE+O42/uJdiuYBQKKKqGlsuR2jOFXi5J7eJPJJnqPxfe0dU14al0T9Y/dFrapZF6WyeqsGPjrq7RuT+LM7+Au7FO2G5jFIsohoGIIoz+PvInjlE4dRJzeFhq5n7MGFKUXkJCamKCwlmXyHHozD7Aq2zL3x2GRLbUfnXF16nxMbRc7oUMPBFKQXv79h3aM3cJ683YsBZyU89mscbGyB09TOn8OZkQ8Zx6My2dwhobITU+hp7PSW2LIjUuqimTSjD0F/bWhK02ztIS7Znb2HNz+NvbMkvZlL9fNQ2skWGyhw9ROHsaa2REbnI/lhyhKtKATqdJ799HsdmU8766il+toRo66LrMTm00cBaXcBYWSU9NyYw7493OuBMiQvjSUxkFwdM30CiSWlFVHqaE6+G12tjz87Tv3MEc6Ce9Zw96qfikL+mF7H/onf7R9/4VIsLwkfsV1OtEnociQDcMjHIf5vCQPORYFv72NkGtBr7UYYYdG3t+QerwikXE8DDK647KCyF1yWEUa90emydVldGQVEpmoHY6UjvqukSeT1Ct4a6s4W1s9taYHyO0bZzFJeylJYJGXWooNU0awpm01GBGkZSTiJ17J6JIfkT0Su+niLP7oyB48u8V8TMdhBA8+7OkpizM4SEyhw+iGDp+rd4LEaf37CE1OobW1U0nvDMkBt57Sywqfkv41SrNm7eoffsdnfuzhLaLYlpEQYhmWaTGxsgdOUz+5EkyH3wgtU/P6WnUCwVyR48SuR6tO/cI2h38ao3I9XqGkLu1RePqdbR8HnNUipmfmXgRjjod3NVVmjdv0r53FxEGKJZF6Hro6Qy5A9MUP/qQ/IkTpKen0fO559ZEqZaFOThE5sABSufPYxSKPY1LV3tlDg0+fzZq7DHwqzVat+9S+/Y7nKUlIl9mwAk/QM1mSe+ZonDqBNlDB0nt2SPH8Jz3w+zvo/TxeVAVOg8e4Dcavczobtkeb3ub+sVLqJk05c8+/WmUVOgVxH2srlvXG6OraJb0qEW+R+TFyRKBgrO6xsZ/+BVeZZuhf/HPyB053PPGP2qsv9339RHiem7exiatm7do3bpN0O4gFEVqyFIp0vv3kj9xDLOvDz2fp/PgIZ35eaLlZenR63Ro35zBKBSwBgZJT02hGPprD8srcQHjntZ1l7ZM1TTMAZmhLoKQztw8zuISoetCFOJvb9O8fh29VEAv5tGLhad/URw+9ysVWrfv0L57H79aB0VDy2SldnV0BGtslMi2CW/clAeqXpbOa5uAR//83t/HcxRnhj/rdej5PNlDB0nv3Uv02Wc7yRvxgVt7i9GihKeTGHgJr5ZYK+dXa7Tvz0oB//Y2wvNQDYPI9zGsFLmjRymePk1qZAQt82LhCUXX0PQMqfEximdPE9od6t9+h1+r9kqphM0m7Xv3MPrKlD/9GBGGz7zJhLaNX63hrKzQnn2APTePu7kVl8owiFwPo1Cg/Nkn9P38c1JTUxiF/AslEKimidnfR/mTj8gfPSLLlHQ3pm4ZlXRK1pR7DkTg73hTZx/Qun2HoNkCBKppEto2aqzvKpw9S6pbKuIFxqAYBnqxSHpigvyxYwT1BvbSMuH6BiKMUA0Zmm/euBUb50dITYy/ezq8Z0LplZFJjY9R/vwz9GwWe2kJd2VFJgHV64TNJvWLlwhbLfRMhsh2SE2OS53XO5ho0i00HtRq2HNz1C9doXnjJqFto1kWIow974cP0ffZpzLJI05MqH7xpdS9RRGh69J5OIdeKMgQ9cig9AoXCmiZzGseRTdjNs4ejQRCBAjHIZXJUDh7thcaJQjwNA2/XsOv16h+/Q1oKpnp/VKe0KtL+ChBq427tEzz2g1aN29hz80jggBF17GGBskdO0Jmej96oYCzvELr5gyh48Cu6qVvnFgzKeIyLaFt41WqcYLvkxIsiJUIcY3GOOys5/OJp+4nQmLgJbxSRBAX/603COoNwmaTyHZ6uhcFgZaySI2NYI2OvFz1+Bg1kyY1NUFqfpzG1etyQVdUVE2T9bk8qSXya3XCTueZdXGR6xFUq3hr6zLDtd1GBD4iklop1TIw+kpk9u2V+rNS8cU1KLHOUstmX2kpkcjz8CsVqY+q12TdNN97JCnHyGXJTE6QnhiPk1teQkejKKgZaXRbIyN4le2e9041pajd397G29oism0pgH8Xky1+BEWRBl7ouCiGSe7wITL79uBubElv742bNK5cxd+qoGia9OT93f+LX6/T92d/ilEuo72DBl7Y7sgM7sVFWfJjcVEmRcX1GnXLIjU8RGp8nNTkpJRcaCpGqUhqZAR7YACvsk3Y6SDCEHdjg/btO2iZNNlDh8gcmH4DBl4cnlWENKbUuI6g40AYYhSLmAP9siNHGKJYFpHv4W1VcJZW6DyYx1laluHlUumJeseg0aB56xbN6zdwNzaIHLnGqYaBNTRE4aTU4nqVuMZn/A50i2S/MWLvs6LrqHGiXVCv07o1Q9Bs0bp+Ay2TkUbfY0Ze9xmPXBctmyU9Pk567xTp6f0Y5fIbHETCi5IYeAmvFBGFMnPNdRC+11vcdmeGKbqGms2g5l6NZqMrGNa69ebY1RsxjGvNdZy4xltcvuAZDLzuBh62O7KuVFezEi+EiqbJkgfpjKx1p+tvtgTNMyD1iB2iThvhebuKRxMblfJkrmUyaJk0qm68tLGlaKr0NqYfK98Ri94j15UlbzwfghCh0yvz85Mirs8IMkxlDgygF6TxEHZsnPlF3LV1GbJ0HDoP5zDKJVKTk6RGR2RLr1z2sQzrtzsJYaeDt7pK536ccNBsynAcoOo6Rl8f5uAgerEg3zdTGqlaKo01PII5NIRfbxAFAQqC0O7grK6i38+jFwpY46NvdkDKLq9VIBOVFFWNs07H4rWhg7e6hgg2EKEjk8OWlvBWp9BST05oCTsdqU9cXCTqdOSt0zS0lIVRLpIaG8McGsSvbMtSTpF8Tt488TOlKnE5LVV69JdXCBoN7Lksim48sUyKoigyAmDb6OUSwZEjKIaONTYKiYH3kyAx8BJeLSLu4BC3MhJdYfquTUzRdWmQpVJyY39JlG5h4Z73LG7VoyhESOF15HlEti0NtdyzechEGMo2Xu227Jcrokc9dN1NWVEAlR9bwIUf9Hq7yh7D7o7xqzzWX2S3IRmXwZHZwNnnq+kXRr2euJEfyAKwu/t1KnFbJ8t8he3QlJ2sPmXXf6uqTAr1PELbkR/Pk4VX1R+fv3eP7iFCdr4QgDkyjDU5SeQHBM0mURjgrq7KPqGdDp37s2yn/4Bf3SZ3/Bj5Y8ewxsZ6nVre5hT0tJp37lH75jvsh3OyT66qEgUBZiZN/vhR8ieOYw0PPXI400tFcqdP4LeaOGtrsLUlZQyOi7OwhPAD9FKJ9P79iKnghVuCvfjgZAJCFASEvoeaTZOZnsYaHCRyXDoP52F+QRZO396mcfkaWi6PXu5DL5W6ExRrKwP8yjbtO3dp35slbHdQu5n6w0NYExNY42PoxSKR7xO02tK4fCsGfLyGKN3nSyVyHNxOB9Y34kSPpyRa9Ay8NubwIKplYY2NETrP2Gkj4a2TGHgJr4cfSs7qVUR/Y1fzYghBFEVEYUgU/VjG2Y8Pxt3coH37Lq07d+ncn6UzO4tfqcQh49hzFodURBBIUXYkMPv7sUaHKZw5Tf8/+lPS+/Y++xCU7kd5pE3a9y49DtGI131THq8w8lOz6Z6ACKO420WAYuiY/WVK58+Rnpog+8E0a//n/03r1gxCC3G3KvhffkXr1m3Ki8uIIKRomhjFoqwvt7v14Bt8PyLPI2y1sBfmad24SePyZdn2Kghl1mkQYg2PUP7Z55Q++RijVEAEoex6Aui5LKXz50BEtG7N4G9txS3MPJzVNfxaHaNcJjU+gdlXlhm4b7KriSIN2NBx8DY2CNsdzOFhzOEhshubtO/P4q2v41erBPU69StXUTMZckePkhobkwkJQsj+0uvrNC5foTVzB2d5BRGGpEZHyB4+SO7IYQqnTmAOD8lM5GZTzmPvIPfmhrwbIQRKJBCqQNV11LQhk152l5N5hPiA3DXwBgcw+2TrvdfW8zjhlZMYeAmvFkVB1Q0UIw6DanpcOmKn3U3kB4TtTqzTCV76K0Xc31P0SlgoPU2JEjeJV01T1pt6pPjoj6CqqKYhu1N0O4DsXgiF2Kni/gxl3CPbwd3YoPPgIc3bt2jNzMjafVYazZR9XLtdAqIgkN7GKMIaHiaw7Rc6PcvwdaZXs09RNYQS9BZ12RfVl2HoTgeRz7+Csg1i19yInVpkUYSCTBrRMmn0TCb24v70NHhPZid0bwz0o5eKiCDAWVpGiIjOw4eyY4jtENkujWs3pAyg2aL888/j7Np40w26xvabsfLCVrvXJceeX8Bb30T4PoppxL2SVbR0ChFFBM0GkWOj6tv0Cl8jDxKR66HncxjlkixBYjtEgY/oRNjLKzSvXUcv5CmmUm+8bZ2iKChCIDzZ61pRFFmeaHSE/NEjBNvbNK/fwKtUcFdWsB8+xFmYJz0+ilYooOga3sYGzWvXaV69hr9d6fVo1ksl8seOUv78U9J792D2lXHXN6Sn3vflu/A2HnEh/6dbOkXRNFL79pI9fIj8sWOkp/dhlEqxZ//REkAKSm99UC0Lo68v/iTh2Z8KiYGX8EpRVBXFMlHTMm1eNUwi3ZWLR4jc+4OAoNUibLV6+p6XQYQysSO0bVknrNvzUQjZosg0pCYsk0FNP3sqf7cavZ7LSu+KpsUOtniljlsPRa4rvW1Walfp26f8PiuFnsthlMqYg4NSeK1oKKg9A1iB2CASO/X2etq559vwux0qtExGGt2PGVMikuHroN0h6Njovv/SNsWj9yPYqS4S3w81ZcX6LfPNh+reCEovrG4OD5M/fZKg1ZLt6Kp1AEQU4G9t0Z65g57Pk94zFXvDRK/9kwjf3BWHnQ7O/ALO3DzedjU2SuLsybgVYtDu0Jq5TdBs9vRcu70/QlFw19YIGk3Zx1jRUJQd6UTYbGAvLpBamCB37MibG1yXXjecIE6WivV4pSLpfXuwl5ZoP3hAtLmJ4kgtXrc9l6WqaNksfr2BvbiMvbhE2LHl4VHTMcolrNFR0nv2YA4NSd1eXI+U8Ck16d7guIk75oBMSrNGR8kdPUz+5AnMocEd6cb3flZmIfdaw6nKO6czTng6iYGX8EpRdB0tm8UolTD6yhj9JUQUEHVsoiiSbn/Xw11Zw1leJXfceenvjDo2zsISzuISwnVltm4kFzQtnUbNZDAHBzDKRWnoPGOmq2ZZcT2rUdmuK5PpaQmFEIS+j19v4Cyv4KyukVJVtGzmqZm0eiFPdno/WjpFZu8U3qnTMuPQdfE2t7AfPpQlZcIQJQylNg4NxdBRDU0mRDzn4qrGpUuMvj4pjM9meokWIm6HFTkO7voa7vo6eqmIOTj44s4GIQjbNs7iMs7SGmG7Iz24keiVqDH7+7AG+9FS1nto3D2KOThI35/+Ai2bw9uq9HSgkesS1OuEtg2aKpMtdJ2g3UK1TMIohOANzE18qPC3qzSuXad582YvaxZFRdUMFFTpiZyfx15YAB67bV27oPf/7aoZaBioChBFBI0mnfsPMAcGKH18HuFPPbUMyasfpwxTCt+XBY47NsL3QdOwhgZRdU1mxl6/IcOuQYhfq9O8cRMtnycfRpgjw3jr67Tvz9J5OEfY7qBlMrIA8PR+UlOTmCPD6IUCiqr2MlAjN05uemIo9A3Q1cHGB0Ql1t1iaDvt5n5AC63s0usm/LRIDLz/zBF+IDMaOx2pI/I8QHqp1JSFXizuZKc+ywsen/SMconsB9MEtRrtu/dk4+96XdbS8hxaM7dQs2lyx49iDg/LbgrG82VwiiAgdFzsxSVq33xL8+p1orjJd7eelzUyQvbABxROnsDo63uuJAItmyFlmeiZLN6a7Mrhb20RxkkXiqbhN5psf/k1QkDx/Dm0bEbWkXvCxqUXi7LcwPQ+2RsyDOSfUUTrxk3Z4/ai1D5Fvo+IxC6P24stroquo+VzWKNyHvytCvbCIt76Bn7HRtENglabxneXUDUd1TAx+/vlJqXrz1UyJXI9wmYTZ26O1rUbdO7fR4QhmmEQxdmzajpN/vQp8ufOopdLLzSmnxKqZWKafRROnUAEAamJceoXL9O6dVv2DFUCnMUl1v/t/4Oia/K++wEgpQ5RFKHEXunXQeR5sl7h3BzNa9fp3J8FVUVLpXYyUBVkWy/izf5pjekFsbEgnxkRSd2ZYpogkAkXjRatmbu0bt3ueb3eZLFrIQRh3N93x4NXQi8U8OsN8ieOEzSaOEvL0sC7OYMII7yNDcyhIdq372DPzuJvVxG+T2pogtInn8jQ7L698t2P50pEgsj1iTwfBQVVVWXP1rfmzJOCXBEftgjCXhu8H1x343IpwvOIHIewY8tOGRA/KxZqKt2LciTtyt4dEgPvveXZwnl+VepOmtdu0Ln/AHtpCSKBYupkDx5g8Jf/BbljR9GLxefqomCU+8gfPxa3r9Hx1jdlyYisjggj3NU12jO3qX/7HappkJqYkJl5zyHgDeoN2nfvyd6nt+/ira/LsJ+uxxobFWt4mOLZ0xTPncF4XoNCVVEMU7YiGxkld+QIfmWb5s1bCK+Flk4jXJfm1WsgIrRUCiOfxxoZlm2+Hqv31etg8IRixe7qGkoq3cs0fXQTVV78BB1nE5v9/eSPH5OlXb67RFCrEzkOWj6PCAI6Dx7KJuj90tOXGh/D6O9Hzz/75utvb9O4eJnqF1/KfrqNujTatdj7qKkYA/0Uz52lePYMRuknouXpFYJ9imHT/Te7/9yNomAM9FP+/FPZ/irujuJtb8cbZkfq3bqbo6LI36NqoMT61ddE2GrRuT9L6/Yd3LUNglZHhtAtGc5XFAVzeJDssWNYI0PyGY492E8ap5wDgbexRevWDO7aWu+wFQU+kefgVbZo3rwlawFmc6/UwJPa2x9Y+XYbqF2DNO4JbPb3kz92VBapbjRl/cjKFs2bAfbcHKplETRb+NVq3ENZkf2bz52mePoUxsDAY+WXdj0vyq7P66SXoPMja38k4Glta59A2G7L2oh37tG8eh1vcwt0Kf/ITO8nd+Qwmen9GOUn1w1MeDskBt5Pjl0lQZ7ajkaJN9Uf97qFtoO7ukZr5jbNa9dp35VeF9WyiDoOhVMnyUzvRzzHRg+gZdJYIyOgqDjLq1ij93A3N2UCQdy6yq/WaF6/iZpOEzkuoMgm5nGniCcSZ8JFtqwrVr90meb16/jV6k77HE2GibV8Hmt8jPS+vbIX7Ys09FZAMaRoPnvwAN7GJvb8IkG11st2DV0XZ2mZ1q1bqIYuv29qAmtkJNa+/bDR2q0sL0JftkETsZKv67kT7OjxXhAtlyU1OQ4I/Mo2nYdz8n6ArI/n+3gbG7Ru30UxTfx6jcz+/b15e2rdwCiSert2m869+9S/u0Trxi2CuGxI5PsQP09GPi9rwE1OyLZrz9mV462gqDJUbpoomv5k78Su9+1phWxV00TtM0nvmSR35DBBs0VndhZ7YZHAtmMPmdKzI99UNCzsdLAXFnDmF6ShGYfuozCSiT+ZDKnJSYrnzpDZv7e3pjy5tIa8aEUI7IVFGYr2PIJajcBxpI5UUYgcB3thAXth8tVq8bp1HXtShidMYtfDqKoyPLkLLZMhvXcP6cVlmjdn8LcqCC+QRdsbTYBeP2DVMFBSKYz+PqyxMcxhqbt7VP6h9EKgT7oGaUC/2gSjrmZS1Z/St1qRJVMUXQP92fWvkevhbW7Tuf+A2tffYC8sys41hQKRbctC12MjiEL+lY0l4eVJDLx3ne5C2q1X9HjD8yesYyIICFtt/O3qLkPwyQuyt76Bt1khqMuOEyIMZN24+MQtohcrnCGzrmRrm8Lpk4gwQMukaV6Xpz/FMAhbLWoXvsJdW8O+P0v28CFSE+NS89ZXftRAFUI2yfY8vPUNnOUVmtdvUPv6W+z5BSLXBd0g8nzMgSLp6X1kDx6gcPoU2SOHMYeGnrs/bG8suk5qchyzv4xmmfhbW7KBeL1O5Lmomo6/VaHyuz9Qv3yVzAf74yy1o6T37sEol+I6VIpsVKR0x0OvV6s9N49fqcaGLiiaKo0+IWvXdetVvaiER8tmSU1OxrW5AqIwRDVN2rOzBHXpafOrNWpffU3r1gzZQwfJnzxG/vhRrPFxGbJVdonqu4J118VZXsGem6Nx5Rr1by/irKwCsqp/N+RvjY+TP3qE8uefkZqclJvhu6zp6Y4vDHsZ2U+cegViJXpPyC58X/7V7lqAMdbIMKXPPiU1OUHtq2+o/P6PtDu2/N1RFD/zr394RFFs1G/SvHKd1s0ZIttG0TU5Zt9HGxokvWcPhTOnKZ4/R2b/vmczCASYw8NEQYhQoH3rdq+jiarLZ6Jz9z5GqUzh7Bms8bGdXtQv8kwIWetSXvdO14hH1rxdHjQFgdJNtvB8RGxo6fk8uSOH8Wt1Ghcv4m+sy8c9DHvXJXyfyPel1GJykuzBA1ijI2h5adj09K3x/Aoheme0Xkxl1xBl6NOPJR/S8OwVaX+e8ceJWN2xC/EE91xsbEYiIgp8gnYLb2url5j2dOQ+4SwuYi8uYK8sYq8soqgaer5IamKcsFvr7yUOoQmvnsTAe5fZ5a3y63W8SgV7YYHWzRla9+4TuY7MVNW0XnkNFAVnYZGVv/hLNv7D3+1aNB978RQFRVEJW23c1VW8zU1ZsTyf74nvdxaaF1h04xOqXixQOHeG3NHDFM6eZvNvf0X920vYi4uEjSbC82jfvEX7zt04IaCAUSqhl0rScxR7v0Qgs1Uj28Fv1PGrss+n8KXXSwiBls+THh+neO4spc8+oXDmFEa5LLtMPE9x4CeMRctk0NJpih+dxxwcov7dRTZ/9fe0Zm7HGaMOkWPHmXfL1L64ID2IQ7Lqv2pZskBxPC/dzSi0bcJmE79ex9+uEjpObBgooMmPXsjIXp5DgzIx4QXohoeN/n7Kn31C/uQJSp98xPq//Xc0Ll/Br2xLLabtENTquKurVL+8gF4sYA3ujEFRdRBCln9w7F5bOr9ek4t8/OwIIaTGamyU1NQkhdOnKX30IbnDh9Ey6WdOdHnjxB6soN7AWV3FXVvD365J7+bMbRme03Tp2dNkIk7oODRv3ESIiNTYGNboiEwwKpUwSkW0zE5vXz2fR8tmsUaG0fI5jIF+ahe+on7xMt7mVs/ju2P8dkPDr2ZsUayj8ra2sBcWaVy8TP3iRez5BQRClkKJy2loKQujmJdh+vh6tNQPeNeJDyyui6JrGIW8rO+XTaOaBkKV2cVEsp5c69YMW//p1/jVbVKjo5hDQ+j5HGom82zFvGOjO6g3cJaW8TY28KtVWjO3sRcWCV1PTpuqSi2qEAjXo33/AYpp4mxtYa8sY/b3YZZK6MUSRl8f2YMfkD9zitB25DrVbqMaJui6PPD6AUapSPlnn9H3s8+wRkaIXBdvexu/VpMa3Y5N++49nIUFwmaLbhszdGmIBbUazvwCmmkiItnSzSiX404h/c/Wzi027IJ6A39rC2+rEnsbG/JZ3aqgdJMoDANN11FUFX9jk9qXX9O+cx+jWES1zCd2sgB6Eo+w08Hd3MTb3CSyPfRMDhEKmS3djSi95ULdCd8nMfDeYUQQ4G1s4swvULt0ifIVNf0AACAASURBVK0vv8Cem0PxQvCkp0A1zVgIveNZ8SrbuNtVQJ5Wn4qyE1LpbsrdJISnVjd/XhSl10aseOY0qZFRih9+yObf/wONK1dxl5fxq9WecapWKqiGKduJxWn5wM7pNAzjThm+3IgUpdfQvHD6JAO//HMKp05g9Mk6ZK+mM8OusRQKZI8exujvI713ivp3F6n89ne07t4j6nSIHFdKezQVpV7H29jYSVZQ1d76J71DkfT4BHGyRSjrYiiGgdlXJj26l9TEOPnjxymeOUXu6FGMgf6XG4KqouVyaNkspY/PYw0N0bh0mcqvf0Pz+k38Wo2g0UKICEVV8CsV3OWVHQ9Lt55hGMZh5bDn1RBxGyijv4/s9DSFs6cpnDpB7ugRzKFBjFLpjdc+e14i3ydstqhfvszq3/wNjWvXEV4Ql9aQJS8UQ5cbmSrrqEWdDrWvvqF26TKKITNi80ePUjpzluLZs6T379vRf8aboZbPkTtyhNT4OEZ/P97mNn61hgh8ECFC1WQdsjB+TkT05A34OcfmLC7SvjdL/eJFal9/jT0/T2S7gECNtWgi9nS7a+tSkrC8glupUDr/IflTJ7CGh5/6HUGjSfv+rHwvfv1b2nfvIVxXvtum1PV1vVjOyjKrf/03bP/+D2T27iUzvZ/8iWPkT54kvWfqhwfTfQbDkPadO6z85V/RuHRFHlJcR3aNideHridVxC3jWjdv0rp7B2GoqNkMuQMHKZ44SensWbJHDmH091M8c4ao3cHf2sLfqsgDbyQNW9U0pLb33Bnyp0+hF/LYD+fZ/uJL6cFeX8WvVggaDYQbIHxZgqRrtIowlDq/Wo32/EP4/36DOTBAdt80xXNnGfjln0tv6Y8gwpDQdWnevEnlH35N7cJXBLWGXB89j8jzejrKR+5Rs0nQbNCZm9/5ux96th75N3EpJ8MAEctJorjDR/Q2E0gSnkRi4L3DCCGIHAe/VsNZWKR59Sqdh3OY2QJ6KoNqyk23Z4zFnQhEEMaerUd7p37/C+j1It1d06q3qYSR7LUpXs2Lq+VyZA7kUAydoN1GS6do37mHt75O5MkM3q6xE3lubETExWPjRVrRVFTLQstl0SxZT80cGSZ76CCFs2conv/wxzeHl0BqfCzMkSG0TAo1nSLyffRCAa9SkckLcc/XKK6RF3V7UT7hPnSbuGtZo9cT1igWSY2PkZqaJL13D9kDH8jw9djoq9PrKAp6sUju2BFZlsNxUDMZ3NU13I3NeJP0e23OQtveOeXH9dHo6n2yWTTdQEunZObxxDj5E8cpnDlN9tBBMvv2oVov4UF9k0SyELdfqdK6dZvGpStxLTcNNS2LM6tm7PGIjQfh+wSNJoFjE7o2iqmjoGANDZM58AGpwP/e13T7oWr5HNkPpskeOoBf2cJdXye07d47GcUZn/I93JUc8EJjiwjqTdylZdozt2lcu4q3sYFR7EdLZ3akH4oscBvGtSqjwCc1Nkp6coLsoQM//BWui7e5iT03T+vWDPb8vNTDptOyxNAueUnY6eCsrMhSQa02keehl8tkpqefbTxdb2utQfvOPRpXriICH8UwZOuwdPrREHms/41abQKnQ+B0EJqC8ALMQpHs3n0Iz4+1eFM4S0touSxCRESeC6Esu6Tlc5hDg5hDQxjlMoqmEnbauMsrMst2aQF3axPheRj5Eloq3fOkde9D5HlEYUBod/A7bRk5cXys4WHCjv3M4ycICKo1Og/+//bu7Tlu684T+BdooLvR9ysv4v0mipJISZTlyJZlbSpZlyf7kIpTSWozbzuVh/xDycPUJuXKpLZSeUiVJ05mnN04jldSLNuyTUlskeJdpMjmrZvsC7obDWAf0ICaIilSl8wmmO+nimWW1UQDaJD44pzzO2ceu19MWKESVpj0hMPw+J6YisgZY6k764Qf+eDQfJ9oPHTbD6eGx+MM67G6Z5nw/pYw4P0Ns6Yq8UOKReE70Y7QwAAknx9SIGx1GRx4s29MOnrcG4HQCE92paD9ZFzXrbElwcBLn4zW196Olv/2D4h/7RKqK4+gPlxGdS2L6vo66vl8oxWpYAWjRjm+IEkQZWtMnxSLwhuPw9dh3XT8nZ3wdXTAm0o6Y2H+2kSvF0I8jvD581AGBlBby6K6umq1emxtQdvOobq+Dm1jC/VioyvZXnC8UbknSB7rhhEJQ04k4Eun4e+wJkv1tbZCClstbdbNyv/SR94Logh4vVD6+9CeTiH5X66iml1HNbuO2sYmahubqK6uobaxYRVNNFrpIAgQZckK2oofUiwGbzIJf0cHlJ4u+DpOwNfaCjkRh+j3Q/T+HS1tJFrHJsdjCA4MwVBrVqurPeGvJDWWrWr6maYxV3rN6p4M9PZZXdvBoNWlexABVhA80Yb0P/xXeFMJ5G78BdXVNavFVPI0ujxr8Hd0QLKHGzx3q7RgzamoKJCTSQT6+iHH4vAEwxBlX1PFpwCYjeXXKmV4UynI6VRj+p+n3zIEUbTmj4xFofR0QvR6IPoViL7G9dtUGG5qGvRSCZ5gAL72dnhTSUjh4DNU0lvDRzyhIAL9fVa4aRRa7V2Ga+9PWfPh1aBXKxAEINjbB3+L1WUuSBIke3s7VlenaRowqlaRiJxKwdfejsi5MXiTCatlXhDgCQTga29FcGgAnlAAvu1W6JWKFe4OG/trmtBrVRiVCqRwCMH+fqsw67hVqI2HXikchtLdjdDZM9bk8WhMUG0Hsn33AXss7zM8LNj3icZ+Ww/hNQheL+SWNKRICIJX5hQpf2ME86X1xdFL15iY02hMzmnfZAXxrzT7f3NLoNkolLAnB/5rzG/UuCkaNQ2GVoNR0x7PDafXH6/iADhduBBFZ8yh6PM2VsuwWr/+f/5xcQZLN7qOzbrmzGm2pyW1qTXBqaZrHI8gy9Z8gI0ly/6jl/AynSKBunPdWcewtzXYdLq9hEZFnrT/8/h7nQ/LbuFQVWi5PPRKZc9n8NTCEPPxSiSi3w8xEIAnoFif51NCmT0hrl62xmMaNe1xi49pwjSs8VueUBBiQLHGwT1PwZBpQq9UnalZdFW1upwP+XtiD9wXJMl6EAkErAe+I8bg6apqHUupZBWcHNBN6Jyvxlx0gmy3YFvvc5wKa/vWpZdV1HM5GKr6OD8e9XtjFyYAzoOKaC+dJ0lW74mqQsvlrCX8dGsIi/NgEwjAEw473a5GtQq9WLIqke0hF6aJI9efdc6BB4LPZy3hFw4fr8W7cQy6qqJeKMAolZuWazzGOXhe9t+BxvGJgYC12k9jSqy/y997l2LAIyIieprm2+TfcvU3URN20RIRET0NQx39HWJbKhEREZHLMOARERERuQwDHhEREZHLMOARERERuQwDHhEREZHLMOARERERuQwDHhEREZHLMOARERERuQwnOiZ6CXRdR71eR61Wg6qqKJfLqFarME0TkiTB5/MhEAhAURT4/X4I9nJfTUzTRFlVsb21BV3XEVAU+BUFXq8XsizD8xxrkGqahnK5DFVVUa1WUavVYDSWMxJFEbIsQ1EUhMPhQ/fraUzTRK1WQ6VSgaqqUFUVmqY9XgZKFOHxeODz+eD3+6EoCnw+37GPxTRNlMtlbG1tQdM0+Hw+Z1t+vx/yMdYt1XUdNU2DqqooFgrweDxIJBJQFOWp75nL5WCaJoLBIAKBACRJgsfjea4loDRNQ6lUQqVSQb1eh2EYe857M/s6ss9rpVKBJElIJpP79tk0TRimiXq9jlKphMLuLlRVPfDaEgQBkiTB6/UiEAggHA7D6927JFa9Xke1WkW1WnXeu1arOdszTROyLDvn3/48DvscDMOAYRjOdVgsFlGpVA4854IgwOv1Or8rwWDwWJ8vER2MAY/oBdg3r2w2i/n5eSwsLGBxcRELCwtYX1+HaZoIBAJoSadx9uxZXBgfx8jp0wgGAhA9HoiNG6dhGNDqdXzx5Zf4n//8z8jncrj4yis4d+4cenp60NnZiUQicez9qlQq2NnZweLiIr766is8ePAAW1tbyOfz0DQNhmFAlmWEQiH09vbi4sWLOHnyJOLxOGKxGGRZhiiKEA9ZV7JSqaBQKCCfz2NxcRHz8/OYmZnBo0ePsLu7C13XrXUqRRF+vx8nTpxAT08Pzpw5g7GxMbS2th55DJqmoayquHXrFn7+s59heXkZvb296O/vx9jYGC5cuIC+vr4jt1MoFjG/tITbt2/j/3zwARS/H9/73vcwPj6OYDAIRVGc49QNA1pdw5cTX+HnP/s5tFoN165dw+XLl5FOpw8MZMexurqK69evY2JiAqurq1BVFZcuXcJbb72FsbGxPa/d2trC3NwcZmdncf/+fWQyGbS0tOBHP/oRxsfH956jeh1lVcXm1hb+78cf49//7d9wP5NBOByGoiiQZRmCIMAwDIiiiGQyiba2NoyOjuLq1avo7u7es72NjQ1MTU1hamoKmUwG9+7dw/b2NpLJJPx+P3RdRzgcRmtrK9rb2zEwMIDh4WF0d3cfGMjs6zCbzeLmzZv48MMPMTMzg2g0Cr/f7wR9+0HIvk7sz7e9vf2ZzzURWRjwiF5AoVDA0tISbt68iY8++gj37t3D4OAgXrt8Ge985zvwSBLy+TxmZmZQ1TSo1QrKFRWSLMEHL8TGovEmTOimgUJhF7Nzc9ja3ERrWxu6urqQSqWgadqx9sdugbl79y5+85vf4O7du6jVamjvOIEzZ89icGgIwUAAskdCYXfXCWY//elPEY1GcfHiRZw7dw7d3d1ob29HPB7ft31VVfHVV1/hj3/8I27duoVyuQxBENDV3Y0r195ET08PZI8HpUIRm5ub2NzcRKFQwMLCAgzDQE9Pz7EC3sbGBj799FPcuHEDc3NzWFpawtzcHD7//HNsb28jGo2itbX1yBZBwzBQr1aR29xEJpPB2uoq5ufnceXKFVy+fBnnz59HNBqFz+eD6BFhmCZ2CwXMzDxARa1geHgYxWIR0WgUuq4f63N4kqqqWFlZwf3797G4uIhSqYR0Oo1CobDvtbVaDTs7O1hdXcXs7Czu3LmDrq6uA19rGAY0XYdarWJzawsLCwsoFot44403cP78ebS3t0NRFKvVUNfh8/vhDwSQTCQOfGAol8vIZrOYm5vDzMwM5ubm0NLSgu9///s4ffo0dnZ2MDk5idu3b+PmzZtIJpPo7+/Hq6++imvXrqG3t3fP9nRdR7VaRaFQcLZbqVTwrW99C+fPn0ckEoEkSajX6zBNE0rQallMJJOIxqLPda6JyMKAR/QCCoUCpqamMDExgZmZGWSzWZw5cwYjIyO4evUqBFFEtVrFq6++CkEUEYlEEAqFrO6+Pa1jAkTY3aNW96bdAiaK4rG7BXd3dzE7O4sbN27gz3/+M0RRxA9/+EO89tpriCeTiEajVnevIKBWq2F8fBx37tzB+++/j6mpKXzwwQe4fv06rly5grfffntfwMvn88hkMvj4449x48YN3L9/H+fOncNbb72F119/Ha3t7QiHw/AIghMGK5WK0+3n9/uP3SqTzWbxhz/8ATMzMxgdHcXQ0BDm5uaQy+WQzWaRyWQQj8fR19e3bz+bCU1fpmFgc3MTn332GYrFIvL5PDY2NnDy5EkMDAwglU5DgACYgKFb3Ytm80Lzz0kQBHg8HsiyDK/Xi3q97rSSPsnj8cDv9yMYDMLn8zktcAfthwDrj7gMwCMIEAAkEglcunQJV69eRTweh8/ng2maTjeovS+SdPCff7tbVZIkhEIhtLW1YXh4GBcvXoSmaUilUpAkCZIkYWNjA3fu3IGu6zh79uy+gAfAeW97/xOJBF599VW8+eabTguj/W+CIED0eOBpfBHR82PAI3oB9rgmSZIgiiJ0Xcf29jamHzxALB5HKBRCNBpFOp1GIBA4tNvzcQixv4NzM36WMV/lchkLCwuYn59HoVBAV1cXRkdHceHChX2vVRQF0WgUNU3D0sOHyO/sYHpqCgsLC2hvb0cun0dd1+FpCpjFYhEzMzOYnZ1FoVCAoijo6urC+fPnD3yPSCRy7H236bqOWrWKrc1N5zhOnjzpjPmanZ1FvV7H3Nwc4vE4UqnUUwOeaZpOy5vf70csFnO6oZeWlqxWz1oNgWAQoUgYoiDuOeaXQRRF5zqxA5uqqsjn81hfX3fGRQqCgK2tLeRyORQKBWcc52Ehv/kasf9V13VrvGGxCFEU4fP5nNc2j1182vE1h0B7DKg93q65K9Y+t7quHzsIG4aBarXq7J99bPa+KrIM+ZDwSUTHx98iohcQi8UwNjYGVVVRKpVQLpexuLiId999F++99x5OnDiBoaEhXLhwAUNDQ0gkEk64OCq8PdnycRx2AHv48CGCwSBaWlqOHDMWjcUweuE8tnd3sL65gbVsFsVyGYVSCeVKBYrP59xwd3d3kclk8PDhQ/h8PvT396OjowPBYNDZnt0tVy6XUS6XUalUnNajQCDgFDgcduzFYhGLi4uYfvAAu7u7CIfDGB0dRUdHB1LJJE60t2NpaQl37txBoVDA0NAQOjo6Dg1B9v4YhoFIJILR0VFcu3YNgiBgYmICt2/fRqFUQrVeh1qrob+vF6JHhCA+W7h+GkEQIMsyTNOEqqrY2NjA9PQ0bty4gY2NDdRqNQCALMvI5XJYXFzEw4cPsby8DFVVD23tO+ga2djYwEcffYS1tTUkEgkEAgEIggCfz4euri709/ejra3t0IIR+zza4W13dxcLCwuIxWIAgHv37mFiYgL37t1DNBrF8PAwXnnllUPHiD75oLK+vo4PP/wQKysr8Hq9ToAMBALO/rW0tLzwOSf6z44Bj+gFBAIBdHd3IxKJ4Ny5c1hdXcXc3BwePHiAubk5rKysIJPJ4Fe/+hVGRkbwzW9+E1euXEFraysikcihlZwA9rRuHDdo1Ot1p2rWNM1jVX16PCL8fh98Pi9EQbS6A3UdpmEAjZYlm2maTnehXYVrt0rZCoUCFhcWMDExgYk7dzA1NeW0Rp0dHcX/+Kd/wsXxcXiaikyaPXr0CO+99x5u374NURTR2toKTdMgyzJOjYwg3dKCjz/+GJOZDDKZDGZnZ9HR0XFkZax9buLxuPMZ9PX14c6dO1hYXMT/+pd/wf/+4AN857vvNCpIq07l7It6MuTU63Wsrq7i9u3bWF5edlrwPB4PSqUStre3nVY8Xdfh9XoPLXh5UqlUwsLCAlRVdaq2ZVlGJBKBrutIpdNobWuDCeDJs+/xeOD1ep0ArqoqJiYm8JOf/AR+vx9bW1uQJAnpdBrj4+MYGxvDxYsXcerUqT0h/2l2d3cxOTmJra0tpztaURTEYjEYhoF0Os2AR/QSMOARvQC7izaRSCASiaCnpwejo6MoFArY3d1FuVzGp59+infffReTk5NIJpPw+XwYHh7G4ODgoYHEnl7koAD1NIqioKOjA/Pz81hcXMTa2tqB01I0U0tlLM8vYnlhCbv5PMxGhW1QURBQlD0BJxAIoLOz06kUXllZQVdXF4rF4t7XdHVB8HgQTyTQ1dWF27dv45NPPsGD6Wnk8zloeh2iKADC/vCUy+Xw+eef4/r16xAEAWtra1hbW3O6YTVNw8rKChYXFxGPx/Hll18iFovhwoULTw3MhmE408T4/X4MDg6ipaUFY2Nj+NOf/oTf/e53mPjiS6w8fAjAqmYdHh6Gz+dzuuCft0XPfm+7qrq1tRXj4+P4xje+gbNnzzqvs7tol5eXsbCwgMnJSUxMTEBV1WMXeHR0dOCdd97B+Pi4U0lrh/FIJIJEIgHpkOBvV4XbLZ6SJKGvrw/f/e53Ua/X8ctf/hIbGxtob29HW1sbTpw4gba2tiO7yJtbGbu6uvCDH/wA4+PjTtesfa1Ho9FnqhYnosMx4BG9gFqthmKx6IyVAqyQ1dnZCa/XC9M0Ua1WEY1GUS6XnZunPVXJYewbYvPgetM0oTcPRre+2dMKEwqFMDAwgOXlZXzxxRfOtBdtbW1IJBKIRqNOYKvX66hUKlhdXUVmchIrDx/C5/Wit7cXHR0dCIfD+wbih0IhDA4OYmVlBY8ePUIul8PS0hIymQxaW1vR1taGYCiEWCIBSZYRDocRi8WQy+Vw9+5d1HUddad4Ye/x2nPpra2tYX19HZVKxankrVar2NragiiKzjmxC0ZmZmbQ0tKCgYEBtLW1HfuzUxQFiqIgFAohl8shn8ujVq0hk5lEPp+HoihO1/KztqQ+ye7utFs+/X4/WltbMTIygkuXLu157draGgKBAAzDwOrqqlNl+rTrpVkwGERvby/GxsYQDoedMXjPsp/1eh0AIEkSWlpanLC4sLCA6elpyLKMjY0NZLNZbG5uIpVKQVGUQws3moVCIQwNDeHixYvPPa8gER2NAY/oBTSPSSsUCqjVamhpaUFnZyfi8Th0XXfGGnV1dWFwcBCnTp1CZ2fnni4ts+kLsKbVyGazmJ2ddcZtxRMJVDUNkiQhHAwiHA5bkyH7/U73XSKRwNe+9jWIooiFhQXMzMzgt7/9Lebn5zE2NuYUKwBWV97GxgYmJydx8y9/gaZpuDA+juHhYQwPD6Ozs3Pf8aZSKbzxxhtOJWi9Xsfm5iZ++6//ioX5eVx905omxQRQrVRQLJWwubmJarUKn8+HcCgEn1eCKApovq9rmoalpSVMT0/jk08+gaqqOHXqFL797W/j5MmTMAzDCVpGoxJ2dW0NDx48QCaTgWEYOHfuHDo7Ow9s9bTDsh0Om8esKYpizQM4dBKjY6P4xS9+gVu3bmFnZwfFYtFpeXsZdF1HpVJBuVxGrVY7sFXO7kre2dlBqVQ69hQ59j7ald3RaBTJZBKBQADA3oKgQCCASCSyL/w1t7Y1P1yIooihoSH8+Mc/xq1bt5w5/XK5HPL5PHZ2dnDmzBm0tLZCPCIIF4tFTE9PIxaLwe/3O6GQEx0TvVwMeEQvQBRFeL1eFItFTE5OYnZ2FlojhImN1olYLIbXXnsNp0+fxvDwMPr6+g5t7Qg0qlLtgfbZ9Sx8Xh9krwyPR4JpmkglkxgZGcHo6CgGBwfR09PjrEggyzJkWcbXLl9Gb18fMvfv4+b165ibn8fU1BQ0uyWoKeSkUilcuXIFIyMj6OnpQUdHBwKBwIE3V1mWEYvF8Prrr2N4ZAT//R//EfczGdy9cwfT09O4Nzm5J0TZLVahUAhnz57F6Ogoert7oXj9e0KApmlYXl7GjRs3MDMzg7GxMZw+fRpvv/02BgcH9wUsrV5HTdPw+Wef4d1334VaLmN+fh7pdBqtra3OVB7Nn5E9QW8qldoTbERRRDgcRjgcxte//nV0d3fj1qef4ve//71VNRoKAcALTZni8XgQCAQQi8WciupoNLpvJQl7f2RZRjAYRCqVQk9vL06cOOEEtWZ2q6IkSYjGYujq7sb9+/fx7x98gD9//DHkRteyCUDyeKyq43Qao2fP4s1GGN+zn5IHfsWPUDiEeDyOYrHoTLViT/Fjf9/d3Y2VRyu49ektPFpbhS+gIBSJQGl0aQOAIIoQvV54g0HE02l09fRgfnYWv/71r/H+++9DsLu9GytktLe3O8McLly48EwtskS0l2C+rEdTov+E7CWodnZ2kM/nsVsoWAP0G122Pp8PkXAY6cZUHsoTLW42Owxt53KYn59HLp+3/p89J17T6/x+P+LxOBJxa9xfOBTav73Ga4vFIrLZLHK5nLNfmqZB0zR4RBHBRtBIpVLO6gL23GtPY29fq9exu7OD3PY2tnM5lMpl5z08jRYZv88HRVGcgLNvya3GdrLrWaw8eoRSuYx4NIpEPOGEocP2Id84X5VKBfFEAvF4HKHG0mL2OanVaiiWCtjezmF1LQuv14uhgYEDx3rpug5N05DL57GysgITQCqZRKIRcp53ybhSqYTs+jpyuZwzni6VSuHEAZNJ26uEFAoF7DbGcvoVBYMDA0g88VrDMKxpZWo1bG5uIpvNYjuXg6brjwNpU9D2+/0IBAJIJ5NON/ye/SyXrRa53R2USyWUyyqCgQD6+/ud/axUKigWiygUCsjt5JHL5SDLMvr6+pBKJuGVvY+HAeg6tHodaqWC7a0tbGxsIN+Yfueg/QuFQohEIkinUkin0wge8tkT0dEY8IheItM0UdM0VGs1mAD8jSlGhCfGyh3GME3ohgHDtFuL7ID3eC1QQIDHY1XYPp4c+ejtGoYB3TBQr9ehaRpEQbAG4b+kOccM01pV4fH2Rfi8XniPmHPNvsFrug5Nt7ojfbIP0jGClL0Wqx10BACexiTSe7poYcIwrPcQAMgez5FVqc1/GF90lJi9n83dw4IoWpMTH3BunG7SxrFBECAdUnVs0xtd0NY4x/0BSkCjMtvjgdwoatj/YNB438Z/YVjHftBYOTvg1xprD8seDyTJ03jt/nOrN/bJ2T/7XDSdE09jvkC5Ub183MphItqPAY/oJbNvzEDT9BjP+PPA3oDR9K/OdoHHwe+427Vb3uwKhxepDD3yPYAjx2Pt/9mm83bMY2suQnHOyyHv2bxiwn+0A1eiOEZLKZ5hn5vPRfPP2u8s4OgJtM3mK898+n7uCaxHnfvG/jRfH/v2r+n3hcUXRC+GAY+IiIjIZdj+TUREROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhEREREv2VyPAAAAUpJREFULsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQyDHhERERELsOAR0REROQy/w9RlBXrNURdSgAAAABJRU5ErkJggg==\" data-filename=\"log_document.png\"><font color=\"#085294\"><br></font></h1>', 'active', '2019-05-05 13:13:58', '2019-05-05 13:13:58');
INSERT INTO `as_terms_and_condition` (`t_c_id`, `added_by`, `type`, `document`, `body_text`, `status`, `created_at`, `updated_at`) VALUES
(2, '1', 't&c', '1557062174.png', '<p>\r\n				                 <iframe src=\"//www.youtube.com/embed/UebSF-d5UOs\" class=\"note-video-clip\" width=\"640\" height=\"360\" frameborder=\"0\"></iframe></p><h2>Test Video for Terms &amp; Condition in <font color=\"#085294\"><b>Software Galaxy Ltd</b></font><br></h2>', 'active', '2019-05-05 13:16:14', '2019-05-05 13:16:14'),
(3, '31', 'privacy', '', 'bvnbnmbm', 'deactive', '2019-05-15 08:50:32', '2019-05-15 08:50:32'),
(4, '31', 'privacy', '1557910418.PNG', 'bvnbnmbm', 'deactive', '2019-05-15 08:53:38', '2019-05-15 08:53:38');

-- --------------------------------------------------------

--
-- Table structure for table `as_tutorials`
--

CREATE TABLE `as_tutorials` (
  `tutorial_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','deactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_tutorials`
--

INSERT INTO `as_tutorials` (`tutorial_id`, `title`, `link`, `status`, `created_at`, `updated_at`) VALUES
(4, 'POS-1', 'https://www.youtube.com/embed/mJ3_SCn8Azs', 'active', '2019-03-13 08:15:06', '2019-03-13 08:15:06');

-- --------------------------------------------------------

--
-- Table structure for table `as_users`
--

CREATE TABLE `as_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmation_key` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_verify_key` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_confirmed` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_reset_key` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_reset_confirmed` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_reset_timestamp` datetime DEFAULT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_role` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `banned` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `status_now` enum('followup','followed','Mojammel','Nur-alom') COLLATE utf8mb4_unicode_ci DEFAULT 'Nur-alom',
  `permission` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_users`
--

INSERT INTO `as_users` (`user_id`, `email`, `username`, `password`, `confirmation_key`, `sms_verify_key`, `sms_confirmed`, `confirmed`, `password_reset_key`, `password_reset_confirmed`, `password_reset_timestamp`, `register_date`, `user_role`, `last_login`, `banned`, `status_now`, `permission`, `created_at`, `updated_at`) VALUES
(1, 'rifat@gmail.com', 'super-admin1', '$2a$13$UPj8EVJWvP73FZ9Ih0xzUebANa.Jwzy83Q4Nij.mDbivHo2pDGRE.', '', '', 'Y', 'Y', '', 'N', '2019-03-02 00:00:00', '2019-03-12 06:56:18', 4, '2019-03-03 00:00:00', 'N', 'followup', 'account,maintainer', '2019-03-11 18:00:00', '2019-05-02 10:14:03'),
(4, 'rifat12@gmail.com', 'super', '$2a$13$UPj8EVJWvP73FZ9Ih0xzUebANa.Jwzy83Q4Nij.mDbivHo2pDGRE.', '', '', 'Y', 'Y', '', 'N', '2019-03-02 00:00:00', '2019-03-12 06:56:18', 2, '2019-03-03 00:00:00', 'N', 'followup', NULL, '2019-03-11 18:00:00', '2019-05-06 09:09:03'),
(13, NULL, '345', '$2a$13$UPj8EVJWvP73FZ9Ih0xzUebANa.Jwzy83Q4Nij.mDbivHo2pDGRE.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-16 11:57:14', 4, NULL, 'N', 'Nur-alom', 'account', '2019-03-16 11:57:14', '2019-05-06 11:41:41'),
(24, 'sdfds@dfg.com', '852', '$2a$13$UPj8EVJWvP73FZ9Ih0xzUebANa.Jwzy83Q4Nij.mDbivHo2pDGRE.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-18 15:11:57', 1, NULL, 'N', 'Nur-alom', 'maintainer', '2019-03-18 15:11:57', '2019-05-14 05:26:28'),
(25, NULL, '3', '$2a$13$UPj8EVJWvP73FZ9Ih0xzUebANa.Jwzy83Q4Nij.mDbivHo2pDGRE.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 11:43:44', 4, NULL, 'N', 'Nur-alom', 'account,maintainer', '2019-05-06 11:43:44', '2019-05-09 09:04:59'),
(27, '4@5', '7', '$2a$13$UPj8EVJWvP73FZ9Ih0xzUebANa.Jwzy83Q4Nij.mDbivHo2pDGRE.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 11:50:25', 4, NULL, 'Y', 'Nur-alom', 'account', '2019-05-06 11:50:25', '2019-05-06 11:53:40'),
(31, 'superadmin@appsowl.com', 'super-admin', '$2a$13$UPj8EVJWvP73FZ9Ih0xzUebANa.Jwzy83Q4Nij.mDbivHo2pDGRE.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-13 09:42:32', 4, NULL, 'N', 'Nur-alom', 'account,maintainer', '2019-05-13 09:42:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `as_user_details`
--

CREATE TABLE `as_user_details` (
  `id_user_details` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `domain_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zone` mediumtext COLLATE utf8mb4_unicode_ci,
  `area` mediumtext COLLATE utf8mb4_unicode_ci,
  `profile_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid_card` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_user_details`
--

INSERT INTO `as_user_details` (`id_user_details`, `user_id`, `domain_id`, `agent_id`, `added_by`, `store_id`, `first_name`, `last_name`, `dob`, `phone`, `address`, `permanent_address`, `country`, `country_code`, `zone`, `area`, `profile_image`, `nid_card`, `attach`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 4, 1, 2, 'Rifat', 'Hasan', '1997-12-11', '5413621', 'cfgbcvfbg', 'cvgfbhnvg', 'gh', '123', 'bhnj', '', '', '', '', NULL, NULL),
(2, 4, NULL, NULL, NULL, NULL, 'dsfds', 'sf', '2019-03-16', '345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-16 11:57:14', '2019-03-16 11:57:14'),
(3, 13, NULL, NULL, NULL, NULL, 'Test', 'Test', '2019-03-18', '8520', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-18 04:54:54', '2019-03-18 04:54:54'),
(13, 24, NULL, NULL, NULL, NULL, 'Test main', 'Test main', '2019-03-18', '01730974883', NULL, NULL, NULL, '+88', NULL, NULL, NULL, NULL, NULL, '2019-03-18 15:11:57', '2019-03-18 15:11:57'),
(14, 25, NULL, NULL, NULL, NULL, '1', '2', '2019-05-15', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 11:43:44', '2019-05-06 11:43:44'),
(15, 27, NULL, NULL, NULL, NULL, '1', '2', '2019-05-06', '7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 11:50:25', '2019-05-06 11:50:25'),
(18, 31, NULL, NULL, NULL, NULL, 'Super', 'Admin', NULL, '01916908068', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-13 09:42:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `as_user_roles`
--

CREATE TABLE `as_user_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `role` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_user_roles`
--

INSERT INTO `as_user_roles` (`role_id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'user', '2019-05-13 09:42:32', NULL),
(2, 'agent', '2019-05-13 09:42:32', NULL),
(3, 'admin', '2019-05-13 09:42:32', NULL),
(4, 'superadmin', '2019-05-13 09:42:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `as_voucher`
--

CREATE TABLE `as_voucher` (
  `voucher_id` int(10) UNSIGNED NOT NULL,
  `voucher_title` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_amount` decimal(20,2) DEFAULT NULL,
  `voucher_price` decimal(20,2) DEFAULT NULL,
  `generated_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_available` enum('available','not_available','reject') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'available',
  `user_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_note` mediumtext COLLATE utf8mb4_unicode_ci,
  `voucher_document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_status` enum('active','inactive','cancel') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_voucher`
--

INSERT INTO `as_voucher` (`voucher_id`, `voucher_title`, `voucher_code`, `voucher_amount`, `voucher_price`, `generated_by`, `voucher_available`, `user_id`, `voucher_note`, `voucher_document`, `voucher_status`, `created_at`, `updated_at`) VALUES
(1, 'Test', '1234567890', '100.00', '5.00', '1', 'available', NULL, 'note', '1557635896.jpg', 'active', '2019-05-12 04:38:17', '2019-05-12 05:01:12');

-- --------------------------------------------------------

--
-- Table structure for table `as_withdrawal`
--

CREATE TABLE `as_withdrawal` (
  `withdrawal_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `withdrawal_type` enum('system','user','agent') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `withdrawal_amount` decimal(20,2) DEFAULT NULL,
  `withdrawal_charge` decimal(20,2) DEFAULT NULL,
  `withdrawal_total_amount` decimal(20,2) DEFAULT NULL,
  `withdrawal_note` mediumtext COLLATE utf8mb4_unicode_ci,
  `withdrawal_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `withdrawal_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `withdrawal_status` enum('paid','due','hold','cancel','requested') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `withdrawal_approve_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approve_note` mediumtext COLLATE utf8mb4_unicode_ci,
  `withdrawal_document` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `as_withdrawal`
--

INSERT INTO `as_withdrawal` (`withdrawal_id`, `user_id`, `withdrawal_type`, `withdrawal_amount`, `withdrawal_charge`, `withdrawal_total_amount`, `withdrawal_note`, `withdrawal_method`, `withdrawal_transaction_id`, `withdrawal_status`, `withdrawal_approve_by`, `approve_note`, `withdrawal_document`, `created_at`, `updated_at`) VALUES
(1, '24', 'user', '500.00', '20.00', '520.00', 'jgdfngdfngkndsf.\r\ndsfdsfds\r\ndsfdsfds\r\ndsfdsf', 'Bkash', '9999999999999999', 'paid', '31', '9999999999999999999999999999999999', '1557824316.txt', '2019-05-13 18:00:00', '2019-05-14 08:58:37'),
(2, '24', 'user', '500.00', '20.00', '520.00', 'jgdfngdfngkndsf.\r\ndsfdsfds\r\ndsfdsfds\r\ndsfdsf', 'Bkash', NULL, 'cancel', '31', NULL, NULL, '2019-05-13 18:00:00', NULL),
(3, '24', 'user', '500.00', '20.00', '520.00', 'jgdfngdfngkndsf.\r\ndsfdsfds\r\ndsfdsfds\r\ndsfdsf', 'Bkash', NULL, 'hold', '31', NULL, NULL, '2019-05-13 18:00:00', NULL),
(4, '24', 'user', '500.00', '20.00', '520.00', 'jgdfngdfngkndsf.\r\ndsfdsfds\r\ndsfdsfds\r\ndsfdsf', 'Bkash', NULL, 'due', '31', 'withdraw pay first', '1557823859.doc', '2019-05-13 18:00:00', '2019-05-14 08:50:59'),
(5, '24', 'user', '500.00', '20.00', '520.00', 'jgdfngdfngkndsf.\r\ndsfdsfds\r\ndsfdsfds\r\ndsfdsf', 'Bkash', '12435465464', 'paid', '31', 'ygfhgfhg', '1557832616.PNG', '2019-05-13 18:00:00', '2019-05-14 11:16:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2019_03_11_091824_create_as_country_table', 2),
(3, '2019_03_11_103249_create_as_desktop_soft_version_table', 3),
(4, '2019_03_11_103512_create_as_pos_requirements_table', 3),
(5, '2019_03_11_104744_create_as_software_table', 3),
(6, '2019_03_11_105657_create_as_software_variation_table', 4),
(8, '2019_03_11_111050_create_as_subscribe_payment_table', 5),
(9, '2019_03_11_114330_create_as_login_attempts_table', 5),
(10, '2019_03_11_114540_create_as_sub_domain_table', 5),
(12, '2019_03_12_050551_create_as_user_table', 6),
(16, '2019_03_12_055309_create_as_user_details_table', 7),
(17, '2019_03_12_055641_create_as_user_roles', 7),
(18, '2019_03_12_060029_create_software_category_table', 7),
(22, '2019_03_13_125924_create_as_tutorials_table', 9),
(35, '2019_04_24_174135_create_as_agent_commission_table', 13),
(38, '2019_04_30_172707_create_as_terms_and_condition_table', 15),
(40, '2019_05_04_142307_create_activity_log_table', 16),
(41, '2019_03_11_110141_create_as_subscribe_table', 17),
(42, '2019_03_13_163132_create_as_voucher_table', 17),
(43, '2019_04_23_131545_create_as_plugins_table', 17),
(44, '2019_05_11_152817_create_as_withdrawal_table', 17),
(46, '2019_05_12_114031_create_as_invoices_table', 18),
(47, '2019_05_12_113843_create_as_payment_methods_table', 19),
(50, '2019_05_12_121309_create_as_notification_table', 21),
(52, '2019_05_12_114200_create_as_payments_table', 22),
(53, '2019_03_11_071333_create_as_agent_payment_table', 23),
(54, '2019_05_13_140038_create_sms_log_table', 24);

-- --------------------------------------------------------

--
-- Table structure for table `sms_log`
--

CREATE TABLE `sms_log` (
  `sms_log_id` bigint(20) UNSIGNED NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci,
  `send_to` int(11) DEFAULT NULL,
  `sender` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `software_category`
--

CREATE TABLE `software_category` (
  `pos_category_id` int(10) UNSIGNED NOT NULL,
  `software_id` int(11) NOT NULL,
  `pos_category_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_category_icon` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_category_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_category_tagline` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_category_about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_category_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_category_module` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `as_agent_commission`
--
ALTER TABLE `as_agent_commission`
  ADD PRIMARY KEY (`commission_id`);

--
-- Indexes for table `as_agent_payment`
--
ALTER TABLE `as_agent_payment`
  ADD PRIMARY KEY (`agent_payment_id`);

--
-- Indexes for table `as_country`
--
ALTER TABLE `as_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `as_desktop_soft_version`
--
ALTER TABLE `as_desktop_soft_version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `as_invoices`
--
ALTER TABLE `as_invoices`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `as_login_attempts`
--
ALTER TABLE `as_login_attempts`
  ADD PRIMARY KEY (`id_login_attempts`);

--
-- Indexes for table `as_notification`
--
ALTER TABLE `as_notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `as_payment`
--
ALTER TABLE `as_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `as_payment_method`
--
ALTER TABLE `as_payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `as_plugins`
--
ALTER TABLE `as_plugins`
  ADD PRIMARY KEY (`plugins_id`),
  ADD UNIQUE KEY `as_plugins_plugins_unique_name_unique` (`plugins_unique_name`);

--
-- Indexes for table `as_pos_requirements`
--
ALTER TABLE `as_pos_requirements`
  ADD PRIMARY KEY (`pos_requirement_id`);

--
-- Indexes for table `as_promocode_old`
--
ALTER TABLE `as_promocode_old`
  ADD PRIMARY KEY (`promocode_id`);

--
-- Indexes for table `as_software`
--
ALTER TABLE `as_software`
  ADD PRIMARY KEY (`software_id`);

--
-- Indexes for table `as_software_variation`
--
ALTER TABLE `as_software_variation`
  ADD PRIMARY KEY (`software_variation_id`);

--
-- Indexes for table `as_subscribe`
--
ALTER TABLE `as_subscribe`
  ADD PRIMARY KEY (`subscribe_id`);

--
-- Indexes for table `as_sub_domain`
--
ALTER TABLE `as_sub_domain`
  ADD PRIMARY KEY (`domain_id`),
  ADD UNIQUE KEY `as_sub_domain_sub_domain_unique` (`sub_domain`);

--
-- Indexes for table `as_terms_and_condition`
--
ALTER TABLE `as_terms_and_condition`
  ADD PRIMARY KEY (`t_c_id`);

--
-- Indexes for table `as_tutorials`
--
ALTER TABLE `as_tutorials`
  ADD PRIMARY KEY (`tutorial_id`);

--
-- Indexes for table `as_users`
--
ALTER TABLE `as_users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `as_user_username_unique` (`username`),
  ADD UNIQUE KEY `as_user_email_unique` (`email`);

--
-- Indexes for table `as_user_details`
--
ALTER TABLE `as_user_details`
  ADD PRIMARY KEY (`id_user_details`);

--
-- Indexes for table `as_user_roles`
--
ALTER TABLE `as_user_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `as_voucher`
--
ALTER TABLE `as_voucher`
  ADD PRIMARY KEY (`voucher_id`),
  ADD UNIQUE KEY `as_voucher_voucher_code_unique` (`voucher_code`);

--
-- Indexes for table `as_withdrawal`
--
ALTER TABLE `as_withdrawal`
  ADD PRIMARY KEY (`withdrawal_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_log`
--
ALTER TABLE `sms_log`
  ADD PRIMARY KEY (`sms_log_id`);

--
-- Indexes for table `software_category`
--
ALTER TABLE `software_category`
  ADD PRIMARY KEY (`pos_category_id`),
  ADD UNIQUE KEY `software_category_pos_category_key_unique` (`pos_category_key`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `as_agent_commission`
--
ALTER TABLE `as_agent_commission`
  MODIFY `commission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `as_agent_payment`
--
ALTER TABLE `as_agent_payment`
  MODIFY `agent_payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_country`
--
ALTER TABLE `as_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `as_desktop_soft_version`
--
ALTER TABLE `as_desktop_soft_version`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_invoices`
--
ALTER TABLE `as_invoices`
  MODIFY `invoice_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_login_attempts`
--
ALTER TABLE `as_login_attempts`
  MODIFY `id_login_attempts` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_notification`
--
ALTER TABLE `as_notification`
  MODIFY `notification_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `as_payment`
--
ALTER TABLE `as_payment`
  MODIFY `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `as_payment_method`
--
ALTER TABLE `as_payment_method`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_plugins`
--
ALTER TABLE `as_plugins`
  MODIFY `plugins_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `as_pos_requirements`
--
ALTER TABLE `as_pos_requirements`
  MODIFY `pos_requirement_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_promocode_old`
--
ALTER TABLE `as_promocode_old`
  MODIFY `promocode_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `as_software`
--
ALTER TABLE `as_software`
  MODIFY `software_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_software_variation`
--
ALTER TABLE `as_software_variation`
  MODIFY `software_variation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_subscribe`
--
ALTER TABLE `as_subscribe`
  MODIFY `subscribe_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_sub_domain`
--
ALTER TABLE `as_sub_domain`
  MODIFY `domain_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_terms_and_condition`
--
ALTER TABLE `as_terms_and_condition`
  MODIFY `t_c_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `as_tutorials`
--
ALTER TABLE `as_tutorials`
  MODIFY `tutorial_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `as_users`
--
ALTER TABLE `as_users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `as_user_details`
--
ALTER TABLE `as_user_details`
  MODIFY `id_user_details` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `as_user_roles`
--
ALTER TABLE `as_user_roles`
  MODIFY `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `as_voucher`
--
ALTER TABLE `as_voucher`
  MODIFY `voucher_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `as_withdrawal`
--
ALTER TABLE `as_withdrawal`
  MODIFY `withdrawal_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `sms_log`
--
ALTER TABLE `sms_log`
  MODIFY `sms_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `software_category`
--
ALTER TABLE `software_category`
  MODIFY `pos_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
